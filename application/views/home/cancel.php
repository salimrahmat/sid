<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                  <tr>
                    <th>No</th>
                    <th>No Order</th>
                    <th>Nama Member</th>
                    <th>Tanggal Keluar</th>
                    <th>Total Jogrog</th>
                    <th>Total Non Jogrog</th>
                    <th>Diskon</th>
                    <th></th>
                  </tr>
                  <?php 
                    foreach($data['list_cancel']['records']  as $idx => $record){ ?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo $record->ORDER_NO;?></td>
                            <td><?php echo $record->MEMBER_NAME;?></td>
                            <td><?php echo $record->OUT_DATE;?></td>
                            <td><?php echo $record->TOTAL_JOGROG;?></td>
                            <td><?php echo $record->TOTAL_NON_JOGROG;?></td>
                            <td><?php echo $record->DISCOUNT;?></td>
                            <td>
                                <a href="<?php echo site_url('home/detail/' . $record->ORDER_NO);?>?<?php echo url_create_return_query();?>"
                                        target="_blank"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Detail <?php echo html_escape($record->ORDER_NO);?>">
                                        <i class="fa  fa-file-text-o"></i>
                                </a>
                            </td>
                        </tr>
                  <?php      
                    }
                  ?>
              </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['list_cancel']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>