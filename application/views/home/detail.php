<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="konfirmasi_pembelian">Konfirmasi Pembelian</label>
                    <p class="konfirmasi_pembelian"><?php echo ($data['header']['confirm'] === "C" ? "Cancel" : "Invoice" ) ?></p>
                </div>
                <div class="form-group">
                    <label for="member">Member</label>
                    <p class="member"><?php echo $data['header']['member_name']; ?></p>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <p class="alamat"><?php echo nl2br($data['header']['member_alamat']); ?></p>
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <p class="phone"><?php echo nl2br($data['header']['member_hp']); ?></p>
                </div>
                <div class="form-group">
                    <label for="outside_date">Tanggal Keluar</label>
                    <p class="outside_date"><?php echo $data['header']['outdate']; ?></p>
                </div>
                <div class="form-group">
                    <label for="tax">Tax</label>
                    <p class="tax"><?php echo ($data['header']['tax'] !== 'NULL' ? 'Y' : 'N '); ?></p>
                </div>
                <label for="member">Detail Barang</label>
                <div class="box-body ">
                    <?php
                        $total_jogrog=0; 
                        $total_non_jogrog=0;
                        $discount=0;
                        foreach ($data['details'] as $idx => $record) { ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 200px">No Registrasi</th><td><?php echo $record['registerno']; ?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Registrasi</th><td><?php echo $record['registerdate']; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipe Ternak</th><td><?php echo $record['type_ternak']; ?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Ternak</th><td><?php echo $record['jenis_ternak']; ?></td>
                                </tr>
                                <tr>
                                    <th>Berat Masuk</th><td><?php echo $record['berat']; ?></td>
                                </tr>
                                <tr>
                                    <th>Berat Customer</th><td><?php echo $record['berat_konsumen']; ?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Jual</th><td><?php echo $record['jenis']; ?></td>
                                </tr>
                                <tr>
                                    <th>Harga Perkilo</th><td><?php echo "Rp " . number_format($record['non_jogrog'],2,',','.'); ?></td>
                                </tr>
                                <tr>
                                    <th>Jogrog</th><td><?php echo "Rp " . number_format($record['jogrog'],2,',','.'); ?></td>
                                </tr>
                                <tr>
                                    <th>Discount</th><td><?php echo ($record['discount'] ==='NULL' ? 0 : $record['discount'])."%"; ?></td>
                                </tr>
                            </table>
                        <br>
                    <?php
                         if  (intval($record['jogrog']) > 0) {
                             $total_jogrog = $total_jogrog + floatval($record['jogrog']);
                         } else {
                             $total_non_jogrog = $total_non_jogrog + (floatval($record['berat_konsumen']) * floatval($record['non_jogrog']));
                         }
                         $discount = $discount+floatval($record['discount']);
                        }
                    ?>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 200px">Sub Total Jogrog</th><td><?php echo "Rp " . number_format($total_jogrog,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Sub Total Non Jogrog</th><td><?php echo "Rp " . number_format($total_non_jogrog,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Discount</th><td><?php echo "Rp " . number_format(($total_jogrog + $total_non_jogrog) * $discount / 100,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Total</th><td><?php echo "Rp " . number_format($total_jogrog + $total_non_jogrog,2,',','.'); ?></td>
                        </tr>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>