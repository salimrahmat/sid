<?php
echo form_open(($data['flow_data']['ID_DAILY_WEIGHT'] !==NULL ? 'penimbangan/update/' . $data['flow_data']['ID_DAILY_WEIGHT'] : 'penimbangan/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('penimbangan/save')]); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div>  
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="registrasi_no">No Registrasi</label>
                    <?php
                         $options = [];
                         $options[''] = 'Pilih Tipe Ternak';
                         if ($data['flow_data']['REGISTER_NO'] !== NULL)
                             $options[$data['flow_data']['REGISTER_NO']] = $data['flow_data']['REGISTER_NO'];
                         echo form_dropdown('registrasi_no', $options, set_value('registrasi_no', ($data['flow_data']['REGISTER_NO'] !== NULL ? $data['flow_data']['REGISTER_NO'] : NULL), FALSE), 'id="registrasi_no" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="weight_date">Tanggal Timbang</label>
                    <?php
                    echo form_input([
                        'name'          => "weight_date",
                        'class'         => "form-control",
                        'id'            => "datepicker",
                        'placeholder'   => "Tanggal Timbang",
                        'required'      => 'required',
                        'value'         => set_value('weight_date', ($data['flow_data']['WEIGHT_DATE'] !== NULL ? $data['flow_data']['WEIGHT_DATE'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="weight">Berat Timbang</label>
                    <?php
                    echo form_input([
                        'name'          => "weight",
                        'placeholder'   => "Berat Timbang",
                        'class'         => "form-control",
                        'type'          => "decimal",
                        'required'      => 'required',
                        'value'         => set_value('weight', ($data['flow_data']['WEIGHT'] !== NULL ? $data['flow_data']['WEIGHT'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="add_vitamin">Penambahan Vitamin</label>
                    <br>
                    <?php
                        echo form_radio([
                            'name'          => "add_vitamin",
                            'value'         => "Ya",     
                            'checked'       => TRUE,
                            'class'         => 'privilege-radio',
                            'required'      => 'required',
                            'value'         => ($data['flow_data']['WEIGHT'] !== 'Ya' ? 'Ya' : $data['flow_data']['WEIGHT'])
                        ]);
                        echo form_label('Ya');
                        echo form_radio([
                            'name'          => "add_vitamin",
                            'value'         => "No",     
                            'checked'       => FALSE,
                            'required'      => 'required',
                            'class'         => 'privilege-radio',
                            'value'         => ($data['flow_data']['WEIGHT'] !== 'No' ? 'No' : $data['flow_data']['WEIGHT'])
                        ]);
                        echo form_label('Tidak');
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>

<?php echo form_close();?>
<script type="text/javascript">
 window.onload = function(event) {
    let formForm = $('form[name="form-input"]');

    formForm.submit(function() {
        InputHelper.input_release($(this));
        return true;
    });

    <?php 
    if ( count($data['flow_data']['value']) > 0) { ?>
        $('.alert-dismissible').show();
    <?php
    } else { ?>
        $('.alert-dismissible').hide();
    <?php    
    }
    ?>
 }
</script>