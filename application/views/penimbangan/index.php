<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_PENIMBANGAN,PRIVILEGE_CREATE)) { ?>
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('penimbangan/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                <?php 
                  }
                ?>
                </div>
                <div class="box-tools">
                    <?php
                    echo form_open('penimbangan/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>No Registrasi</th>
                        <th>Tanggal Penimbangan</th>
                        <th>Berat Badan Awal</th>
                        <th>Berat Badan Akhir</th>
                        <th>Deviasi Berat Badan</th>
                        <th>Vitamin</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['penimbangan']['records'] as $idx => $record)
                        
                        {?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->REGISTER_NO);?></td>
                            <td><?php echo date_reformat($record->WEIGHT_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->WEIGHT_IN;?></td>
                            <td><?php echo $record->WEIGHT;?></td>
                            <td><?php echo $record->WEIGHT - $record->WEIGHT_IN;?></td>
                            <td><?php echo ($record->ADD_VITAMIN =='Ya' ? 'Ya' : 'Tidak');?></td>                
                            <td class="text-center">
                                <?php 
                                  if(is_privilege(PRIVILEGE_PENIMBANGAN,PRIVILEGE_UPDATE)) { ?>
                                    <a href="<?php echo site_url('penimbangan/form/' . $record->ID_DAILY_WEIGHT);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Mengubah <?php echo html_escape($record->REGISTER_NO);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                <?php 
                                  }
                                ?>
                                <?php 
                                  if(is_privilege(PRIVILEGE_PENIMBANGAN,PRIVILEGE_DETAIL)) { ?>
                                    <a class="btn btn-flat btn-sm  btn-logs"
                                        data-toggle="tooltip" data-placement="bottom"
                                        data-message="<?php echo html_escape($record->REGISTER_NO);?>"
                                        title="Logs <?php echo html_escape($record->REGISTER_NO);?>">
                                        <i class="fa fa-file-text text-danger"></i>
                                    </a> 
                                <?php 
                                  }
                                ?>     
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['penimbangan']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-delete">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive no-padding" id="modal-table">
                    
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Close" data-dismiss="modal" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {

        $('.btn-logs').click(function(e) {
            e.preventDefault();

            let rowData = $(this);
            let modalRemove = $('.modal-delete');
        
            $('.modal-title', modalRemove).text(rowData.attr('data-original-title'));
            $.ajax({
                url: '<?php echo site_url('penimbangan/getLogWeight'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: rowData.attr('data-message')},
                success: function(d) {
                    var HTML = "";
                    HTML = "<table class='table table-hover'><tr><th>No Registrasi</th><th>Berat Sebelumnya</th><th>Berat Baru</th><th>Tanggal Timbang </th><th>Tanggal Proses </th></tr>";
                    $.each( d, function( key, value ) {
                        HTML +=['<tr><td>'+value['noregistrasi']+'</td><td>'+value['from']+'</td><td>'+value['to']+'</td><td>'+value['dtm_timbang']+'</td><td>'+value['dtm_crt']+'</td></tr>'];
                    });
                    HTML += "</table>";
                    $('#modal-table').html(HTML);
                    // $('#table').attr('id');
                }
            });
            
            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });

    };
</script>