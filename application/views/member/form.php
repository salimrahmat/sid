<?php
echo form_open(($data['flow_data']['member_id'] !==NULL ? 'member/update/' . $data['flow_data']['member_id'] : 'member/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('member/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php 
            foreach ($data['flow_data']['value'] as $gagal) {
              echo $gagal;
            } 
        ?>
        </div>  
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="member_name">Nama</label>
                        <?php
                        echo form_input([
                            'name'          => "member_name",
                            'placeholder'   => "Nama",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('member_name', ($data['flow_data']['member_name'] !== NULL ? $data['flow_data']['member_name'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="member_alamat">Alamat</label>
                        <?php
                        echo form_textarea([
                            'name'          => "member_alamat",
                            'placeholder'   => "Alamat",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('member_alamat', ($data['flow_data']['member_alamat'] !== NULL ? $data['flow_data']['member_alamat'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="member_email">Email</label>    
                        <?php
                        echo form_input([
                            'id'            => "member_email",
                            'name'          => "member_email",
                            'placeholder'   => "Email",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('member_email', ($data['flow_data']['member_email'] !== NULL ? $data['flow_data']['member_email'] : NULL), FALSE)
                            ]);?>
                    </div>
                    <div class="form-group">
                        <label>Phone </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                            </div>                 
                            <input type="text" name="member_hp" class="form-control" data-inputmask='"mask": "+6299 999 999 999"' data-mask value= "<?php echo ($data['flow_data']['member_hp'] !== NULL ? $data['flow_data']['member_hp'] : NULL);?>" >
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                        <i class="fa fa-undo"></i> Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    };
</script>