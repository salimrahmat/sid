<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_MEMBER,PRIVILEGE_CREATE)) { ?>
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('member/form');?>?<?php echo url_create_return_query();?>">Input</a>        
                <?php
                  } 
                ?>    
                </div>
                <div class="box-tools">
                    <?php
                    echo form_open('member/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['member']['records'] as $idx => $record)
                        {?>
                            <tr>
                                <td><?php echo $idx + 1; ?></td>
                                <td><?php echo html_escape($record->MEMBER_NAME);?></td>
                                <td><?php echo nl2br(html_escape($record->MEMBER_ALAMAT));?></td>
                                <td><?php echo html_escape($record->MEMBER_EMAIL);?></td>
                                <td><?php echo html_escape($record->MEMBER_HP);?></td>
                                <td class="text-center">
                                <?php 
                                  if(is_privilege(PRIVILEGE_MEMBER,PRIVILEGE_UPDATE)) { ?>
                                    <a href="<?php echo site_url('member/form/' . $record->MEMBER_ID);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Update Member <?php echo html_escape($record->MEMBER_NAME);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                <?php
                                  } 
                                ?>
                                </td>
                            </tr>
                    <?php 
                        }
                    ?>            
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['member']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>