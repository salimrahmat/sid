<?php
echo form_open(($data['flow_data']['id_farm'] !==NULL ? 'registrasi/update/' . $data['flow_data']['id_farm'] : 'registrasi/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php 
            foreach ($data['flow_data']['value'] as $gagal) {
              echo $gagal;
            } 
        ?>
        </div>   
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="noregis">No Registrasi</label>
                        <?php
                        echo form_input([
                            'name'          => "noregis",
                            'placeholder'   => "No Registrasi",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'maxlength'     => '11',
                            'value'         => set_value('noregis', ($data['flow_data']['noregis'] !== NULL ? $data['flow_data']['noregis'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="register_date">Tanggal Registrasi</label>
                        <?php
                        echo form_input([
                            'name'              => "register_date",
                            'class'             => "form-control",
                            'id'                => "datepicker",
                            'placeholder'       => "Tanggal Registrasi",
                            'required'          => 'required',
                            'value'             => set_value('register_date', ($data['flow_data']['register_date'] !== NULL ? $data['flow_data']['register_date'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="tipe_ternak">Tipe Ternak</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Ternak';
                        if ($data['tipe_ternak'] !== NULL ){
                            foreach ($data['tipe_ternak'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('tipe_ternak', $options, set_value('tipe_ternak', ($data['flow_data']['tipe_ternak'] !==NULL ? $data['flow_data']['tipe_ternak'] : NULL), FALSE), 'id="tipe_ternak" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="jenis_ternak">Jenis Ternak</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Ternak';
                        if ($data['jenis_ternak'] !== NULL ){
                            foreach ($data['jenis_ternak'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('jenis_ternak', $options, set_value('jenis_ternak', ($data['flow_data']['jenis_ternak'] !==NULL ? $data['flow_data']['jenis_ternak'] : NULL), FALSE), 'id="jenis_ternak" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="weight">Berat Masuk</label>
                        <?php
                        echo form_input([
                            'name'          => "weight",
                            'placeholder'   => "Berat Masuk",
                            'class'         => "form-control",
                            'type'          => "decimal",
                            'required'      => 'required',
                            'value'         => set_value('weight', ($data['flow_data']['weight'] !== NULL ? $data['flow_data']['weight'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="jenis_domba">Tipe Kandang</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        if ($data['tipe_kandang'] !== NULL ){
                            foreach ($data['tipe_kandang'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('tipe_kandang', $options, set_value('tipe_kandang', ($data['flow_data']['tipe_kandang'] !==NULL ? $data['flow_data']['tipe_kandang'] : NULL), FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                       <input type="hidden" name="exis_kandang" value="<?php echo $data['flow_data']['exis_kandang'] !== NULL ? $data['flow_data']['exis_kandang'] : NULL ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="noroom">Nomor Kamar</label>
                        <?php
                        echo form_input([
                            'name'          => "noroom",
                            'placeholder'   => "Nomor Kamar",
                            'class'         => "form-control",
                            'type'          => "number",
                            'required'      => 'required',
                            'value'         => set_value('noroom', ($data['flow_data']['noroom'] !== NULL ? $data['flow_data']['noroom'] : NULL), FALSE)
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
 <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

       
    };
</script>