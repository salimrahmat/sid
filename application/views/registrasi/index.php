<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_REGISTRASI,PRIVILEGE_CREATE)) { ?>      
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('registrasi/form');?>?<?php echo url_create_return_query();?>">Tambah</a>       
                <?php
                  }
                ?>     
                </div>
                <?php
                    echo form_open('registrasi/search', 
                    [
                        'name'   => 'form-search-ternak',
                        'method' => 'post',
                        'id'     => 'ternak',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Ternak';
                        foreach ($data['tipe_ternak'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_ternak', $options, set_value('tipe_ternak', NULL, FALSE), 'id="tipe_ternak" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
                <?php
                    echo form_open('registrasi/search', 
                    [
                        'name'   => 'form-jenis-ternak',
                        'method' => 'post',
                        'id'     => 'form-jenis-ternak',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Ternak';
                        foreach ($data['jenis_ternak'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('jenis_ternak', $options, set_value('jenis_ternak', NULL, FALSE), 'id="jenis_ternak" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
                <?php
                    echo form_open('registrasi/search', 
                    [
                        'name'   => 'form-tipe-kandang',
                        'method' => 'post',
                        'id'     => 'form-tipe-kandang',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        foreach ($data['tipe_kandang'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_kandang', $options, set_value('tipe_kandang', NULL, FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:void(0);" onclick="reset();" class="btn btn-default">
                           <i class="fa fa-refresh"></i>
                        </a>
                        <!-- <a href="<?php echo site_url('registrasi/export');?>?<?php echo url_create_return_query();?>">
                           <i class="fa fa-edit"></i>
                        </a> -->
                    </div>

                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools">
                    <?php
                    echo form_open('registrasi/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        
        <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>No Registrasi</th>
                        <th>Tanggal Registrasi</th>
                        <th>Nomor Kamar</th>
                        <th>Berat Masuk</th>
                        <th>Tipe Ternak</th>
                        <th>Jenis Ternak</th>
                        <th>Tipe Kandang</th>
                        <th>QR Code</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['registrasi']['records'] as $idx => $record)
                        {?>
                    <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->REGISTER_NO);?></td>
                            <td><?php echo date_reformat($record->REGISTER_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->ROOM_NUMBER;?></td>
                            <td><?php echo $record->WEIGHT_IN;?></td>
                            <td><?php echo $record->TIPE_TERNAK;?></td>
                            <td><?php echo $record->JENIS_TERNAK;?></td>
                            <td><?php echo $record->TIPE_KANDANG;?></td> 
                            <td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$record->IMAGE;?>"></td></td>                
                            <td class="text-center">
                                <?php 
                                  if(is_privilege(PRIVILEGE_REGISTRASI,PRIVILEGE_UPDATE)) { ?>      
                                    <a href="<?php echo site_url('registrasi/form/' . $record->ID_FARM);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Ganti Kandang <?php echo html_escape($record->REGISTER_NO);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                <?php
                                  }
                                ?>
                                <?php 
                                  if(is_privilege(PRIVILEGE_REGISTRASI,PRIVILEGE_DETAIL)) { ?> 
                                    <a class="btn btn-flat btn-sm  btn-logs"
                                        data-toggle="tooltip" data-placement="bottom"
                                        data-message="<?php echo html_escape($record->REGISTER_NO);?>"
                                        title="Melihat Logs <?php echo html_escape($record->REGISTER_NO);?>">
                                        <i class="fa fa-file-text text-danger"></i>
                                    </a>   
                                <?php
                                  } 
                                ?>
                            </td>
                        </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['registrasi']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-delete">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive no-padding" id="modal-table">
                    
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Close" data-dismiss="modal" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('#tipe_ternak').change(function(){
            $('#ternak').submit();
        });

        $('#jenis_ternak').change(function(){
            $('#form-jenis-ternak').submit();
        });
        
        $('#tipe_kandang').change(function(){
            $('#form-tipe-kandang').submit();
        });
        
        $('.btn-logs').click(function(e) {
            e.preventDefault();

            let rowData = $(this);
            let modalRemove = $('.modal-delete');
        
            $('.modal-title', modalRemove).text(rowData.attr('data-original-title'));
            $.ajax({
                url: '<?php echo site_url('registrasi/getLogKandang'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: rowData.attr('data-message')},
                success: function(d) {
                    var HTML = "";
                    HTML = "<table class='table table-hover'><tr><th>No Registrasi</th><th>Kandang Sebelumnya</th><th>Kandang Baru</th><th>Tanggal Proses </th></tr>";
                    $.each( d, function( key, value ) {
                        HTML +=['<tr><td>'+value['noregistrasi']+'</td><td>'+value['from']+'</td><td>'+value['descr']+'</td><td>'+value['dtm_crt']+'</td></tr>'];
                    });
                    HTML += "</table>";
                    $('#modal-table').html(HTML);
                    // $('#table').attr('id');
                }
            });
            
            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        
    };
    function reset() {        
        $('#tipe_ternak').val(null).trigger('change');
        $('#jenis_ternak').val(null).trigger('change');
        $('#tipe_kandang').val(null).trigger('change');
    }
</script>
