<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                    <?php 
                    if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_CREATE)) { ?>
                        <a class="btn btn-block btn-primary" href="<?php echo site_url('registrasi/form_order');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                    <?php
                    } 
                    ?>    
                </div>
                <div class="col-md-2">
                    <?php 
                    if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_DETAIL)) { ?>
                        <a class="btn btn-block btn-primary" href="<?php echo site_url('registrasi/listPo');?>?<?php echo url_create_return_query();?>">Print PO & Receive</a>        
                    <?php
                    } 
                    ?>    
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama Vendor</th>
                        <th>Tanggal Order</th>
                        <th>Tanggal Estimasi Kedatangan</th>
                        <th>Tanggal Actual Kedatangan</th>
                        <th>Tipe Farm</th>
                        <th>Jenis Farm</th>
                        <th>Quantity KG</th>
                        <th>Harga KG</th>
                        <th>Total </th>
                        <th>Biaya Lain</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['order_farm']['records'] as $idx => $record)
                        {
                    ?>
                         <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->VENDOR_NAME);?></td>
                            <td><?php echo date_reformat($record->ORDER_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo date_reformat($record->ESTIMATION_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo date_reformat($record->ACTUAL_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo html_escape($record->NAME_TYPE_FARM);?></td>
                            <td><?php echo html_escape($record->NAME_JENIS_FARM);?></td>
                            <td><?php echo $record->QTY;?></td>   
                            <td><?php echo $record->PRICE_KG;?></td>
                            <td><?php echo $record->TOTAL;?></td>
                            <td><?php echo $record->BIAYA_LAIN;?></td>
                            <td><?php echo $record->STATUS;?></td>
                            <td class="text-center">
                            <?php 
                                if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_UPDATE)) { 
                                    if ($record->STATUS==='Order') {  
                                ?>
                                    <a href="<?php echo site_url('registrasi/form_order/' . $record->ID_FARM_DETAIL);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Mengubah">
                                        <i class="fa fa-edit"></i>
                                    </a>
                            <?php
                                    }
                                } 
                            ?>
                            </td>
                         </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['order_farm']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>