<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama Vendor</th>
                        <th>Tanggal Order</th>
                        <th>Estimasi Kedatangan</th>
                        <th>QTY</th>
                        <th>Total Harga KG</th>
                        <th>Total Biaya Lain</th>
                        <th>Total </th>
                        <th>Status </th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['order_farm_po']['records'] as $idx => $record)
                        {?>
                    <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->VENDOR_NAME);?></td>
                            <td><?php echo date_reformat($record->ORDER_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo date_reformat($record->ESTIMATION_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->QTY;?></td>   
                            <td><?php echo $record->PRICE_KG;?></td>
                            <td><?php echo $record->BIAYA_LAIN;?></td>
                            <td><?php echo $record->TOTAL;?></td>
                            <td><?php echo $record->STATUS;?></td>
                            <td class="text-center">
                                <a href="<?php echo site_url('registrasi/pdf_po/'.$record->ID_VENDOR.'/'.$record->ORDER_DATE.'/'.$record->ORDER_NO);?>?<?php echo url_create_return_query();?>"
                                    class="btn btn-flat btn-sm" target="_blank"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="Print PO <?php echo html_escape($record->VENDOR_NAME);?>">
                                    <i class="fa fa-file-pdf-o"></i>
                                </a>
                                <?php 
                                    if($record->STATUS == "Print Po") { ?>
                                    <a href="<?php echo site_url('registrasi/receive/'.$record->ORDER_NO);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-primary btn-receive"        
                                       data-toggle="tooltip" data-placement="bottom"
                                       data-message="<?php echo $record->ORDER_NO;?>"
                                       title="Receive <?php echo html_escape($record->VENDOR_NAME);?>">
                                       <i class="fa fa-thumbs-up"></i>
                                    </a>
                                <?php    
                                    }
                                ?>
                            </td>
                        </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['order_farm_po']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>