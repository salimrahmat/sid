<?php
echo form_open(($data['flow_data']['id_farm'] !==NULL ? 'registrasi/updatecage/' . $data['flow_data']['id_farm'] : 'registrasi/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php 
            foreach ($data['flow_data']['value'] as $gagal) {
              echo $gagal;
            } 
        ?>
        </div>   
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="jenis_domba">Tipe Kandang</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        if ($data['tipe_kandang'] !== NULL ){
                            foreach ($data['tipe_kandang'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('tipe_kandang', $options, set_value('tipe_kandang', ($data['flow_data']['tipe_kandang'] !==NULL ? $data['flow_data']['tipe_kandang'] : NULL), FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exis_kandang">Kandang Sebelumnya</label>
                        <?php
                        echo form_input([
                            'name'          => "exis_kandang",
                            'placeholder'   => "Kandang Sebelumnya",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('exis_kandang', ($data['flow_data']['exis_kandang'] !== NULL ? $data['flow_data']['exis_kandang'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="no_registrasi">No Registrasi</label>
                        <?php
                        echo form_input([
                            'name'          => "no_registrasi",
                            'placeholder'   => "No Registrasi",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('no_registrasi', ($data['flow_data']['no_registrasi'] !== NULL ? $data['flow_data']['no_registrasi'] : NULL), FALSE)
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    };
</script>