<?php
echo form_open(('registrasi/proses_receive/' . $data['flow_data']['order_no']), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
$orders = $data['orders'];
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php 
            foreach ($data['flow_data']['value'] as $gagal) {
              echo $gagal;
            } 
        ?>
        </div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="actual_date">Actual Kedatangan</label>
                    <?php
                    echo form_input([
                        'name'              => "actual_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Actual Date",
                        'required'          => 'required',
                        'value'             => set_value('actual_date', NULL, FALSE)
                    ]);?>
                </div>
            </div>
        </div>
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="type_farm">Tipe Domba</label>
                        <?php
                        echo form_input([
                            'name'          => "type_farm[]",
                            'placeholder'   => "Tipe Domba",
                            'class'         => "form-control",
                            'id'            => "type_farm",
                            'readonly'      => 'readonly'
                        ]);?>
                    </div>
                    <input type="hidden" name="type_farm_1[]">
                    <div class="form-group">
                        <label for="jenis_farm">Jenis Domba</label>
                        <?php
                        echo form_input([
                            'name'          => "jenis_farm[]",
                            'placeholder'   => "Jenis Farm",
                            'class'         => "form-control",
                            'id'            => "jenis_farm",
                            'readonly'      => 'readonly'
                        ]);?>
                    </div>
                    <input type="hidden" name="jenis_farm_1[]"> 
                    <div class="form-group">
                        <label for="noregis">No Registrasi</label>
                        <?php
                        echo form_input([
                            'name'          => "noregis[]",
                            'placeholder'   => "No Registrasi",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'maxlength'     => '11',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="register_date">Tanggal Registrasi</label>
                        <?php
                        echo form_input([
                            'name'              => "register_date[]",
                            'class'             => "form-control",
                            'placeholder'       => "Tanggal Registrasi",
                            'required'          => 'required',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="weight">Berat Masuk</label>
                        <?php
                        echo form_input([
                            'name'          => "weight[]",
                            'placeholder'   => "Berat Masuk",
                            'class'         => "form-control",
                            'type'          => "decimal",
                            'required'      => 'required',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="jenis_domba">Tipe Kandang</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        if ($data['tipe_kandang'] !== NULL ){
                            foreach ($data['tipe_kandang'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('tipe_kandang[]', $options, set_value('tipe_kandang', NULL, FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="noroom">Nomor Kamar</label>
                        <?php
                        echo form_input([
                            'name'          => "noroom[]",
                            'placeholder'   => "Nomor Kamar",
                            'class'         => "form-control",
                            'type'          => "number",
                            'required'      => 'required',
                        ]);?>
                    </div>
                </div>
            </div>
        </div>   
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                <i class="fa fa-undo"></i> Batal
            </a>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    window.onload = function(event) {
        tableinit();
        tableFirst();
        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });
    };

    var trbase = null;
    function tableinit(){
        var tr = $('.table-row');
        trbase = tr.clone(false, false);
        tr.remove();
    }

    function tableFirst(){
        <?php
        if (count($orders) > 0)
        {
            foreach ($orders as $order)
            {?>
            tableAdd(<?php echo json_encode($order);?>);
            <?php
            }
        }?>
    }

    var idx=0;
    function tableAdd(order) {
        let trNew = trbase.clone();
        $('.table-body').append(trNew);
        console.log(order);
        if (order !== null) {
            $('[name="type_farm[]"]',trNew).val(order.name_type_farm);
            $('[name="jenis_farm[]"]', trNew).val(order.name_jenis_farm);            
            $('[name="register_date[]"]', trNew).attr('id','datepicker_'+idx);
            $('[name="type_farm_1[]"]', trNew).val(order.type_farm);;
            $('[name="jenis_farm_1[]"]', trNew).val(order.jenis_farm);;
             
        }
        $('#datepicker_'+idx).datepicker({
            autoclose: true
        })

        //Date picker
        $('#datepicker_'+idx).datepicker({
            autoclose: true
        })

        idx++;
    }
</script>