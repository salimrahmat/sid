<?php
$orders = $data['orders'];
echo form_open(($data['flow_data']['id_order_farm'] !==NULL ? 'registrasi/update_order/' . $data['flow_data']['id_order_farm'] : 'registrasi/save_order'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                    echo $gagal;
                } 
            ?>
        </div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="id_vendor">Nama Vendor</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Vendor';
                    if ($data['id_vendor'] !== NULL ){
                        foreach ($data['id_vendor'] as $row) {
                          $options[$row['master_code']] = $row['descr'];
                        }
                    }
                    echo form_dropdown('id_vendor', $options, set_value('id_vendor', ($data['flow_data']['id_vendor'] !==NULL ? $data['flow_data']['id_vendor'] : NULL), FALSE), 'id="id_vendor" class="form-control select2 id_vendor" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="vendor_address">Alamat Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_address",
                        'placeholder'   => "Alamat Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_address",
                        'readonly'      => 'readonly',
                        'value'         => set_value('vendor_address', ($data['flow_data']['alamat_vendor'] !== NULL ? $data['flow_data']['alamat_vendor'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="vendor_phone">Phone Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_phone",
                        'placeholder'   => "Phone Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_phone",
                        'readonly'      => 'readonly',
                        'value'         => set_value('phone_vendor', ($data['flow_data']['phone_vendor'] !== NULL ? $data['flow_data']['phone_vendor'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="vendor_pic">Pic Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_pic",
                        'placeholder'   => "Pic Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_pic",
                        'readonly'      => 'readonly',
                        'value'         => set_value('pic_vendor', ($data['flow_data']['pic_vendor'] !== NULL ? $data['flow_data']['pic_vendor'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="tax">Tax</label>
                    <br>
                    <?php
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "Ya",   
                            'checked'       => ($data['flow_data']['tax'] == 1 ? TRUE : FALSE),  
                            'class'         => 'privilege-radio',
                            'required'      => 'required',
                            'id'            => 'taxyes'
                        ]);
                        echo form_label('Ya');
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "No",
                            'checked'       => ($data['flow_data']['tax'] == 1 ? FALSE : TRUE),
                            'required'      => 'required',
                            'class'         => 'privilege-radio',
                            'id'            => 'taxno'
                        ]);
                        echo form_label('Tidak');
                    ?>
                </div>
                <div class="form-group">
                    <label for="tax_value">Tax Value</label>
                    <?php
                    echo form_input([
                        'name'              => "tax_value",
                        'class'             => "form-control",
                        'placeholder'       => "tax value",
                        'readonly'          => 'readonly',
                        'value'             => set_value('tax_value', ($data['flow_data']['tax_value'] !== NULL ? $data['flow_data']['tax_value'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="order_date">Tanggal Order</label>
                    <?php
                    echo form_input([
                        'name'              => "order_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Tanggal Order",
                        'required'          => 'required',
                        'value'             => set_value('order_date', ($data['flow_data']['order_date'] !== NULL ? $data['flow_data']['order_date'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                        <label for="estimation_date">Estimasi Kedatangan</label>
                        <?php
                        echo form_input([
                            'name'              => "estimation_date",
                            'class'             => "form-control",
                            'id'                => "datepicker1",
                            'placeholder'       => "Tanggal Order",
                            'required'          => 'required',
                            'value'             => set_value('estimation_date', ($data['flow_data']['estimation_date'] !== NULL ? $data['flow_data']['estimation_date'] : NULL), FALSE)
                        ]);?>
                </div>
            </div>
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="tipe_ternak">Tipe Ternak</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Ternak';
                        if ($data['tipe_ternak'] !== NULL ){
                            foreach ($data['tipe_ternak'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('tipe_ternak[]', $options, set_value('tipe_ternak', ($data['flow_data']['tipe_ternak'] !==NULL ? $data['flow_data']['tipe_ternak'] : NULL), FALSE), 'id="tipe_ternak" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="jenis_ternak">Jenis Ternak</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Ternak'; 
                        if ($data['jenis_ternak'] !== NULL ){
                            foreach ($data['jenis_ternak'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        } 
                        echo form_dropdown('jenis_ternak[]', $options, set_value('jenis_ternak', ($data['flow_data']['jenis_ternak'] !==NULL ? $data['flow_data']['jenis_ternak'] : NULL), FALSE), 'id="jenis_ternak" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <?php
                        echo form_input([
                            'name'          => "qty[]",
                            'placeholder'   => "Quantity KG",
                            'class'         => "form-control",
                            'id'            => "qty",
                            'type'          => "decimal",
                            'required'      => 'required',
                        ]);?>
                    </div> 
                    <div class="form-group">
                        <label for="price_farm">Harga</label>
                        <?php
                        echo form_input([
                            'name'              => "price_farm[]",
                            'class'             => "form-control",
                            'placeholder'       => "Harga PerKilo",
                            'id'                => "price_farm",
                            'required'          => 'required',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="total">Total</label>
                        <?php 
                        echo form_input([
                            'name'              => "total[]",
                            'class'             => "form-control",
                            'placeholder'       => "total",
                            'id'                => "total",
                            'required'          => 'required',
                            'readonly'          => 'readonly',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="biaya_lain">Biaya Lain</label>
                        <?php 
                        echo form_input([
                            'name'              => "biaya_lain[]",
                            'class'             => "form-control",
                            'placeholder'       => "Biaya Lain",
                            'required'          => 'required',
                        ]);?>
                    </div>
                    <?php 
                        if (count($orders) == 0) { ?>
                            <a  class="remove text-danger" title="Hapus">
                                <span class="fa fa-trash"></span>
                            </a>   
                    <?php
                        }
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <?php 
                    if (count($orders) == 0) { ?>
                        <a  class="addMore" title="Tambah">
                            <span class="fa fa-plus"></span>
                        </a> 
                <?php 
                    }
                ?>        
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>  
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
 
    let formForm = $('form[name="form-input"]');

    formForm.submit(function() {
        InputHelper.input_release($(this));
        return true;
    });

    window.onload = function(event) {
        tableInit();
        tableFirst();
        $('.addMore').on('click', function() {
            tableAdd(null);
        });
        $('.id_vendor').on("select2:select", function(e) { 
                $.ajax({
                    url: '<?php echo site_url('order_pakan/detailVendor'); ?>',
                    type: "post",
                    dataType  : 'json',
                    data: {searchTerm: e.params.data.id},
                    success: function(d) {
                        $.each( d, function( i, val ) {
                            $('#vendor_address').val(val['address']);
                            $('#vendor_phone').val(val['phone']);
                            $('#vendor_pic').val(val['pic']);
                        });
                    }
                });
        });
    }

  <?php 
    if ( count($data['flow_data']['value']) > 0) { ?>
        $('.alert-dismissible').show();
    <?php
    } else { ?>
        $('.alert-dismissible').hide();
   <?php    
    }
   ?>

    $('#taxyes').click(function() {
            $.ajax(
                {url: '<?php echo site_url('order_pakan/getSelectTax')?>', 
                 success: function(result){
                    $("input[name='tax_value']").val(result);
                }
            });
    });

    $('#taxno').click(function() {
        $("input[name='tax_value']").val('');
    });

    var trbase = null;
    function tableInit(){
        var tr = $('.table-row');
        trbase = tr.clone(false, false);
        tr.remove();
    }

    function tableFirst(){
        <?php
        if (count($orders) > 0)
        {
        foreach ($orders as $order)
        {?>
        tableAdd(<?php echo json_encode($order);?>);
        <?php
        }
        }
        else
        {?>
        tableAdd(null);
        <?php
        }?>
    }

    var idx=0;
    function tableAdd(order) {
        let trNew = trbase.clone();
        $('.table-body').append(trNew);

        if (order !== null) {
            $('[name="qty[]"]',trNew).val(order.qty);
            $('[name="price_farm[]"]', trNew).val(order.price_farm);
            $('[name="total[]"]', trNew).val(order.total);
            $('[name="biaya_lain[]"]', trNew).val(order.biaya_lain); 
            // $('[name="tipe_ternak[]"]', trNew)
            //         .prepend(
            //             $('<option>', { value : order.tipe_ternak.id })
            //                 .text(order.name_tipe_ternak.name)
            //         )
            //         .val(order.tipe_ternak.id);  
            // $('[name="jenis_ternak[]"]', trNew)
            //         .prepend(
            //             $('<option>', { value : order.jenis_ternak.id })
            //                 .text(order.name_jenis_ternak.name)
            // )
            // .val(order.jenis_ternak.id);        
 
        }
        tableAttach(trNew);
    }

    let idxEnabled=0;
    function tableAttach(trNew) {

        $("#price_farm", trNew).on("change", function(){
            $('#total', trNew).val($(this).val() * $('#qty', trNew).val());
        });

        $("#qty", trNew).on("change", function(){
            $('#total', trNew).val($(this).val() * $('#price_farm', trNew).val());
        });

        $('[name="tipe_ternak[]"]', trNew);
        $('[name="jenis_ternak[]"]', trNew);
        $('[name="qty[]"]', trNew);
        $('[name="price_farm[]"]', trNew);
        $('[name="total[]"]', trNew);
        $('[name="biaya_lain[]"]', trNew);
             

        $('.remove', trNew).click(function (e) {
            e.preventDefault();

            let tableRow = $(this).parent();
                tableRow.remove();
        });
    }

</script>