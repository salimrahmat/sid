<?php
echo form_open('general_setting/save', 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <?php 
            foreach ($data['flow_data']['value'] as $gagal) {
              echo $gagal;
            } 
        ?>
        </div>
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                  <div class="form-group">
                        <label for="setting_code">Tipe</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe';
                        if ($data['setting_code'] !== NULL ){
                            foreach ($data['setting_code'] as $row) {
                                     $options[$row['GENERAL_SETTING_CODE']] = $row['DESCR'];
                                }
                        }
                        echo form_dropdown('setting_code', $options, set_value('setting_code', ($data['flow_data']['setting_code'] !==NULL ? $data['flow_data']['setting_code'] : NULL), FALSE), 'id="setting_code" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                  </div>
                  <div class="form-group">
                        <label for="mastercode">Kode Master</label>
                        <?php
                        echo form_input([
                            'name'          => "mastercode",
                            'placeholder'   => "Kode Master",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'maxlength'     => '5'
                        ]);?>
                  </div>
                  <div class="form-group">
                        <label for="descr">Deskripsi</label>
                        <?php
                        echo form_input([
                            'name'          => "descr",
                            'placeholder'   => "Deskripsi",
                            'class'         => "form-control",
                            'required'      => 'required'
                        ]);?>
                  </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    };
</script>