<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_SETTING,PRIVILEGE_CREATE)) { ?> 
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('general_setting/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                <?php
                  } 
                ?>    
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Deskripsi</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['setting']['records'] as $idx => $record)
                        {?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->TIPE);?></td>
                            <td><?php echo html_escape($record->CODE);?></td>
                            <td><?php echo html_escape($record->DETAIL_TIPE);?></td>
                            <td><?php echo html_escape($record->STATUS);?></td>
                            <td class="text-center">
                            <?php 
                              if(is_privilege(PRIVILEGE_SETTING,PRIVILEGE_UPDATE)) { ?> 
                                <a class="btn btn-flat btn-sm btn-delete"
                                    data-href="<?php echo site_url('general_setting/update/' . $record->GENERAL_SETTING_ID);?>?<?php echo url_create_return_query();?>"
                                    class="btn btn-flat btn-sm" 
                                    data-message="Apakah Anda yakin Update <?php echo html_escape($record->DETAIL_TIPE);?>"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="Update <?php echo html_escape($record->DETAIL_TIPE);?>">
                                     <i class="fa fa-trash text-danger"></i>
                                </a>    
                            <?php
                              } 
                            ?>        
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                    echo $data['setting']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-delete">
    <div class="modal-dialog modal-danger modal-sm">
        <div class="modal-content">
<?php
echo form_open('', [
    'name'      => 'form-delete',
    'method'    => 'post']);?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>&nbsp;</p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Batal" data-dismiss="modal" />
                <input type="submit" class="btn btn-flat btn-outline" value="Update" />
            </div>
<?php
echo form_close();?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('.btn-delete').click(function(e) {
            e.preventDefault();

            let rowData = $(this);
            let modalRemove = $('.modal-delete');

            $('form[name="form-delete"]', modalRemove).attr("action", rowData.attr('data-href'));
            $('.modal-title', modalRemove).text(rowData.attr('data-original-title'));
            $('.modal-body p', modalRemove).text(rowData.attr('data-message'));

            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });
    }
</script>    