<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <?php
                        echo form_open('penjualan_domba/lists', 
                        [
                            'name'   => 'form-search-ternak',
                            'method' => 'post',
                            'id'     => 'ternak',
                            ]); 
                    ?>
                    <div class="col-xs-2">
                        <?php
                        echo form_input([
                            'name'        => "outside_date1",
                            'class'       => "form-control",
                            'id'          => "datepicker",
                            'placeholder' => "Tanggal Keluar",
                        ]);?>
                    </div>
                    <div class="col-xs-2">
                        <?php
                        echo form_input([
                            'name'        => "outside_date2",
                            'class'       => "form-control",
                            'id'          => "datepicker1",
                            'placeholder' => "Tanggal Keluar",
                        ]);?>        
                    </div>
                    <div class="col-xs-2">
                        <button type="submit" class="btn btn-primary">Cari</button>    
                    </div>
                    <?php echo form_close(); ?>
                </div>     
            </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>No Registrasi</th>
                    <th>Tanggal Keluar</th>
                    <th>Berat keluar</th>
                    <th>Berat konsumen</th>
                    <th>Jenis Jual</th>
                    <th>Tipe ternak</th>
                    <th>Jenis ternak</th>
                    <th>Tipe kandang</th>
                    <th>Proses jual</th>
                </tr>
                    <?php
                        foreach ($data['penjualan']['records'] as $idx => $record) 
                    { ?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->REGISTER_NO);?></td>
                            <td><?php echo date_reformat($record->tanggal_keluar, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->WEIGHT_CAGE;?></td>
                            <td><?php echo $record->WEIGHT_CUSTOMER;?></td>
                            <td><?php echo $record->jenis_jual;?></td>
                            <td><?php echo $record->type_ternak;?></td>
                            <td><?php echo $record->jenis_ternak;?></td>   
                            <td><?php echo $record->tipe_kandang;?></td>   
                            <td><?php echo date_reformat($record->proses_jual, 'j M Y H:i:s', '&ndash;')?></td>   
                        </tr>
                    <?php 
                        }
                    ?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php
              echo $data['penjualan']['pagination'];
            ?>
        </div>
     </div>
</div>