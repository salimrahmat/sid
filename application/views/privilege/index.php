<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <?php
                echo form_open('privilege/search', 
                [
                    'name'   => 'form-search-privilege',
                    'method' => 'post',
                    'id'     => 'privilege',
                    ]); 
                ?>
                <div class="col-md-2">
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Tipe Akses';
                    foreach ($data['access_user'] as $row) {
                                $options[$row['master_code']] = $row['descr'];
                    }
                    echo form_dropdown('access_user', $options, set_value('access_user', NULL, FALSE), 'id="access_user" class="form-control select2" data-width="100%"  required="required"');
                    ?>
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools">
                    <?php
                    echo form_open('privilege/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>User Akses</th>
                        <th>Nama Menu</th>
                        <th>List</th>
                        <th>Add</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Detail</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['detail']['records'] as $idx => $record)
                        {?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo $record->DESCR;?></td>
                            <td><?php echo html_escape($record->MENU_NAME);?></td>
                            <td><?php echo ($record->MENU_LIST==="1" ? 'Y' : 'N');?></td>
                            <td><?php echo ($record->MENU_ADD==="1" ? 'Y' : 'N');?></td>
                            <td><?php echo ($record->MENU_EDIT==="1" ? 'Y' : 'N');?></td>
                            <td><?php echo ($record->MENU_DELETE==="1" ? 'Y' : 'N');?></td>
                            <td><?php echo ($record->MENU_DETAIL==="1" ? 'Y' : 'N');?></td>
                            <td class="text-center">
                                <a href="<?php echo site_url('privilege/form/' . $record->ID_ACCESS_MENU);?>?<?php echo url_create_return_query();?>"
                                    class="btn btn-flat btn-sm"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="Edit Kandang <?php echo html_escape($record->MENU_NAME);?>">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    <?php 
                        }
                    ?>            
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['detail']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('#access_user').change(function(){
            $('#privilege').submit();
        });
    }
    function reset() {        
        $('#access_user').val(null).trigger('change');
    }
</script>    