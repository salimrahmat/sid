<?php
echo form_open(($data['detail']['id'] !==NULL ? 'privilege/update/' . $data['detail']['id'] : 'privilege/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="useraccess">User Akses</label>
                        <?php
                        echo form_input([
                            'name'          => "useraccess",
                            'placeholder'   => "User Akses",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('noregis', ($data['detail']['descr'] !== NULL ? $data['detail']['descr'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="menuname">Nama Menu</label>
                        <?php
                        echo form_input([
                            'name'          => "menuname",
                            'placeholder'   => "Nama Menu",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('menuname', ($data['detail']['nama'] !== NULL ? $data['detail']['nama'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="list">List</label>
                    <br>
                        <?php
                            echo form_radio([
                                'name'          => "list",
                                'value'         => "1",   
                                'checked'       => ($data['detail']['list'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'listyes'
                            ]);
                            echo form_label('Ya');
                            echo form_radio([
                                'name'          => "list",
                                'value'         => "0",
                                'checked'       => ($data['detail']['list'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'listno'
                            ]);
                            echo form_label('Tidak');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="add">Add</label>
                    <br>
                        <?php
                            echo form_radio([
                                'name'          => "add",
                                'value'         => "1",   
                                'checked'       => ($data['detail']['add'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'addyes'
                            ]);
                            echo form_label('Ya');
                            echo form_radio([
                                'name'          => "add",
                                'value'         => "0",
                                'checked'       => ($data['detail']['add'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'addno'
                            ]);
                            echo form_label('Tidak');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="edit">Edit</label>
                    <br>
                        <?php
                            echo form_radio([
                                'name'          => "edit",
                                'value'         => "1",   
                                'checked'       => ($data['detail']['edit'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'edityes'
                            ]);
                            echo form_label('Ya');
                            echo form_radio([
                                'name'          => "edit",
                                'value'         => "0",
                                'checked'       => ($data['detail']['edit'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'editno'
                            ]);
                            echo form_label('Tidak');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="delete">Delete</label>
                    <br>
                        <?php
                            echo form_radio([
                                'name'          => "delete",
                                'value'         => "1",   
                                'checked'       => ($data['detail']['delete'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'deleteyes'
                            ]);
                            echo form_label('Ya');
                            echo form_radio([
                                'name'          => "delete",
                                'value'         => "0",
                                'checked'       => ($data['detail']['delete'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'deleteno'
                            ]);
                            echo form_label('Tidak');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="detail">Detail</label>
                    <br>
                        <?php
                            echo form_radio([
                                'name'          => "detail",
                                'value'         => "1",   
                                'checked'       => ($data['detail']['detail'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'detailyes'
                            ]);
                            echo form_label('Ya');
                            echo form_radio([
                                'name'          => "detail",
                                'value'         => "0",
                                'checked'       => ($data['detail']['detail'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'detailno'
                            ]);
                            echo form_label('Tidak');
                        ?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>