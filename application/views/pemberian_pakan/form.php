<?php
echo form_open(($data['flow_data']['id_food_farm'] !==NULL ? 'pemberian_pakan/update/' . $data['flow_data']['id_food_farm'] : 'pemberian_pakan/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('pemberian_pakan/form')]); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="food_date">Tanggal Pemberian Pakan</label>
                    <?php
                    echo form_input([
                        'name'          => "food_date",
                        'class'         => "form-control",
                        'id'            => "datepicker",
                        'placeholder'   => "Tanggal Pemberian Pakan",
                        'required'      => 'required',
                        'value'         => set_value('food_date', ($data['flow_data']['food_date'] !== NULL ? $data['flow_data']['food_date'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="time_food">Jadwal Pemberian Pakan</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Jadwal';
                    if ($data['jadwal_pakan'] !== NULL ){
                        foreach ($data['jadwal_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('time_food', $options, set_value('time_food', ($data['flow_data']['time_food'] !==NULL ? $data['flow_data']['time_food'] : NULL), FALSE), 'id="time_food" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="type_food">Jenis Pakan</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Jenis Pakan';
                    if ($data['jenis_pakan'] !== NULL ){
                        foreach ($data['jenis_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('type_food', $options, set_value('type_food', ($data['flow_data']['type_food'] !==NULL ? $data['flow_data']['type_food'] : NULL), FALSE), 'id="type_food" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="tipe_kandang_pakan">Tipe Kandang</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Tipe Kandang';
                    if ($data['tipe_kandang'] !== NULL ){
                        foreach ($data['tipe_kandang'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('tipe_kandang_pakan', $options, set_value('tipe_kandang_pakan', ($data['flow_data']['tipe_kandang_pakan'] !==NULL ? $data['flow_data']['tipe_kandang_pakan'] : NULL), FALSE), 'id="tipe_kandang_pakan" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="total_weight">Total Berat Badan</label>
                    <?php
                    echo form_input([
                        'name'          => "total_weight",
                        'placeholder'   => "Total Berat Badan",
                        'class'         => "form-control",
                        'required'      => 'required',
                        'readonly'      => 'readonly',
                        'value'         => set_value('total_weight', ($data['flow_data']['total_weight'] !== NULL ? $data['flow_data']['total_weight'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="total_pakan">Total pakan</label>
                    <?php
                    echo form_input([
                        'name'          => "total_pakan",
                        'placeholder'   => "Total Pakan",
                        'class'         => "form-control",
                        'type'          => "decimal",
                        'required'      => 'required',
                        'value'         => set_value('total_pakan', ($data['flow_data']['total_pakan'] !== NULL ? $data['flow_data']['total_pakan'] : NULL), FALSE)
                    ]);?>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {
        let formForm = $('form[name="form-input"]');
        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        $('#tipe_kandang_pakan').on("select2:select", function(e) {
            $.ajax({
                url: '<?php echo site_url('pemberian_pakan/getTotalWeight'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: e.params.data.id},
                success: function(d) {
                    $('[name="total_weight"]').val(JSON.parse(d));
                }
            });
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    }
</script>