<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_PEMBERIAN_PAKAN,PRIVILEGE_CREATE)) { ?>  
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('pemberian_pakan/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                <?php
                  }
                ?>
                </div>
                <?php
                    echo form_open('pemberian_pakan/search', 
                    [
                        'name'   => 'jadwal',
                        'method' => 'post',
                        'id'     => 'from-jadwal-pakan',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jadwal Pakan';
                        foreach ($data['jadwal_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('jadwal_pakan', $options, set_value('jadwal_pakan', NULL, FALSE), 'id="jadwal_pakan" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
                <?php
                    echo form_open('pemberian_pakan/search', 
                    [
                        'name'   => 'pakan',
                        'method' => 'post',
                        'id'     => 'form-jenis-pakan',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Pakan';
                        foreach ($data['jenis_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('jenis_pakan', $options, set_value('jenis_pakan', NULL, FALSE), 'id="jenis_pakan" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div> 
                <?php echo form_close(); ?>
                <?php
                    echo form_open('pemberian_pakan/search', 
                    [
                        'name'   => 'form-tipe-kandang',
                        'method' => 'post',
                        'id'     => 'form-tipe-kandang',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        foreach ($data['tipe_kandang'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_kandang', $options, set_value('tipe_kandang', NULL, FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%"  required="required"');
                        ?> 
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:void(0);" onclick="reset();" class="btn btn-default">
                           <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
        <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Tanggal Pemberian Pakan</th>
                        <th>Jadwal Pemberian Pakan</th>
                        <th>Jenis Pakan</th>
                        <th>Tipe Kandang</th>
                        <th>Berat Badan</th>
                        <th>Total Pakan</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['pemberian_pakan']['records'] as $idx => $record)
                        {?>
                    <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo date_reformat($record->FOOD_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo html_escape($record->SCEDULE_FOOD);?></td>
                            <td><?php echo html_escape($record->TYPE_FOOD);?></td>
                            <td><?php echo html_escape($record->TYPE_CAGE);?></td>
                            <td><?php echo $record->TOTAL_WEIGHT;?></td>
                            <td><?php echo $record->TOTAL_FOOD;?></td>                
                            <td class="text-center">
                            <?php 
                                if(is_privilege(PRIVILEGE_PEMBERIAN_PAKAN,PRIVILEGE_UPDATE)) { ?> 
                                    <a href="<?php echo site_url('pemberian_pakan/form/' . $record->ID_FOOD_FARM);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Mengubah ">
                                        <i class="fa fa-edit"></i>
                                    </a>
                            <?php 
                                }
                            ?>
                            </td>
                        </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['pemberian_pakan']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.onload = function(event) {
        $('#jadwal_pakan').change(function(){
            $('#from-jadwal-pakan').submit();
        });

        $('#jenis_pakan').change(function(){
            $('#form-jenis-pakan').submit();
        });
        
        $('#tipe_kandang').change(function(){
            $('#form-tipe-kandang').submit();
        });
    };
    function reset() {        
        $('#jadwal_pakan').val(null).trigger('change');
        $('#jenis_pakan').val(null).trigger('change');
        $('#tipe_kandang').val(null).trigger('change');
    }
</script>
  