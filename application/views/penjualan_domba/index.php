<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <?php 
                  if(is_privilege(PRIVILEGE_PENJUALAN,PRIVILEGE_DETAIL)) { ?>
                    <a href="<?php echo site_url('penjualan_domba/cart');?>?<?php echo url_create_return_query();?>"
                        class="btn btn-flat btn-sm"
                        data-toggle="tooltip" data-placement="bottom"
                        title="Melihat Keranjang">
                        <i class="fa fa-shopping-cart"></i> 
                    </a>
                <?php
                  } 
                ?>
                <div class="box-tools">
                    <?php
                    echo form_open('penjualan_domba/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div class="row">
                <?php
                    foreach ($data['penjualan_list']['records'] as $idx => $record)
                    {?>
                    <a href="<?php echo site_url('penjualan_domba/detail/' . $record->REGISTER_NO);?>?<?php echo url_create_return_query();?>">
                        <div class="col-md-4">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-yellow">
                                    <h3 class="widget-user-username">Farm Corporation</h3>
                                    <!-- <h5 class="widget-user-desc">Lead Developer</h5> -->
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="<?php echo site_url('penjualan_domba/detail/' . $record->REGISTER_NO);?>?<?php echo url_create_return_query();?>">No Registrasi <span class="pull-right"><?php echo $record->REGISTER_NO;?></span></a></li>
                                        <li><a href="<?php echo site_url('penjualan_domba/detail/' . $record->REGISTER_NO);?>?<?php echo url_create_return_query();?>">Tipe Ternak <span class="pull-right"><?php echo $record->TYPE_TERNAK;?></span></a></li>
                                        <li><a href="<?php echo site_url('penjualan_domba/detail/' . $record->REGISTER_NO);?>?<?php echo url_create_return_query();?>">Jenis Ternak <span class="pull-right"><?php echo $record->JENIS_TERNAK;?></span></a></li>
                                        <li><a href="<?php echo site_url('penjualan_domba/detail/' . $record->REGISTER_NO);?>?<?php echo url_create_return_query();?>">Berat Masuk <span class="pull-right"><?php echo $record->WEIGHT_IN;?></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php
                    } 
                ?>    
        </div>
        <div class="box-footer clearfix">
            <?php
            echo $data['penjualan_list']['pagination'];
            ?>
        </div>
    </div>
</div>