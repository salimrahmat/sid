<div class="row">
    <div class="col-md-12">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pembelian</h3>
                <div class="box-tools">
                    <?php
                    echo form_open('penjualan_domba/search', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>No Order</th>
                  <th>Tanggal Keluar</th>
                  <th>No Invoice </th>
                  <th>Non Jogrog</th>
                  <th>Total Jogrog</th>
                  <th>Konfirmasi Pembelian</th>
                  <th></th>
                </tr>
                <?php 
                    foreach($data['penjualan_list_jual']['records']  as $idx => $record) {                    
                      if ($record->CONFIRM_SALE==="N" || $record->CONFIRM_SALE==="C") {
                          $label="label-danger";
                      } else {
                          $label="label-success";
                      }
                      
                      if ($record->CONFIRM_SALE==="Y") {
                          $detail="Create Invoice";
                      } elseif ($record->CONFIRM_SALE==="C") {
                          $detail="Cancel";
                      }
                ?>
                     <tr>
                        <td><?php echo $idx + 1; ?></td>
                        <td><?php echo $record->ORDER_NO;?></td>
                        <td><?php echo date_reformat($record->OUT_DATE, 'j M Y', '&ndash;'); ?></td>
                        <td><?php echo $record->INVOICE_NO;?></td>
                        <td><?php echo "Rp " . number_format($record->TOTAL_NON_JOGROG,2,',','.'); ?></td>
                        <td><?php echo "Rp " . number_format($record->TOTAL_JOGROG,2,',','.');?></td>
                        <td><span class="label <?php echo $label ?>"><?php echo $record->CONFIRM_SALE; ?></span> <?php echo $detail ?> </td>
                        <td>
                        <?php 
                            if(is_privilege(PRIVILEGE_LIST_PENJUALAN,PRIVILEGE_UPDATE)) { ?>
                                <a href="<?php echo site_url('penjualan_domba/print/' . $record->INVOICE_NO);?>?<?php echo url_create_return_query();?>"
                                        target="_blank"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Print Invoice <?php echo html_escape($record->INVOICE_NO);?>">
                                        <img style="width: 35px;" src="<?php echo base_url().'assets/images/invoice_img.png';?>">
                                </a>
                        <?php 
                         }
                         if ($record->CONFIRM_SALE ==="Y") {
                        ?>
                                <a data-href="<?php echo site_url('penjualan_domba/cancel/' . $record->INVOICE_NO);?>?<?php echo url_create_return_query();?>"
                                   class="btn btn-flat btn-invoice"
                                   data-toggle="tooltip" data-placement="bottom"
                                   title="Cancel <?php echo html_escape($record->INVOICE_NO);?>">
                                   <i class="fa fa-file-text text-danger"></i>
                                </a>
                        <?php
                         }
                        ?>
                        </td>
                     </tr>
                <?php
                    };
                ?>
              </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['penjualan_list_jual']['pagination'];
                ?>
            </div>
          </div>
    </div>
</div>
<div class="modal fade modal-invoice">
    <div class="modal-dialog modal-danger modal-sm">
        <div class="modal-content">
            <?php
                echo form_open('', [
                    'name'      => 'form-delete',
                    'method'    => 'post']);
            ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan Cancel Invoice ini?</p>
                    <div class="form-group">
                        <label for="alasan">Alasan</label>
                        <?php
                        echo form_textarea([
                            'name'          => "alasan",
                            'placeholder'   => "Alasan",
                            'class'         => "form-control",
                            'required'      => 'required',
                        ]);?>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Batal" data-dismiss="modal" />
                <input type="submit" class="btn btn-flat btn-outline" value="Proses" />
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('.btn-invoice').click(function(e) {
            e.preventDefault();

            let rowData = $(this);
            let modalRemove = $('.modal-invoice');

            $('form[name="form-delete"]', modalRemove).attr("action", rowData.attr('data-href'));
            // $('.modal-title', modalRemove).text(rowData.attr('data-original-title'));
            // $('.modal-body p', modalRemove).text(rowData.attr('data-message'));

            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });
    }
</script>