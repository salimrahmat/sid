
<?php
echo form_open(('penjualan_domba/cart'),
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('penjualan/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
                <div class="box-body table-body">
                    <div class="table-row">
                        <div class="form-group">
                            <label for="register_no">No Registrasi</label>
                            <?php
                            echo form_input([
                                'name'          => "register_no",
                                'placeholder'   => "No Registrasi",
                                'class'         => "form-control",
                                'required'      => 'required',
                                'maxlength'     => '11',
                                'readonly'      => 'readonly',
                                'value'         => set_value('register_no', ($data['detail']['register_no'] !== NULL ? $data['detail']['register_no'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="register_date">Tanggal Registrasi</label>
                            <?php
                            echo form_input([
                                'name'              => "register_date",
                                'class'             => "form-control",
                                'placeholder'       => "Tanggal Registrasi",
                                'required'          => 'required',
                                'readonly'          => 'readonly',
                                'value'             => set_value('register_date', ($data['detail']['register_date'] !== NULL ? $data['detail']['register_date'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="tipe_ternak">Tipe Ternak</label>
                            <?php
                            echo form_input([
                                'name'              => "tipe_ternak",
                                'class'             => "form-control",
                                'placeholder'       => "Tipe Ternak",
                                'required'          => 'required',
                                'readonly'          => 'readonly',
                                'value'             => set_value('tipe_ternak', ($data['detail']['tipe_ternak'] !== NULL ? $data['detail']['tipe_ternak'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="jenis_ternak">Jenis Ternak</label>
                            <?php
                            echo form_input([
                                'name'              => "jenis_ternak",
                                'class'             => "form-control",
                                'placeholder'       => "Jenis Ternak",
                                'required'          => 'required',
                                'readonly'          => 'readonly',
                                'value'             => set_value('jenis_ternak', ($data['detail']['jenis_ternak'] !== NULL ? $data['detail']['jenis_ternak'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="berat_masuk">Berat Masuk</label>
                            <?php
                            echo form_input([
                                'name'          => "berat_masuk",
                                'placeholder'   => "Berat Masuk",
                                'class'         => "form-control",
                                'type'          => "decimal",
                                'required'      => 'required',
                                'readonly'      => 'readonly',
                                'value'         => set_value('berat_masuk', ($data['detail']['berat_masuk'] !== NULL ? $data['detail']['berat_masuk'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="tipe_kandang">Tipe Kandang</label>
                            <?php
                            echo form_input([
                                'name'          => "tipe_kandang",
                                'placeholder'   => "Tipe Kandang",
                                'class'         => "form-control",
                                'required'      => 'required',
                                'readonly'      => 'readonly',
                                'value'         => set_value('tipe_kandang', ($data['detail']['tipe_kandang'] !== NULL ? $data['detail']['tipe_kandang'] : NULL), FALSE)
                            ]);?>    
                        </div>
                        <div class="form-group">
                            <label for="nomor_kamar">Nomor Kamar</label>
                            <?php
                            echo form_input([
                                'name'          => "nomor_kamar",
                                'placeholder'   => "Nomor Kamar",
                                'class'         => "form-control",
                                'type'          => "number",
                                'required'      => 'required',
                                'readonly'      => 'readonly',
                                'value'         => set_value('nomor_kamar', ($data['detail']['nomor_kamar'] !== NULL ? $data['detail']['nomor_kamar'] : NULL), FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="weight_cus">Berat Badan Di Customer</label>
                            <?php
                            echo form_input([
                                'name'          => "weight_cus",
                                'placeholder'   => "Berat Badan Di Customer",
                                'class'         => "form-control",
                                'type'          => "number",
                                'required'      => 'required',
                                'value'       => set_value('weight_cus', NULL, FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="jenis_jual">Jenis Jual</label>
                            <?php
                            $options = [];
                            $options[''] = 'Pilih Jenis Penjualan';
                            if ($data['jenis_jual'] !== NULL ){
                                foreach ($data['jenis_jual'] as $row) {
                                            $options[$row['master_code']] = $row['descr'];
                                    }
                            }
                            echo form_dropdown('jenis_jual', $options, set_value('jenis_jual', NULL, FALSE), 'id="jenis_jual" class="form-control select2" data-width="100%" required="required"');
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="price_kg">Harga Perkilo</label>
                            <?php
                            echo form_input([
                                'name'          => "price_kg",
                                'placeholder'   => "Harga Perkilo",
                                'class'         => "form-control price_kg",
                                'type'          => "number",
                                'readonly'      => "readonly",
                                'value'       => set_value('price_kg', NULL, FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="jogrog">Harga Jogrog Rp</label>
                            <?php
                            echo form_input([
                                'name'          => "jogrog",
                                'placeholder'   => "Jogrog",
                                'class'         => "form-control jogrog",
                                'type'          => "number",
                                'readonly'      => "readonly",
                                'value'         => set_value('jogrog', NULL, FALSE)
                            ]);?>
                        </div>
                        <div class="form-group">
                            <label for="discount">Discount</label>
                            <?php
                            echo form_input([
                                'name'          => "discount",
                                'placeholder'   => "discount",
                                'class'         => "form-control",
                                'type'          => "decimal",
                                'value'         => set_value('discount', NULL, FALSE)
                            ]);?>
                        </div>
                    </div>
                </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Order</button>
            <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                <i class="fa fa-undo"></i> Batal
            </a>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {
        $("#jenis_jual").on("select2:select", function (e) { 
            var select_val = $(e.currentTarget).val();
                    if (select_val =="JG") {
                        $(".price_kg").attr('readonly','readonly').val("0");
                        $(".jogrog").removeAttr('readonly');
                    } else {
                        $(".jogrog").attr('readonly','readonly').val("0");
                        $(".price_kg").removeAttr('readonly');
                    }
            });
    }
</script>