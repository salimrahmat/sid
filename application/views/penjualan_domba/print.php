<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Farm | Invoice</title>
  <style type="text/css" media="print">
       @media print {
        @page { margin: 5mm; }
        body { margin: 1.6cm; }
        }
        
  </style>
  <style>
    th, td {
          border: "1px solid black";
          border-collapse: "collapse";
        }
    th {
      text-align: "center";
    }    
  </style>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="print_Pdf()">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <div style="text-align: center; font-weight: bold;">
            <span>
            INVOICE
            </span><br>
            <span>
            No : <?php echo $member['invoice_no'] ?>
            </span>
          </div>
          <!-- <small class="pull-right">Tanggal Order: <?php echo date('d/m/Y');?></small> -->
        </h2>
      </div>
      <!-- /.col -->
    
    <div class="col-xs-12 table-responsive">
        <div class="col-xs-5">
            <address>
            <table> 
                <div class="pull-left image">
                    <img src="<?php echo base_url(); ?>/assets/images/orang.jpg" class="img-circle" alt="User Image" style="width:100px;height:100px;">
                </div>
                <tr style="vertical-align:top">
                <td>Company</td><td>:</td><td>Farm Corporation</td>
                </tr>
                <tr style="vertical-align:top">
                <td>Address</td><td>:</td><td><?php echo nl2br("Jl. Pramuka No Sekian Kel Aceh Jakarta Barat") ?></td>
                </tr style="vertical-align:top">
                <tr>
                <td>Phone/Fax</td><td>:</td><td><?php echo nl2br("021 12312321") ?></td>
                </tr>
                <tr style="vertical-align:top">
                <td>Pic</td><td>:</td><td><?php echo nl2br("Aden Kembalen") ?></td>
                </tr>
            </table>
            </address>
        </div>
        <div class="col-xs-2">
        </div>
        <div class="col-xs-5">
            <table> 
                <tr style="vertical-align:top">
                <td>Date </td><td>:</td><td><?php echo date('d/m/Y');?></td>
                </tr>
            </table>
            <address>
                <table>
                    <tr style="vertical-align:top">
                    <td>Bill To</td><td>:</td><td><?php echo $member['nama_member'];?></td>
                    </tr>
                    <tr style="vertical-align:top">
                    <td>Address</td><td>:</td><td><?php echo nl2br($member['alamat_member']);?></td>
                    </tr>
                    <tr style="vertical-align:top">
                    <td>Phone/Fax</td><td>:</td><td><?php echo nl2br($member['hp_member']);?></td>
                    </tr>
                </table>
            </address>
        </div>
    </div>
    <br>
        <div class="col-xs-12 table-responsive">
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <label class="pull-right" for="id_vendor">Currency : IDR</label>
                    <table style="width: 100%; border-collapse: collapse;border: 1px solid black;">
                        <thead>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">No</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">Nama Barang</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: left">Berat/Kg</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">Jenis</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">QTY</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">Discount</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">Harga</th>
                            <th style="border-collapse: collapse;border: 1px solid black; text-align: center">Total</th>
                        </thead>
                        <tbody>
                            <?php
                                $total=0;
                                $discount=0;
                                foreach($items as $idx =>$records) {                                  
                                    if ($records['TYPE_SALE']==="JG") {
                                         $total=$total+$records['TOTAL_JOGROG'];
                                    } else {
                                         $total=$total+($records['TOTAL_NON_JOGROG'] * $records['WEIGHT_CUSTOMER']);
                                    }    
                                    $discount = $discount + $records['DISCOUNT'];    
                            ?>
                                <tr style="border-collapse: collapse;border: 1px solid black;">
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: center"><?php echo $idx + 1; ?></td>
                                    <td><?php echo nl2br($records['REGISTER_NO'].' '.$records['TYPE_TERNAK'].' '.$records['JENIS_TERNAK']); ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: center"><?php echo $records['WEIGHT_CUSTOMER']; ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: center"><?php echo $records['TYPESALEDESC']; ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: center"><?php echo $records['QTY']; ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: center"><?php echo ($records['DISCOUNT']===''? 0 : $records['DISCOUNT']).'%'; ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php  echo "Rp " . number_format(($records['TYPE_SALE']==="JG" ? $records['TOTAL_JOGROG'] : $records['TOTAL_NON_JOGROG']),2,',','.'); ?></td>
                                    <td style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php  echo "Rp " . number_format(($records['TYPE_SALE']==="JG" ? $records['TOTAL_JOGROG'] : $records['TOTAL_NON_JOGROG'] * $records['WEIGHT_CUSTOMER']),2,',','.'); ?></td>
                                </tr>
                            <?php
                                } 
                            ?>
                                <tr>
                                    <td colspan="7" style="border-collapse: collapse;border: 1px solid black; text-align: right">Sub Total</td>
                                    <td colspan="1" style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php echo "Rp " . number_format($total,2,',','.'); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="border-collapse: collapse;border: 1px solid black; text-align: right">Discount</td>
                                    <td colspan="1" style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php echo "Rp " . number_format(($total * $discount) / 100,2,',','.'); ?></td>
                                </tr> 
                                <tr>
                                    <td colspan="7" style="border-collapse: collapse;border: 1px solid black; text-align: right">Tax</td>
                                    <td colspan="1" style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php echo "Rp " . number_format(($member['tax'] === 'Y' ? ( $total - ($total * $discount) / 100)* 10 / 100 : 0 ),2,',','.'); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="border-collapse: collapse;border: 1px solid black; text-align: right">Total</td>
                                    <td colspan="1" style="border-collapse: collapse;border: 1px solid black; text-align: right"><?php echo "Rp " . number_format(($total - ($total * $discount) / 100) + (($member['tax'] === 'Y' ? ( $total - ($total * $discount) / 100)* 10 / 100 : 0 ) ),2,',','.'); ?></td>
                                </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </section>

</div>
<script>
    function print_Pdf() {
        window.print();
        document.margin='none';
    }
</script>
</body>
</html>
<!-- onload="print_Pdf();" -->
