<div class="row">
    <div class="col-md-12">  
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="member_name">To</label>
                        <?php
                        echo form_input([
                            'name'          => "member_name",
                            'placeholder'   => "Nama",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('member_name', ($data['member']['nama'] !== NULL ? $data['member']['nama'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="alamat">alamat</label>
                        <?php
                        echo form_input([
                            'name'          => "alamat",
                            'placeholder'   => "alamat",
                            'class'         => "form-control",
                            'readonly'      => 'readonly',
                            'value'         => set_value('alamat', ($data['member']['alamat'] !== NULL ? $data['member']['alamat'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <?php
                        echo form_input([
                            'name'          => "phone",
                            'placeholder'   => "phone",
                            'class'         => "form-control",
                            'readonly'      => 'readonly',
                            'value'         => set_value('phone', ($data['member']['hp'] !== NULL ? $data['member']['hp'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="outdate">Tanggal Keluar</label>
                        <?php
                        echo form_input([
                            'name'          => "outdate",
                            'placeholder'   => "Tanggal Keluar",
                            'class'         => "form-control",
                            'readonly'      => 'readonly',
                            'value'         => set_value('outdate', ($data['member']['tanggal_keluar'] !== NULL ? $data['member']['tanggal_keluar'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <?php
                        echo form_hidden([
                            'name'          => "memberid",
                            'placeholder'   => "Tanggal Keluar",
                            'class'         => "form-control",
                            'readonly'      => 'readonly',
                            'value'         => set_value('memberid', ($data['member']['id_member'] !== NULL ? $data['member']['id_member'] : NULL), FALSE)
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="box-body table-body">
                <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Berat/Kg</th> 
                    <th>Jenis</th> 
                    <th>Qty</th>
                    <th>Discount</th>
                    <th>Harga</th>
                    <th>Total</th>
                </tr>  
                <?php 
                    $total=0;
                    $total1=0;
                    $total2=0;
                    $discount=0;
                    foreach($data['items'] as $idx =>$records) { 
                        if ($records['TYPE_SALE']==="JG") {
                            $total1=$total1 + $records['TOTAL_JOGROG'];
                       } else {
                            $total2=$total2 + $records['TOTAL_NON_JOGROG'] * $records['WEIGHT_CUSTOMER'];
                       }         
                       $total=$total1+$total2;
                       $discount = $discount+floatval($records['DISCOUNT']);
                    ?>
                    <tr>
                        <td><?php echo $idx + 1 ?></td>
                        <td><?php echo $records['REGISTER_NO'].' '.$records['JENIS'].' '.$records['FARM'] ?></td>
                        <td><?php echo $records['WEIGHT_CUSTOMER'];?></td>
                        <td><?php echo $records['TYPE_SALE_DESC'];?></td>
                        <td><?php echo $records['QUANTITY'];?></td>
                        <td><?php echo ($records['DISCOUNT']===''? 0 : $records['DISCOUNT']).'%';?></td>
                        <td><?php echo "Rp " . number_format(($records['TYPE_SALE']==="JG" ? $records['TOTAL_JOGROG'] : $records['TOTAL_NON_JOGROG']),2,',','.');?></td>
                        <td><?php echo "Rp " . number_format(($records['TYPE_SALE']==="JG" ? $records['TOTAL_JOGROG'] : $records['TOTAL_NON_JOGROG'] * $records['WEIGHT_CUSTOMER']),2,',','.');?></td>
                    </tr> 
                <?php      
                    }
                ?>       
                    <tr>
                        <td colspan="7" style="text-align:right">Sub Total</td>
                        <td colspan="1"><?php echo "Rp " . number_format($total,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align:right">Discount</td>
                        <td colspan="1"><?php echo "Rp " . number_format(($total * $discount) / 100,2,',','.'); ?></td>
                    </tr>  
                    <tr>
                        <td colspan="7" style="text-align:right">Tax</td> <?php echo $data['member']['tax'] ?>
                        <td colspan="1"><?php echo "Rp " . number_format(($data['member']['tax'] === 'Ya' ? ( $total - ($total * $discount) / 100)* 10 / 100 : 0 ),2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align:right">Total</td>
                        <td colspan="1"><?php echo "Rp " . number_format(($total - ($total * $discount) / 100) + (($data['member']['tax'] === 'Ya' ? ( $total - ($total * $discount) / 100)* 10 / 100 : 0 ) ),2,',','.'); ?></td>
                    </tr>     
                    <tr>
                        <td colspan="8">
                            <p><?php echo nl2br(terbilang(($total - ($total * $discount) / 100) + (($data['member']['tax'] === 'Ya' ? ( $total - ($total * $discount) / 100)* 10 / 100 : 0 ) )).' Rupiah'); ?></p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
                <a data-href="<?php echo site_url('penjualan_domba/create_invoice/'.$data['member']['id_member'].'/'.$data['member']['tanggal_keluar'].'/'.$data['member']['tax']);?>?<?php echo url_create_return_query();?>" 
                    class="btn btn-primary btn-invoice">Invoice</a>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-invoice">
    <div class="modal-dialog modal-danger modal-sm">
        <div class="modal-content">
            <?php
                echo form_open('', [
                    'name'      => 'form-delete',
                    'method'    => 'post']);
            ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan di proses ke INVOICE?</p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Batal" data-dismiss="modal" />
                <input type="submit" class="btn btn-flat btn-outline" value="Proses" />
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('.btn-invoice').click(function(e) {
            e.preventDefault();

            let rowData = $(this);
            let modalRemove = $('.modal-invoice');

            $('form[name="form-delete"]', modalRemove).attr("action", rowData.attr('data-href'));
            // $('.modal-title', modalRemove).text(rowData.attr('data-original-title'));
            // $('.modal-body p', modalRemove).text(rowData.attr('data-message'));

            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });
    }
</script>
