<?php
echo form_open(('penjualan_domba/save'),
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('penjualan/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div> 
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="member_id">Member</label>
                    <?php
                         $options = [];
                         $options[''] = 'Pilih Member';
                         if ($data['flow_data']['member_id'] !== NULL)
                             $options[$data['flow_data']['member_id']] = $data['flow_data']['member_nama'];
                         echo form_dropdown('member_id', $options, set_value('member_id', ($data['flow_data']['member_id'] !== NULL ? $data['flow_data']['member_id'] : NULL), FALSE), 'id="member_id" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
            </div>
        </div>
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="registrasi_no">No Registrasi</label>
                        <?php
                        echo form_input([
                            'name'          => "registrasi_no",
                            'placeholder'   => "No Registrasi",
                            'class'         => "form-control register",
                            'required'      => 'required',
                            'readonly'      => 'readonly',
                            'value'         => set_value('registrasi_no', ($data['flow_data']['registrasi_no'] !== NULL ? $data['flow_data']['registrasi_no'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="outside_date">Tanggal Keluar</label>
                        <?php
                        echo form_input([
                            'name'        => "outside_date[]",
                            'class'       => "form-control",
                            'id'          => "datepicker",
                            'placeholder' => "Tanggal Keluar",
                            'required'    => 'required',
                            'value'       => set_value('outside_date', ($data['flow_data']['outside_date'] !== NULL ? $data['flow_data']['outside_date'] : NULL), FALSE)
                        ]);?>
                    </div>            
                    <div class="form-group">
                        <label for="weight_out">Berat Keluar Kandang</label>
                        <?php
                        echo form_input([
                            'name'        => "weight_out[]",
                            'placeholder' => "Berat Keluar Kandang",
                            'class'       => "form-control",
                            'type'        => "number",
                            'required'    => 'required',
                            'readonly'    => 'readonly',
                            'value'       => set_value('weight_out', ($data['flow_data']['weight_out'] !== NULL ? $data['flow_data']['weight_out'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="weight_cus">Berat Badan Di Customer</label>
                        <?php
                        echo form_input([
                            'name'          => "weight_cus",
                            'placeholder'   => "Berat Badan Di Customer",
                            'class'         => "form-control",
                            'type'          => "number",
                            'required'      => 'required',
                            'value'       => set_value('weight_cus', ($data['flow_data']['weight_cus'] !== NULL ? $data['flow_data']['weight_out'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="jenis_jual">Jenis Jual</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Penjualan';
                        if ($data['jenis_jual'] !== NULL ){
                            foreach ($data['jenis_jual'] as $row) {
                                        $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('jenis_jual[]', $options, set_value('jenis_jual', ($data['flow_data']['jenis_jual'] !==NULL ? $data['flow_data']['jenis_jual'] : NULL), FALSE), 'id="jenis_jual" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>
                    <a  class="remove text-danger" title="Hapus">
                                <span class="fa fa-trash"></span>
                            </a> 
                </div>
            </div>
            <div class="box-footer">
                    <a  class="addMore" title="Tambah">
                            <span class="fa fa-plus"></span>
                        </a>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<div class="modal fade modal-delete">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>No Registrasi</th>
                                <th>Tanggal Registrasi</th>
                                <th>Nomor Kamar</th>
                                <th>Berat Masuk</th>
                                <th>Tipe Ternak</th>
                                <th>Jenis Ternak</th> 
                                <th>Tipe Kandang</th>
                                <th>&nbsp;</th>
                            </tr>
                        </table>
                        <table class="table table-hover" id="list">
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-flat btn-default" value="Close" data-dismiss="modal" />
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        tableInit();
        $('.addMore').on('click', function() {
            tableAdd(null);
        });

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        $('.register').click(function () {
            let modalRemove = $('.modal-delete');    
            $('.modal-title', modalRemove).text('Katalog Penjualan Hewan Ternak');

            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $(document).on('click', '.register', function () {
            let modalRemove = $('.modal-delete');    
            $('.modal-title', modalRemove).text('Katalog Penjualan Hewan Ternak');

            modalRemove.modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('[name="table_search"]').keyup(function () {
            $.ajax({
                url: '<?php echo site_url('penjualan_domba/getListRegisterno'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: $(this).val()},
                success: function(d) {
                    $("#list").empty();
                    $.each( d, function( key, value ) {
                        $("#list").append("<tr><td art_id="+value['REGISTER_NO']+" weigth="+value['WEIGHT_IN']+">" + value['REGISTER_NO'] + "</td><td>"+value['REGISTER_DATE']+"</td><td>"+value['ROOM_NUMBER']+"</td><td>"+value['WEIGHT_IN']+"</td><td>"+value['TIPE_TERNAK']+"</td><td>"+value['JENIS_TERNAK']+"</td><td>"+value['TIPE_KANDANG']+"</td></tr>");
                    });
                }
            });
        });

       

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>

        $(function () {
            $('#member_id').select2({
                placeholder: 'Masukan No Registrasi',
                ajax: {
                    url: '<?php echo site_url('penjualan_domba/getSelectMember')?>',
                    type: "post",
                    dataType: 'json',
                    delay: 1000,
                    data: function (params) {
                        return {
                            searchTerm: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                                results: response
                            };
                    },
                }
            });

            $('.select2').select2()
    
            //Date picker
            $('#datepicker').datepicker({
            autoclose: true
            })

            //Date picker
            $('#datepicker1').datepicker({
            autoclose: true
            })

            //Timepicker
            $('.timepicker').timepicker({
            showInputs: false
            })
        })
    }

    let trbase = null;
    function tableInit(){
        let tr = $('.table-row');
        trbase = tr.clone(false, false);
        // tr.remove();
    }

    let idx=0;
    function tableAdd(order) {
        let trNew = trbase.clone();
        $('.table-body').append(trNew);

        // if (order !== null) {
        //     $('[name="total_order[]"]',trNew).val(order.total_order);
        //     $('[name="price_food[]"]', trNew).val(order.price_food);
        //     $('[name="total[]"]', trNew).val(order.total);
        //     $('[name="biaya_lain[]"]', trNew).val(order.biaya_lain);
        //     // $('[name="tipe_pakan[]"]', trNew).select2().select2('val',order.tipe_pakan); 
        //     $('[name="tipe_pakan[]"]', trNew)
        //             .prepend(
        //                 $('<option>', { value : order.tipe_pakan.id })
        //                     .text(order.nama_pakan.name)
        //             )
        //             .val(order.tipe_pakan.id);  
        // }
        tableAttach(trNew);
    }

    let idxEnabled=0;
    function tableAttach(trNew) {

        // $("#price_food", trNew).on("change", function(){
        //     $('#total', trNew).val($(this).val() * $('#total_order', trNew).val());
        // });
        // $('[name="registrasi_no[]"]',trNew).attr('id','register'+idxEnabled);
        // $('.register', trNew);
        // $('[name="outside_date[]"]', trNew).attr('id','datepicker');

        $('[name="jenis_jual[]"]', trNew).addClass("form-control select2");
        $('[name="registrasi_no[]"]', trNew).attr('name','registrasi_no['+idxEnabled+']');
        $('[name="weight_out[]"]', trNew).attr('name','weight_out['+idxEnabled+']');
        $('.remove', trNew).click(function (e) {
            e.preventDefault();

            let tableRow = $(this).parent();
                tableRow.remove();
                modal.modal('hide');
        });

        autoclose(trNew);

        idxEnabled++;
    }

    function autoclose(trNew) {
        $(document).on('click','td',function() {
            $('[name="registrasi_no[]"]').val($(this).attr('art_id'));
            $('[name="weight_out[]"]').val($(this).attr('weigth'));
            $('.modal-delete').modal('toggle');
        });
    }
</script>