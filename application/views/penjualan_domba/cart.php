<?php
echo form_open('penjualan_domba/confrim_order', 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="member">Pembeli</label>
                    <?php
                        $options = [];
                        $options[''] = 'Pilih Pember';
                        foreach ($data['member'] as $row) {
                                    $options[$row['member_id']] = $row['member_name'];
                            }

                        echo form_dropdown('member', $options, set_value('member', NULL, FALSE), 'id="member" class="form-control select2" data-width="100%" required="required"');
                        ?>
                </div>
                <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <p class="alamat"></p>
                </div>
                <div class="form-group">
                        <label for="phone">Phone</label>
                        <p class="phone"></p>
                </div>
                <div class="form-group">
                        <label for="outside_date">Tanggal Keluar</label>
                        <?php
                        echo form_input([
                            'name'        => "outside_date",
                            'class'       => "form-control",
                            'id'          => "datepicker",
                            'placeholder' => "Tanggal Keluar",
                            'required'    => 'required',
                            'value'       => set_value('outside_date', NULL, FALSE)
                        ]);?>
                </div>    
                <div class="form-group">
                    <label for="tax">Tax</label>
                    <br>
                    <?php
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "Ya",   
                            'checked'       => FALSE,  
                            'class'         => 'privilege-radio',
                            'required'      => 'required',
                            'id'            => 'taxyes'
                        ]);
                        echo form_label('Ya');
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "No",
                            'checked'       => TRUE,
                            'required'      => 'required',
                            'class'         => 'privilege-radio',
                            'id'            => 'taxno'
                        ]);
                        echo form_label('Tidak');
                    ?>
                </div>
                <label for="member">Detail Barang</label>
                <div class="box-body ">
                            <?php
                            foreach ($data['items'] as $idx => $record)
                            {
                                ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="width: 200px">No Registrasi</th><td><?php echo $record['register_no']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Registrasi</th><td><?php echo $record['tanggal_register']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tipe Ternak</th><td><?php echo $record['tipe_ternak']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Ternak</th><td><?php echo $record['jenis_ternak']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Berat Masuk</th><td><?php echo $record['berat_masuk']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Berat Customer</th><td><?php echo $record['berat_customer']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Jual</th><td><?php echo $record['jenis_jual_desc']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Harga Perkilo</th><td><?php echo "Rp " . number_format($record['price_kg'],2,',','.'); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jogrog</th><td><?php echo "Rp " . number_format($record['jogrog'],2,',','.'); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Discount</th><td><?php echo ($record['discount'] ==='' ? 0 : $record['discount'])."%"; ?></td>
                                    </tr>
                                    <tr>
                                        <th> 
                                            <a href="<?php echo site_url('penjualan_domba/remove/' . $record['register_no']);?>?<?php echo url_create_return_query();?>"
                                                class="btn btn-flat btn-sm"
                                                data-toggle="tooltip" data-placement="bottom"
                                                title="Delete <?php echo html_escape($record['register_no']);?>">
                                                <i class="fa  fa-trash"></i> 
                                            </a>
                                        </th>
                                    </tr>
                                </table>
                                <br>
                            <?php
                            }  
                            ?>
                    <?php
                        $total_jogrog=0; 
                        $total_non_jogrog=0;
                        $discount=0;
                        foreach ($data['items'] as $idx => $record)
                        {
                            if  (intval($record['jogrog']) > 0) {
                                $total_jogrog = $total_jogrog + floatval($record['jogrog']);
                            } else {
                                $total_non_jogrog = $total_non_jogrog + (floatval($record['berat_customer']) * floatval($record['price_kg']));
                            }
                            $discount = $discount+floatval($record['discount']);
                        }
                    ?>    
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 200px">Sub Total Jogrog</th><td><?php echo "Rp " . number_format($total_jogrog,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Sub Total Non Jogrog</th><td><?php echo "Rp " . number_format($total_non_jogrog,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Discount</th><td><?php echo "Rp " . number_format(($total_jogrog + $total_non_jogrog) * $discount / 100,2,',','.'); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px">Total</th><td><?php echo "Rp " . number_format($total_jogrog + $total_non_jogrog,2,',','.'); ?></td>
                        </tr>
                    </table>   
                </div>                 
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Confirm</button>
                    <a class="btn btn-default" href="<?php echo site_url('penjualan_domba/lists')?>?<?php echo url_return_query();?>">
                        <i class="fa fa-undo"></i> Tambah
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {
        $("#member").on("select2:select", function (e) { 
            var select_val = $(e.currentTarget).val();
             $.ajax({
                url: '<?php echo site_url('penjualan_domba/getSelectMember'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: select_val},
                success: function(d) {
                    console.log(d);
                    $.each( d, function( i, val ) {
                           $('.alamat').text(val['MEMBER_ALAMAT']);
                           $('.phone').text(val['MEMBER_HP']);
                    });
                }
            });
        });
    }
</script>