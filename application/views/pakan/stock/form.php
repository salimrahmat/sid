<?php
echo form_open(('pakan/stock/save'),
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
    var_dump($data);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="jenis_pakan">Jenis Pakan</label>
                    <?php 
                    $options = [];
                    $options[''] = 'Pilih Jenis Pakan';
                    if ($data['tipe_pakan'] !== NULL ){
                        foreach ($data['tipe_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('jenis_pakan', $options, set_value('jenis_pakan', NULL, FALSE), 'id="jenis_pakan" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="proses">Proses</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Proses';
                    if ($data['proses'] !== NULL ){
                        foreach ($data['proses'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('proses', $options, set_value('proses', NULL, FALSE), 'id="proses" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="proses_date">Tanggal Proses</label>
                    <?php
                    echo form_input([
                        'name'              => "proses_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Tanggal Proses",
                        'required'          => 'required',
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="amount">Jumlah</label>
                    <?php
                    echo form_input([
                        'name'          => "amount",
                        'placeholder'   => "Jumlah",
                        'class'         => "form-control",
                        'type'          => "number",
                        'required'      => 'required',
                    ]);?>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script>
    $(function () {
        let formForm = $('form[name="form-input"]');
        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        $('.select2').select2()
    })
</script>