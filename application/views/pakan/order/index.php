<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('pakan/order/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                </div>
                <?php
                    echo form_open('order/pakan/search', 
                    [
                        'name'   => 'form-search-pakan',
                        'method' => 'post',
                        'id'     => 'pakan',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Ternak';
                        foreach ($data['tipe_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_pakan', $options, set_value('tipe_pakan', NULL, FALSE), 'id="tipe_pakan" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Tipe Pakan</th>
                        <th>Tanggal Order</th>
                        <th>Tanggal Datang</th>
                        <th>Total Order</th>
                        <th>&nbsp;</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>