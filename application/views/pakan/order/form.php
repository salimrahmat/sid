<?php
echo form_open(('registrasi/save'),
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="jenis_pakan">Jenis Pakan</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Jenis Pakan';
                    if ($data['tipe_pakan'] !== NULL ){
                        foreach ($data['tipe_pakan'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                            }
                    }
                    echo form_dropdown('jenis_pakan', $options, set_value('jenis_pakan', NULL, FALSE), 'id="jenis_pakan" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="order_date">Tanggal Order</label>
                    <?php
                    echo form_input([
                        'name'              => "order_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Tanggal Proses",
                        'required'          => 'required',
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="total_order">Jumlah Order</label>
                    <?php
                    echo form_input([
                        'name'          => "total_order",
                        'placeholder'   => "Jumlah Order",
                        'class'         => "form-control",
                        'type'          => "number",
                        'required'      => 'required',
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="arrival_date">Tanggal Kedatangan</label>
                    <?php
                    echo form_input([
                        'name'              => "order_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Tanggal Proses",
                        'required'          => 'required',
                    ]);?>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script>
    $(function () {
        let formForm = $('form[name="form-input"]');
        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        $('.select2').select2()
    })
</script>