<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <?php if($header) {echo $header;}?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>F</b>M</span>
      <span class="logo-lg"><b>Farm</b>Management</span>
    </a>
    <?php if($navbar) {echo $navbar;}?>
  </header>
  <aside class="main-sidebar">
    <?php if($sidebar) {echo $sidebar;}?>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <?php if($breadcrumb) {echo $breadcrumb;}?>
    </section>
    <section class="content">
        <?php if($page) {echo $page;}?>
    </section>
  </div>
  <footer class="main-footer">
    <?php if($footer) {echo $footer;}?>
  </footer>
    <?php if($sidebarControl) {echo $sidebarControl;}?>
  <div class="control-sidebar-bg"></div>
</div>
<?php if($js) {echo $js;}?>
</body>
</html>
