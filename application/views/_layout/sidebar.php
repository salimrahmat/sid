<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/images/orang.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- var_dump($this->is_privilage('Registrasi','menu_list'));
            $this->is_privilage('Registrasi','menu_list');
            die(); -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-registered"></i><span>Registrasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <?php 
           if(is_privilege(PRIVILEGE_REGISTRASI,PRIVILEGE_LIST)) { ?>
            <li><a href="<?php echo site_url('registrasi/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-circle-o"></i>Registrasi Domba</a></li>
          <?php } ?>
          <?php
            if(is_privilege(PRIVILEGE_ORDER_DOMBA,PRIVILEGE_LIST)){ ?>
            <li><a href="<?php echo site_url('registrasi/lists_order')?>?<?php echo url_return_query();?>"><i class="fa fa-circle-o"></i>Order Domba</a></li>
          <?php
            } 
          ?>
          </ul>
        </li>
        <li>
        <?php 
           if(is_privilege(PRIVILEGE_PENIMBANGAN,PRIVILEGE_LIST)) { ?>
            <a href="<?php echo site_url('penimbangan/lists')?>?<?php echo url_return_query();?>">
              <i class="fa fa-th"></i> <span>Penimbangan Harian</span>
            </a>
          <?php    
           }
         ?>
        </li>
        <li>
        <?php 
           if(is_privilege(PRIVILEGE_PEMBERIAN_PAKAN,PRIVILEGE_LIST)) { ?>
            <a href="<?php echo site_url('pemberian_pakan/lists')?>?<?php echo url_return_query();?>">
              <i class="fa fa-th"></i> <span>Pemberian Pakan</span>
            </a>
          <?php
           }
        ?>
        </li>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Penjualan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php 
            if(is_privilege(PRIVILEGE_PENJUALAN,PRIVILEGE_LIST)) { ?>
              <li><a href="<?php echo site_url('penjualan_domba/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-text-height"></i>Form Penjualan</a></li>
              <?php
              }
            ?>      
            <?php 
            if(is_privilege(PRIVILEGE_LIST_PENJUALAN,PRIVILEGE_LIST)) { ?>
              <li><a href="<?php echo site_url('penjualan_domba/lists_jual')?>?<?php echo url_return_query();?>"><i class="fa fa-list-alt"></i>List Penjualan</a></li>
              <?php
              }
            ?>  
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Pakan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <?php 
            if(is_privilege(PRIVILEGE_STOK_PAKAN,PRIVILEGE_LIST)) { ?>
              <li><a href="<?php echo site_url('order_pakan/stock')?>?<?php echo url_return_query();?>"><i class="fa fa-circle-o"></i> Stock Pakan</a></li>
            <?php
              }
            ?>
          <?php 
            if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_LIST)) { ?>
              <li><a href="<?php echo site_url('order_pakan/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-circle-o"></i> Order Pakan</a></li>
          <?php
              }
          ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i>
            <span>Review</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!-- <?php 
              if(is_privilege(PRIVILEGE_STOCK_TERNAK,PRIVILEGE_LIST)) { ?>
                <li><a href="<?php echo site_url('review/stock_ternak')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Stock Ternak</a></li>
                <?php
              }
            ?> -->
            <?php 
              if(is_privilege(PRIVILEGE_CHECK_TERNAK,PRIVILEGE_LIST)) { ?>          
                <li><a href="<?php echo site_url('review/check_ternak')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Check Ternak</a></li>
            <?php
              }
            ?>    
            <?php 
              if(is_privilege(PRIVILEGE_CHART_TERNAK,PRIVILEGE_LIST)) { ?>
                <li><a href="<?php echo site_url('review/chart')?>?<?php echo url_return_query();?>"><i class="fa fa-bar-chart"></i>Chart Ternak</a></li>
              <?php
              }
            ?>
            <?php 
              if(is_privilege(PRIVILEGE_PROGRES_TERNAK,PRIVILEGE_LIST)) { ?>
                <li><a href="<?php echo site_url('review/progres_ternak')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Progres Ternak</a></li>
            <?php
              }
            ?>    
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-male"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php 
                if(is_privilege(PRIVILEGE_SETTING,PRIVILEGE_LIST)) { ?>          
                  <li><a href="<?php echo site_url('general_setting/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Setting</a></li>
            <?php 
                }
            ?>
            <?php 
                if(is_privilege(PRIVILEGE_VENDOR,PRIVILEGE_LIST)) { ?>          
                  <li><a href="<?php echo site_url('vendor/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Vendor</a></li>
            <?php 
                }
            ?>
            <?php 
                if(is_privilege(PRIVILEGE_MEMBER,PRIVILEGE_LIST)) { ?>          
                  <li><a href="<?php echo site_url('member/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Member</a></li>            
            <?php 
                }
            ?>
            <?php 
                if(is_privilege(PRIVILEGE_USER,PRIVILEGE_LIST)) { ?> 
                  <li><a href="<?php echo site_url('user/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>User</a></li>
            <?php 
                }
            ?>   
            <?php 
                if(is_privilege(PRIVILEGE,PRIVILEGE_LIST)) { ?> 
                  <li><a href="<?php echo site_url('privilege/lists')?>?<?php echo url_return_query();?>"><i class="fa fa-minus"></i>Privilege</a></li>
            <?php 
                }
            ?>
          </ul>
        </li>
      </ul>
    </section>
    
 
 