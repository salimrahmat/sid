<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <?php
                echo form_open('review/check_ternak', 
                [
                    'name'          => 'form-search',
                    'method'        => 'post']); 
                ?>
                <div class="col-md-2">
                    <?php 
                    $options = [];
                    $options[''] = 'Pilih Tipe Ternak';
                    foreach ($data['tipe_ternak'] as $row) {
                                $options[$row['master_code']] = $row['descr'];
                    }
                    echo form_dropdown('tipe_ternak', $options, set_value('tipe_ternak', NULL, FALSE), 'id="tipe_ternak" class="form-control select2" data-width="100%"');
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Jenis Ternak';
                    foreach ($data['jenis_ternak'] as $row) {
                                $options[$row['master_code']] = $row['descr'];
                    }
                    echo form_dropdown('jenis_ternak', $options, set_value('jenis_ternak', NULL, FALSE), 'id="jenis_ternak" class="form-control select2" data-width="100%"');
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Kategori';
                    foreach ($data['kategori'] as $row) {
                                $options[$row['master_code']] = $row['descr'];
                    }
                    echo form_dropdown('kategori', $options, set_value('kategori', NULL, FALSE), 'id="kategori" class="form-control select2" data-width="100%"');
                    ?>
                </div>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    <a href="javascript:void(0);" onclick="reset();"
                       class="btn btn-default">
                       <i class="fa fa-refresh"></i>
                    </a>
                    <a href="<?php echo site_url('review/export_ternak/'.$data['kategori_check']);?>?<?php echo url_create_return_query();?>" class="btn btn-default">
                           <i class="fa fa-file-excel-o"></i>
                    </a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>No Registrasi</th>
                        <th>Kategori</th>
                        <th>Tipe Kandang</th>
                        <th>Nomor Kamar</th>
                        <th>Berat Badan</th>
                        <th>Tanggal Timbang</th>
                    </tr> 
                    <?php
                        foreach ($data['check_ternak']['records'] as $idx => $record)
                    {?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo $record->REGISTER_NO; ?></td>
                            <td><?php echo $data['kategori_check']; ?></td>
                            <td><?php echo $record->TIPE_KANDANG; ?></td>  
                            <td><?php echo $record->NOMOR_KAMAR; ?></td>
                            <td><?php echo $record->BERAT_BADAN; ?></td>
                            <td><?php echo date_reformat($record->TANGGAL_TIMBANG, 'j M Y', '&ndash;')?></td>
                            
                        </tr>
                    <?php 
                    }
                    ?>
                </table>
            </div>
             <div class="box-footer clearfix">
                <?php
                echo $data['check_ternak']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function reset() {        
        $('#tipe_ternak').val(null).trigger('change');
        $('#jenis_ternak').val(null).trigger('change');
        $('#kategori').val(null).trigger('change');
    }
</script>