<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                    <?php
                    echo form_open('review/progres_ternak', 
                    [
                        'name'          => 'form-search',
                        'method'        => 'post']); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Kandang';
                        foreach ($data['tipe_kandang'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_kandang', $options, set_value('tipe_kandang', NULL, FALSE), 'id="tipe_kandang" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Kategori';
                        foreach ($data['kategori'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('kategori', $options, set_value('kategori', NULL, FALSE), 'id="kategori" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Ternak';
                        foreach ($data['jenis_ternak'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('jenis_ternak', $options, set_value('jenis_ternak', NULL, FALSE), 'id="jenis_ternak" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Nomor Kamar">
                    </div>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        <a href="<?php echo site_url('review/export_progres');?>?<?php echo url_create_return_query();?>" class="btn btn-default">
                           <i class="fa fa-file-excel-o"></i>
                        </a>                        
                    </div>
                    <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Tipe Kandang</th>
                        <th>Nomor Kamar</th>
                        <th>No Register</th>
                        <th>Tanggal Register</th>
                        <th>Berat Masuk</th>
                        <th>Tanggal Timbang</th>
                        <th>Berat Timbang</th>
                        <th>Total Hari</th>
                        <th>Total Naik</th>
                    </tr> 
                    <?php
                        foreach ($data['progress_ternak']['records'] as $idx => $record)
                    {?>
                    <tr>
                        <td><?php echo $idx + 1; ?></td>
                        <td><?php echo $record->tipe_kandang; ?></td>
                        <td><?php echo $record->nomor_kamar; ?></td>
                        <td><?php echo $record->no_registrasi; ?></td>
                        <td><?php echo date_reformat($record->tanggal_register, 'j M Y', '&ndash;'); ?></td>
                        <td><?php echo $record->berat_masuk; ?></td>
                        <td><?php echo date_reformat($record->tanggal_timbang, 'j M Y', '&ndash;'); ?></td>
                        <td><?php echo $record->berat_timbang; ?></td>
                        <td><?php echo $record->total_hari; ?></td>
                        <td><?php echo $record->total_naik; ?></td>
                    </tr>
                    <?php
                    } 
                    ?>    
                </table>     
            </div>     
            <div class="box-footer clearfix">
                <?php
                echo $data['progress_ternak']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>