
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-3">
                            <h4>Tipe-Tipe Kategori</h4>
                            <div class="form-group">
                                <label><input type="radio" class="category" name="r2" value="kategori" class="minimal-red" checked>
                                  Kategori
                                </label>
                            </div>
                            <div class="form-group">
                                <label><input type="radio" class="tipeternak" name="r2" value="tipeternak" class="minimal-red">
                                  Tipe Ternak
                                </label>
                            </div>
                            <div class="form-group">
                                <label><input type="radio" class="jenisternak" name="r2" value="jenisternak" class="minimal-red">
                                  Jenis Ternak
                                </label>
                            </div>
                            <div class="form-group">
                                <label><input type="radio" class="tipekandang" name="r2" value="kandang" class="minimal-red">
                                  Tipe Kandang
                                </label>
                            </div>
                            <div class="form-group">
                                <label><input type="radio" class="nomorkandang" name="r2" value="Kamar" class="minimal-red">
                                  Nomor Kamar
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <canvas id="pieChart" style="height:80px"></canvas>
                            <!-- <div id="donut-chart" style="height: 300px;"></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-warning">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                echo form_open(NULL, 
                                [
                                    'name'          => 'form-input',
                                    'method'        => 'post'],
                                [
                                    'return_url'    => url_get_return('registrasi/form')]); 
                                ?>

                                <div class="col-xs-2">
                                    <?php
                                    $options = ['' => "Pilih  Tipe "];
                                    foreach (CHART_BARS as $chart) { $options[$chart['id']] = $chart['name'];}
                                    echo form_dropdown('type', $options, set_value('type',NULL, FALSE), 'id="type" class="form-control select2" required="required"');?>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <?php
                                        echo form_input([
                                            'name'              => "first_date",
                                            'class'             => "form-control",
                                            'id'                => "datepicker",
                                            'placeholder'       => "Tanggal Pertama",
                                            'required'          => 'required'
                                        ]);?>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <?php
                                        echo form_input([
                                            'name'              => "end_date",
                                            'class'             => "form-control tanggal",
                                            'id'                => "datepicker1",
                                            'placeholder'       => "Tanggal Kedua",
                                            'required'          => 'required',
                                        ]);?>
                                    </div>
                                </div>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-click"><i class="fa fa-search"></i></button>
                                </div>
                                <?php echo form_close();?>
                                <div class="chart">
                                    <canvas id="barChart" style="height:230px"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>/assets/bower_components/chart.js/Chart.min.js"></script>
<script type="text/javascript">
    window.onload = function(event) {
        chart(<?php echo json_encode($data['chart']);?>);
        function chart(param) {
            $('#pieChart').empty();

            var isi_labels  =[];
            var isi_data    =[];
            var isi_color   =[];    
            var myPieChart  =null;

            $(param).each(function(i){         
                isi_labels.push(param[i].label); 
                isi_data.push(param[i].value);
                isi_color.push(param[i].color);
            }); 

            if(myPieChart!=null){
                myPieChart.destroy();
            }

            var ctx = document.getElementById('pieChart');
            var context = ctx.getContext('2d');
            context.clearRect(0, 0, context.width, context.height);
            myPieChart = new Chart(context, {
                type: 'pie',
                data: {
                    labels: isi_labels,
                    datasets: [{
                        label: 'Data Produk',
                        data: isi_data,
                        backgroundColor: isi_color,
                    }]
                },
                options: {
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var labels = data.labels[tooltipItem.index];
                                var currentValue = dataset.data[tooltipItem.index];
                                return labels+": "+currentValue+" %";
                            }
                        }
                    }
                }
            });
        };

        $(".tipeternak").change(function(){
            $.ajax({
                url: '<?php echo site_url('review/radiochart'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: 'tipeternak'},
                success: function(d) {
                    chart(d);
                }
            });
        });

        $(".category").change(function(){
            $.ajax({
                url: '<?php echo site_url('review/radiochart'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: 'category'},
                success: function(d) {
                    chart(d);
                }
            });
        });

        $(".jenisternak").change(function(){
            $.ajax({
                url: '<?php echo site_url('review/radiochart'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: 'jenisternak'},
                success: function(d) {
                    chart(d);
                }
            });
        });

        $(".tipekandang").change(function(){
            $.ajax({
                url: '<?php echo site_url('review/radiochart'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: 'tipekandang'},
                success: function(d) {
                    chart(d);
                }
            });
        });

        $(".nomorkandang").change(function(){
            $.ajax({
                url: '<?php echo site_url('review/radiochart'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: 'nomorkandang'},
                success: function(d) {
                    chart(d);
                }
            });
        });

            $('.btn-click').click(function(e) {
                e.preventDefault();

                $.ajax({
                    url: '<?php echo site_url('review/barchart'); ?>',
                    type: "post",
                    dataType  : 'json',
                    data: {searchTerm: $('#type').val(),
                        datefrom: $('#datepicker').val(),
                        dateto: $('#datepicker1').val()  
                    },
                    success: function(d) {
                        $('#barChart').empty();

                        var datasets   = [];
                        var isi_periode = [];
                        var isi_label = [];
                        var isi_data = [];
                        $(d).each(function(i){         

                            if (!isi_periode.includes(d[i].periode)) {
                                isi_periode.push(d[i].periode)
                            }   
                            datasets.push({
                                'label'               : d[i].label,
                                'fillColor'           : 'rgba(210, 214, 222, 1)',
                                'strokeColor'         : 'rgba(210, 214, 222, 1)',
                                'pointColor'          : 'rgba(210, 214, 222, 1)',
                                'pointStrokeColor'    : '#c1c7d1',
                                'pointHighlightFill'  : '#fff',
                                'pointHighlightStroke': 'rgba(220,220,220,1)',
                                'data'                :  d[i].data, 
                                'backgroundColor'     : [
                                                            'rgba(255, 99, 132, 0.2)',
                                                            'rgba(255, 159, 64, 0.2)',
                                                            'rgba(255, 205, 86, 0.2)',
                                                            'rgba(75, 192, 192, 0.2)',
                                                            'rgba(54, 162, 235, 0.2)',
                                                            'rgba(153, 102, 255, 0.2)',
                                                            'rgba(201, 203, 207, 0.2)',
                                                            'rgba(255, 99, 132, 0.2)',
                                                            'rgba(255, 159, 64, 0.2)',
                                                            'rgba(255, 205, 86, 0.2)',
                                                            'rgba(75, 192, 192, 0.2)',
                                                            'rgba(54, 162, 235, 0.2)'
                                                            ],
                                'borderColor'         : [
                                                            'rgb(255, 99, 132)',
                                                            'rgb(255, 159, 64)',
                                                            'rgb(255, 205, 86)',
                                                            'rgb(75, 192, 192)',
                                                            'rgb(54, 162, 235)',
                                                            'rgb(153, 102, 255)',
                                                            'rgb(201, 203, 207)',
                                                            'rgb(255, 99, 132)',
                                                            'rgb(255, 159, 64)',
                                                            'rgb(255, 205, 86)',
                                                            'rgb(75, 192, 192)',
                                                            'rgb(54, 162, 235)'
                                                        ],
                                'borderWidth'          : 1
                            })
                        }); 

                        const data = {
                            labels: isi_periode,
                            datasets: datasets
                        };
                        var ctx1 = document.getElementById('barChart');
                        var context1 = ctx1.getContext('2d');
                        context1.clearRect(0, 0, context1.width, context1.height);
                        var barChart = new Chart(context1, {
                            type: 'bar',
                            data: data,
                            options: {
                                scales: {
                                y: {
                                    beginAtZero: true
                                }
                                }
                            },
                        });
                    }
                });

            });
    }
</script>