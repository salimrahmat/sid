<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $data['order']['total'];?></h3>
                <p>Orders</p>
            </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
            <a href="<?php echo site_url('home/moreOrder')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
            <h3><?php echo $data['order_cancel']['total'];?></h3>
                <p>Orders Cancel</p>
            </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
            <a href="<?php echo site_url('home/Order_Cancel')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo $data['member']['total'];?></h3>
            <p>Member Registrations</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="<?php echo site_url('member/lists')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="row">
        <div class="box-body">
            <div class="box-header with-border">
                <h3 class="box-title">Monthly Sale</h3>
                <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                <canvas id="areaChart" style="height:180px"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>/assets/bower_components/chart.js/Chart.js"></script>
<script type="text/javascript">
    window.onload = function(event) {
        $(document).ready(function() {
            areaChart(<?php echo json_encode($data['chart']);?>);
        });
    }

    function areaChart(result){
        var isi_periode = [];
        var datasets   = [];
        var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
        var areaChart       = new Chart(areaChartCanvas)
        $(result).each(function(i){  
            if (!isi_periode.includes(result[i].periode)) {
                isi_periode.push(result[i].periode)
                } 
                datasets.push({
                       'label'               : result[i].label,
                       'fillColor'           : 'rgba(210, 214, 222, 1)',
                       'strokeColor'         : 'rgba(210, 214, 222, 1)',
                       'pointColor'          : 'rgba(210, 214, 222, 1)',
                       'pointStrokeColor'    : '#c1c7d1',
                       'pointHighlightFill'  : '#fff',
                       'pointHighlightStroke': 'rgba(220,220,220,1)',
                       'data'                :  result[i].data
                });    
        });
        var areaChartData = {
        labels  : isi_periode,
        datasets: datasets
        // [
        //     {  
        //     label               : 'Electronics',
        //     fillColor           : 'rgba(210, 214, 222, 1)',
        //     strokeColor         : 'rgba(210, 214, 222, 1)',
        //     pointColor          : 'rgba(210, 214, 222, 1)',
        //     pointStrokeColor    : '#c1c7d1',
        //     pointHighlightFill  : '#fff',
        //     pointHighlightStroke: 'rgba(220,220,220,1)',
        //     data                : [65, 59, 80, 81, 56, 55, 40]
        //     },
        //     {
        //     label               : 'Digital Goods',
        //     fillColor           : 'rgba(60,141,188,0.9)',
        //     strokeColor         : 'rgba(60,141,188,0.8)',
        //     pointColor          : '#3b8bba',  
        //     pointStrokeColor    : 'rgba(60,141,188,1)',
        //     pointHighlightFill  : '#fff',
        //     pointHighlightStroke: 'rgba(60,141,188,1)',
        //     data                : [28, 48, 40, 19, 86, 27, 90]
        //     }
        // ]
        }
        var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        // legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
        }
        //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)
    }
</script>
