<?php
echo form_open(($data['flow_data']['user_id'] !==NULL ? 'user/update/' . $data['flow_data']['user_id'] : 'user/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('user/form')]); 
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div>
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group has-feedback">
                        <label for="user_name">Email</label>
                        <?php
                        echo form_input([
                            'name'          => "user_name",
                            'placeholder'   => "Email",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('user_name', ($data['flow_data']['user_name'] !== NULL ? $data['flow_data']['user_name'] : NULL), FALSE)
                        ]);?>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="password">Password</label>
                        <?php
                        echo form_password([
                            'name'          => "password",
                            'placeholder'   => "Password",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('user_name', ($data['flow_data']['password'] !== NULL ? $data['flow_data']['password'] : NULL), FALSE)
                        ]);?>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group">
                        <label for="access">Akses</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Akses';
                        if ($data['akses_user'] !== NULL ){
                            foreach ($data['akses_user'] as $row) {
                                     $options[$row['master_code']] = $row['descr'];
                                }
                        }
                        echo form_dropdown('access', $options, set_value('tipe_ternak', ($data['flow_data']['access'] !==NULL ? $data['flow_data']['access'] : NULL), FALSE), 'id="access" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="is_active">Status</label>
                        <br>
                        <?php
                            echo form_radio([
                                'name'          => "is_active",
                                'value'         => "1",   
                                'checked'       => ($data['flow_data']['is_active'] == 1 ? TRUE : FALSE),  
                                'class'         => 'privilege-radio',
                                'required'      => 'required',
                                'id'            => 'taxyes'
                            ]);
                            echo form_label('Active');
                            echo form_radio([
                                'name'          => "is_active",
                                'value'         => "0",
                                'checked'       => ($data['flow_data']['is_active'] == 1 ? FALSE : TRUE),
                                'required'      => 'required',
                                'class'         => 'privilege-radio',
                                'id'            => 'taxno'
                            ]);
                            echo form_label('Non Active');
                        ?>
                    </div>
                </div>
            </div>
            <div class="box-footer">    
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div> 
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    };
</script>