<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_USER,PRIVILEGE_CREATE)) { ?>
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('user/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                <?php
                  } 
                ?>    
                </div>
                <?php
                    echo form_open('user/search', 
                    [
                        'name'   => 'form-search-ternak',
                        'method' => 'post',
                        'id'     => 'ternak',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Akses';
                        foreach ($data['akses_user'] as $row) {
                                    $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('akses_user', $options, set_value('akses_user', NULL, FALSE), 'id="akses_user" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Akses</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    <?php
                        foreach ($data['user_list']['records'] as $idx => $record)
                        {?>
                            <tr>
                                <td><?php echo $idx + 1; ?></td>
                                <td><?php echo $record->USERNAME;?></td>
                                <td><?php echo $record->DESCR;?></td>
                                <td><?php echo ($record->IS_ACTIVE === "1" ? 'Active' : 'Non Active');?></td>
                                <td class="text-center">
                                <?php 
                                  if(is_privilege(PRIVILEGE_USER,PRIVILEGE_UPDATE)) { ?>
                                    <a href="<?php echo site_url('user/form/' . $record->ID_USER);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Edit  <?php echo html_escape($record->USERNAME);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                <?php
                                  } 
                                ?>    
                                </td>
                            </tr>
                    <?php 
                        }
                    ?>
                </table>
            </div>   
        </div>
    </div>
</div>