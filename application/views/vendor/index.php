<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                    <div class="col-lg-2">
                    <?php 
                      if(is_privilege(PRIVILEGE_VENDOR,PRIVILEGE_CREATE)) { ?>
                        <a class="btn btn-block btn-primary" href="<?php echo site_url('vendor/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                    <?php
                      }
                    ?>
                    </div>
                    <div class="box-tools">
                        <?php
                        echo form_open('vendor/search', 
                        [
                            'name'          => 'form-search',
                            'method'        => 'post']); 
                        ?>
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>No</th>
                            <th>Nama Vendor</th>
                            <th>Alamat Vendor</th>
                            <th>Phone Vendor</th>
                            <th>Pic</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        <?php
                            foreach ($data['vendors']['records'] as $idx => $record)
                            {?>
                            <tr>
                                <td><?php echo $idx + 1; ?></td>
                                <td><?php echo nl2br(html_escape($record->VENDOR_NAME));?></td>
                                <td><?php echo nl2br(html_escape($record->VENDOR_ADDRESS));?></td>
                                <td><?php echo nl2br(html_escape($record->VENDOR_PHONE));?></td>
                                <td><?php echo nl2br(html_escape($record->VENDOR_PIC));?></td>
                                <td><?php echo $record->STATUS ;?></td>
                                <td class="text-center">
                                <?php 
                                  if(is_privilege(PRIVILEGE_VENDOR,PRIVILEGE_UPDATE)) { ?>
                                    <a href="<?php echo site_url('vendor/form/' . $record->ID_VENDOR);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Edit <?php echo html_escape($record->VENDOR_NAME);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                <?php
                                  } 
                                ?>
                                </td>
                            </tr>
                        <?php 
                            }?>
                    </table>
            </div>
        </div>
    </div>
</div>