<?php
echo form_open(($data['flow_data']['vendor_id'] !==NULL ? 'vendor/update/' . $data['flow_data']['vendor_id'] : 'vendor/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 

?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div> 
        <div class="box box-danger">
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="vendor_name">Nama Vendor</label>
                        <?php
                        echo form_input([
                            'name'          => "vendor_name",
                            'placeholder'   => "Nama Vendor",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('vendor_name', ($data['flow_data']['vendor_name'] !== NULL ? $data['flow_data']['vendor_name'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="vendor_address">Alamat Vendor</label>
                        <?php
                        echo form_textarea([
                            'name'          => "vendor_address",
                            'placeholder'   => "Nama Vendor",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('vendor_address', ($data['flow_data']['vendor_address'] !== NULL ? $data['flow_data']['vendor_address'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label>Phone Vendor</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                            </div>
                            <input type="text" name="vendor_phone" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask value= "<?php echo ($data['flow_data']['vendor_phone'] !== NULL ? $data['flow_data']['vendor_phone'] : NULL);?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="vendor_pic">Pic </label>
                        <?php
                        echo form_input([
                            'name'          => "vendor_pic",
                            'placeholder'   => "PIC",
                            'class'         => "form-control",
                            'required'      => 'required',
                            'value'         => set_value('vendor_pic', ($data['flow_data']['vendor_pic'] !== NULL ? $data['flow_data']['vendor_pic'] : NULL), FALSE)
                        ]);?>
                    </div>
                    <div class="form-group">
                    <label for="status">Status</label>
                    <br>
                    <?php
                        echo form_radio([
                            'name'          => "status",
                            'value'         => "1",     
                            'checked'       => TRUE,
                            'class'         => 'privilege-radio',
                            'required'      => 'required',
                            'value'         => ($data['flow_data']['status'] !== 1 ? 1 : $data['flow_data']['status'])
                        ]);
                        echo form_label('Active');
                        echo form_radio([
                            'name'          => "status",
                            'value'         => "0",     
                            'checked'       => FALSE,
                            'required'      => 'required',
                            'class'         => 'privilege-radio',
                            'value'         => ($data['flow_data']['status'] !== 0 ? 0 : $data['flow_data']['status'])
                        ]);
                        echo form_label('Inactive');
                    ?>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                        <i class="fa fa-undo"></i> Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
    };
</script>