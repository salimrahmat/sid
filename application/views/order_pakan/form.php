<?php
$orders = $data['order'];
echo form_open(($data['flow_data']['id_stock_food'] !==NULL ? 'order_pakan/update/' . $data['flow_data']['id_stock_food'] : 'order_pakan/save'), 
[
    'name'          => 'form-input',
    'method'        => 'post'],
[
    'return_url'    => url_get_return('registrasi/form')]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php 
                foreach ($data['flow_data']['value'] as $gagal) {
                echo $gagal;
                } 
            ?>
        </div>  
        <div class="box box-danger">
            <div class="box-body">
                <div class="form-group">
                    <label for="id_vendor">Nama Vendor</label>
                    <?php
                    $options = [];
                    $options[''] = 'Pilih Vendor';
                    if ($data['id_vendor'] !== NULL ){
                        foreach ($data['id_vendor'] as $row) {
                          $options[$row['master_code']] = $row['descr'];
                        }
                    }
                    echo form_dropdown('id_vendor', $options, set_value('id_vendor', ($data['flow_data']['id_vendor'] !==NULL ? $data['flow_data']['id_vendor'] : NULL), FALSE), 'id="id_vendor" class="form-control select2" data-width="100%" required="required"');
                    ?>
                </div>
                <div class="form-group">
                    <label for="vendor_address">Alamat Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_address",
                        'placeholder'   => "Alamat Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_address",
                        'readonly'      => 'readonly',
                        'value'         => set_value('vendor_address', ($data['flow_data']['alamat_vendor'] !== NULL ? $data['flow_data']['alamat_vendor'] : NULL), FALSE)
                    ]);?>
                </div> 
                <div class="form-group">
                    <label for="vendor_phone">Phone Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_phone",
                        'placeholder'   => "Phone Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_phone",
                        'readonly'      => 'readonly',
                        'value'         => set_value('phone_vendor', ($data['flow_data']['phone_vendor'] !== NULL ? $data['flow_data']['phone_vendor'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="vendor_pic">Pic Vendor</label>
                    <?php
                    echo form_input([
                        'name'          => "vendor_pic",
                        'placeholder'   => "Pic Vendor",
                        'class'         => "form-control",
                        'id'            => "vendor_pic",
                        'readonly'      => 'readonly',
                        'value'         => set_value('pic_vendor', ($data['flow_data']['pic_vendor'] !== NULL ? $data['flow_data']['pic_vendor'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="order_date">Tanggal Order</label>
                    <?php
                    echo form_input([
                        'name'              => "order_date",
                        'class'             => "form-control",
                        'id'                => "datepicker",
                        'placeholder'       => "Tanggal Order",
                        'required'          => 'required',
                        'value'             => set_value('order_date', ($data['flow_data']['order_date'] !== NULL ? $data['flow_data']['order_date'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group">
                    <label for="tax">Tax</label>
                    <br>
                    <?php
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "Ya",   
                            'checked'       => ($data['flow_data']['tax'] == 1 ? TRUE : FALSE),  
                            'class'         => 'privilege-radio',
                            'required'      => 'required',
                            'id'            => 'taxyes'
                        ]);
                        echo form_label('Ya');
                        echo form_radio([
                            'name'          => "tax",
                            'value'         => "No",
                            'checked'       => ($data['flow_data']['tax'] == 1 ? FALSE : TRUE),
                            'required'      => 'required',
                            'class'         => 'privilege-radio',
                            'id'            => 'taxno'
                        ]);
                        echo form_label('Tidak');
                    ?>
                </div>
                <div class="form-group">
                    <label for="tax_value">Tax Value</label>
                    <?php
                    echo form_input([
                        'name'              => "tax_value",
                        'class'             => "form-control",
                        'placeholder'       => "tax value",
                        'readonly'          => 'readonly',
                        'value'             => set_value('tax_value', ($data['flow_data']['tax_value'] !== NULL ? $data['flow_data']['tax_value'] : NULL), FALSE)
                    ]);?>
                </div>
                <div class="form-group"> 
                        <label for="arrival_date">Estimasi Kedatangan</label>
                        <?php
                        echo form_input([
                            'name'              => "arrival_date",
                            'class'             => "form-control",
                            'id'                => "datepicker1",
                            'placeholder'       => "Tanggal Order",
                            'required'          => 'required',
                            'value'             => set_value('arrival_date', ($data['flow_data']['arrival_date'] !== NULL ? $data['flow_data']['arrival_date'] : NULL), FALSE)
                        ]);?>
                </div>
            </div>
            <div class="box-body table-body">
                <div class="table-row">
                    <div class="form-group">
                        <label for="jenis_pakan">Jenis Pakan</label>
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Jenis Pakan';
                        if ($data['tipe_pakan'] !== NULL ){
                            foreach ($data['tipe_pakan'] as $row) {
                            $options[$row['master_code']] = $row['descr'];
                            }
                        }
                        echo form_dropdown('jenis_pakan[]', $options, NULL, 'id="jenis_pakan" class="form-control select2" data-width="100%" required="required"');
                        ?>
                    </div>            
                    <div class="form-group">
                        <label for="total_order">Quantity KG</label>
                        <?php
                        echo form_input([
                            'name'          => "total_order[]",
                            'placeholder'   => "Quantity KG",
                            'class'         => "form-control",
                            'id'            => "total_order",
                            'type'          => "decimal",
                            'required'      => 'required',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="price_food">Harga PerKilo</label>
                        <?php
                        echo form_input([
                            'name'              => "price_food[]",
                            'class'             => "form-control",
                            'placeholder'       => "Harga PerKilo",
                            'id'                => "price_food",
                            'required'          => 'required',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="total">Total</label>
                        <?php
                        echo form_input([
                            'name'              => "total[]",
                            'class'             => "form-control",
                            'placeholder'       => "total",
                            'id'                => "total",
                            'required'          => 'required',
                            'readonly'          => 'readonly',
                        ]);?>
                    </div>
                    <div class="form-group">
                        <label for="biaya_lain">Biaya Lain</label>
                        <?php
                        echo form_input([
                            'name'              => "biaya_lain[]",
                            'class'             => "form-control",
                            'placeholder'       => "Biaya Lain",
                            'required'          => 'required',
                        ]);?>
                    </div>
                    <?php 
                        if (count($orders) == 0) { ?>
                            <a  class="remove text-danger" title="Hapus">
                                <span class="fa fa-trash"></span>
                            </a>   
                    <?php
                        }
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <?php 
                    if (count($orders) == 0) { ?>
                        <a  class="addMore" title="Tambah">
                            <span class="fa fa-plus"></span>
                        </a> 
                <?php 
                    }
                ?>        
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-default" href="<?php echo site_url(url_get_return());?>">
                    <i class="fa fa-undo"></i> Batal
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
    window.onload = function(event) {
        tableInit();
        tableFirst();
        $('.addMore').on('click', function() {
            tableAdd(null);
        });

        let formForm = $('form[name="form-input"]');

        formForm.submit(function() {
            InputHelper.input_release($(this));
            return true;
        });

        <?php 
        if ( count($data['flow_data']['value']) > 0) { ?>
            $('.alert-dismissible').show();
        <?php
        } else { ?>
            $('.alert-dismissible').hide();
        <?php    
        }
        ?>
     
        $('#id_vendor').on("select2:select", function(e) { 
            $.ajax({
                url: '<?php echo site_url('order_pakan/detailVendor'); ?>',
                type: "post",
                dataType  : 'json',
                data: {searchTerm: e.params.data.id},
                success: function(d) {
                    $.each( d, function( i, val ) {
                           $('#vendor_address').val(val['address']);
                           $('#vendor_phone').val(val['phone']);
                           $('#vendor_pic').val(val['pic']);
                    });
                }
            });
        });

        $('#taxyes').click(function() {
            $.ajax(
                {url: '<?php echo site_url('order_pakan/getSelectTax')?>', 
                 success: function(result){
                    $("input[name='tax_value']").val(result);
                }
            });
        });
        $('#taxno').click(function() {
            $("input[name='tax_value']").val('');
        });
   
       
    };
    let trbase = null;
    function tableInit(){
        let tr = $('.table-row');
        trbase = tr.clone(false, false);
        tr.remove();
    }

    function tableFirst(){
        <?php
        if (count($orders) > 0)
        {
        foreach ($orders as $order)
        {?>
        tableAdd(<?php echo json_encode($order);?>);
        <?php
        }
        }
        else
        {?>
        tableAdd(null);
        <?php
        }?>
    }
    let idx=0;
    function tableAdd(order) {
        let trNew = trbase.clone();
        $('.table-body').append(trNew);

        if (order !== null) {
            $('[name="total_order[]"]',trNew).val(order.total_order);
            $('[name="price_food[]"]', trNew).val(order.price_food);
            $('[name="total[]"]', trNew).val(order.total);
            $('[name="biaya_lain[]"]', trNew).val(order.biaya_lain); 
            $('[name="tipe_pakan[]"]', trNew)
                    .prepend(
                        $('<option>', { value : order.tipe_pakan.id })
                            .text(order.nama_pakan.name)
                    )
                    .val(order.tipe_pakan.id);  
        }
        tableAttach(trNew);
    }

    let idxEnabled=0;
    function tableAttach(trNew) {

        $("#price_food", trNew).on("change", function(){
            $('#total', trNew).val($(this).val() * $('#total_order', trNew).val());
        });

        $('[name="jenis_pakan[]"]', trNew)
        $('[name="arrival_date[]"]', trNew).attr('id','datepicker1');

        $('.remove', trNew).click(function (e) {
            e.preventDefault();

            let tableRow = $(this).parent();
                tableRow.remove();
                modal.modal('hide');
        });
    }
</script>