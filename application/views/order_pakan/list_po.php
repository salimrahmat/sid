<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama Vendor</th>
                        <th>Tanggal Order</th>
                        <th>Total Order</th>
                        <th>Total Harga KG</th>
                        <th>Total Biaya Lain</th>
                        <th>Total </th>
                        <th>Status </th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['order_pakan_po']['records'] as $idx => $record)
                        {?>
                    <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->VENDOR_NAME);?></td>
                            <td><?php echo date_reformat($record->ORDER_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->TOTAL_ORDER;?></td>   
                            <td><?php echo $record->TOTAL_HARGA_KG;?></td>
                            <td><?php echo $record->BIAYA_LAIN;?></td>
                            <td><?php echo $record->TOTAL;?></td>
                            <td><?php echo $record->STATUS;?></td>
                            <td class="text-center">
                                <a href="<?php echo site_url('order_pakan/pdf_po/'.$record->ID_VENDOR.'/'.$record->ORDER_DATE.'/'.$record->ORDER_NO);?>?<?php echo url_create_return_query();?>"
                                    class="btn btn-flat btn-sm" target="_blank"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="Print PO <?php echo html_escape($record->VENDOR_NAME);?>">
                                    <i class="fa fa-file-pdf-o"></i>
                                </a>
                                <!-- <a class="btn btn-primary btn-receive"
                                    data-toggle="tooltip" data-placement="bottom"
                                    data-message="<?php echo $record->ORDER_NO;?>"
                                    title="Detail application <?php echo html_escape($record->VENDOR_NAME);?>">
                                    Detail
                                </a>    -->
                                <?php 
                                    if($record->STATUS == "Print Po") { ?>
                                    <!-- <a href="<?php echo site_url('order_pakan/receive/'.$record->ORDER_NO);?>?<?php echo url_create_return_query();?>" -->
                                    <a class="btn btn-primary btn-receive"        
                                       data-toggle="tooltip" data-placement="bottom"
                                       data-message="<?php echo $record->ORDER_NO;?>"
                                       title="Receive <?php echo html_escape($record->VENDOR_NAME);?>">
                                       <i class="fa fa-thumbs-up"></i>
                                    </a>
                                <?php    
                                    }
                                ?>
                            </td>
                        </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['order_pakan_po']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-detial" tabindex="-1" role="dialog" id="exampleModal">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive no-padding" id="modal-table">
                </div>                
                <label for="biaya_lain">Detail</label>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tipe Pakan</th>
                            <th>Qty</th>
                            <th>Harga Per KG</th>
                            <th>Total</th>
                            <th>Biaya Lain</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>   
                <div class="form-group">
                    <label for="actual_date">Actual Kedatangan</label>
                    <?php
                    echo form_input([
                        'name'              => "actual_date",
                        'class'             => "form-control",
                        'id'                => "datepicker1",
                        'placeholder'       => "Actual Kedatangan",
                        'required'          => 'required',
                        'value'             => set_value('arrival_date', NULL, FALSE)
                    ]);?>
                </div>         
                <p id="orderNo">asdasdsad</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-submit">Received</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('.btn-submit').click(function(e){      
        if($('input[name="actual_date"]').val()===""){
            alert("Please add actual kedatangan");
            return
        }
        var actual_date = new Date($('input[name="actual_date"]').val());
        var modalRemove = $('.modal-detial');
        $.ajax({
            url:"<?php echo site_url('order_pakan/receive')?>",    
            type: "post",
            dataType: 'json',
            data: {order_no: $('#orderNo').text(),
                   actual_date: actual_date.getFullYear()+'-'+actual_date.getMonth()+'-'+actual_date.getDay()
            },
            success:function(result){
                console.log(result);
                modalRemove.hide();
                window.top.location = window.top.location;

            },
            error:function(status,error){
                console.log(error);
            }
        });

  });

  $('.btn-receive').click(function(e) {
     e.preventDefault();
     let rowData = $(this);
     let modalRemove = $('.modal-detial');
    $.ajax({
            url:"<?php echo site_url('order_pakan/detailReceive')?>",    
            type: "post",
            dataType: 'json',
            data: {order_no: rowData.attr('data-message')
            },
            success:function(result){
                var HTML = "";    
                    HTML  = "<table class='table'>";
                $.each(result['header'], function( index, value) {
                    HTML +=['<tr><td>'+result['header'][index].name+'</td><td>'+result['header'][index].value+'</td></tr>']; 
                });
                    HTML += "</table>";    
                $('#modal-table').html(HTML);   
                $.each(result['detail'], function(index) {
                    $('table.table.table-bordered>tbody').append(function() {
                        var tr = $('<tr>');
                        tr.append(function() {
                            var td = $('<td>');
                            td.html(result['detail'][index].pakan);
                            return td;
                        });
                        tr.append(function() {
                            var td = $('<td>');
                            td.html(result['detail'][index].qty);
                            return td;
                        });
                        tr.append(function() {
                            var td = $('<td>');
                            td.html(result['detail'][index].hargakg);
                            return td;
                        });
                        tr.append(function() {
                            var td = $('<td>');
                            td.html(result['detail'][index].total);
                            return td;
                        });
                        tr.append(function() {
                            var td = $('<td>');
                            td.html(result['detail'][index].biaya);
                            return td;
                        });
                        
                        return tr;
                    }());
                });
                $("#orderNo").text(result['header']['order'].value);
                $("#orderNo").css('visibility', 'hidden');
            },
            error:function(status,error){
                console.log(error);
            }
        });

    modalRemove.modal({
            backdrop: 'static',
            keyboard: false
        });
    
  });
</script>