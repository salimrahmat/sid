<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_CREATE)) { ?>
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('order_pakan/form');?>?<?php echo url_create_return_query();?>">Tambah</a>        
                <?php
                  } 
                ?>    
                </div>
                <div class="col-md-2">
                <?php 
                  if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_DETAIL)) { ?>
                    <a class="btn btn-block btn-primary" href="<?php echo site_url('order_pakan/listPo');?>?<?php echo url_create_return_query();?>">Print PO & Receive</a>        
                <?php
                  } 
                ?>    
                </div>
                <?php
                    echo form_open('order_pakan/search', 
                    [
                        'name'   => 'form-search-pakan',
                        'method' => 'post',
                        'id'     => 'pakan',
                        ]); 
                    ?>
                    <div class="col-md-2">
                        <?php
                        $options = [];
                        $options[''] = 'Pilih Tipe Pakan';
                        foreach ($data['tipe_pakan'] as $row) {
                                 $options[$row['master_code']] = $row['descr'];
                        }
                        echo form_dropdown('tipe_pakan', $options, set_value('tipe_pakan', NULL, FALSE), 'id="tipe_pakan" class="form-control select2" data-width="100%"  required="required"');
                        ?>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama Vendor</th>
                        <th>Tipe Pakan</th>
                        <th>Tanggal Order</th>
                        <th>Tanggal Datang</th>
                        <th>Tanggal Actual</th>
                        <th>Quantity KG</th>
                        <th>Harga KG</th>
                        <th>Total </th>
                        <th>Biaya Lain</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php
                        foreach ($data['order_pakan']['records'] as $idx => $record)
                        {?>
                    <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->VENDOR_NAME);?></td>
                            <td><?php echo html_escape($record->DESCR);?></td>
                            <td><?php echo date_reformat($record->ORDER_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo date_reformat($record->ARRIVAL_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo date_reformat($record->ACTUAL_DATE, 'j M Y', '&ndash;');?></td>
                            <td><?php echo $record->ORDER_TOTAL;?></td>   
                            <td><?php echo $record->PRICE_KG;?></td>
                            <td><?php echo $record->TOTAL;?></td>
                            <td><?php echo $record->BIAYA_LAIN;?></td>
                            <td><?php echo $record->STATUS;?></td>
                            <td class="text-center">
                            <?php 
                                if(is_privilege(PRIVILEGE_ORDER_PAKAN,PRIVILEGE_UPDATE)) { 
                                    if ($record->STATUS==='Order') {  
                                ?>
                                    <a href="<?php echo site_url('order_pakan/form/' . $record->ID_STOCK_FOOD);?>?<?php echo url_create_return_query();?>"
                                        class="btn btn-flat btn-sm"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Mengubah <?php echo html_escape($record->DESCR);?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                            <?php
                                    }
                                } 
                            ?>
                            </td>
                        </tr>    
                    <?php 
                        }
                    ?>
                </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                echo $data['order_pakan']['pagination'];
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function(event) {
        $('#tipe_pakan').change(function(){
            $('#pakan').submit();
        });        
    };
</script>
