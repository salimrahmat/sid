<div class="row">
    <div class="col-md-12">
        <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Jenis Pakan</th>
                    <th>Total Order</th>
                    <th>Pakan Keluar</th>
                    <th>Pakan Tersedia</th>
                </tr>
                    <?php
                        foreach ($data['stock_pakan']['records'] as $idx => $record) 
                    { ?>
                        <tr>
                            <td><?php echo $idx + 1; ?></td>
                            <td><?php echo html_escape($record->DESCR);?></td>
                            <td><?php echo $record->STOCK;?></td>
                            <td><?php echo $record->PAKAN_KELUAR;?></td>
                            <td><?php echo $record->STOCK_TERSEDIA;?></td> 
                        </tr>
                    <?php 
                        }
                    ?>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php
              echo $data['stock_pakan']['pagination'];
            ?>
        </div>
     </div>
</div>