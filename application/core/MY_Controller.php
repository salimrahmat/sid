<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $data = array();
    public $privilegeIds = [];

    public function __construct()
    {
        parent::__construct();
        $this->list_user_access();
        $this->result = new variable_error();
    }

    /*
     * Configure Layout 
     */
    public function layout() {
        $this->template['header'] = $this->load->view('_layout/header',$this->data,TRUE);
        $this->template['navbar'] = $this->load->view('_layout/navbar',$this->data,TRUE);
        $this->template['sidebar'] = $this->load->view('_layout/sidebar',$this->data,TRUE);
        $this->template['breadcrumb'] = $this->load->view('_layout/breadcrumb',$this->data,TRUE);
        $this->template['footer'] = $this->load->view('_layout/footer',$this->data,TRUE);
        $this->template['sidebarControl'] = $this->load->view('_layout/sidebar-control',$this->data,TRUE);
        $this->template['js'] = $this->load->view('_layout/js',$this->data,TRUE);
        $this->template['page'] = $this->load->view($this->page,$this->data,TRUE);
        $this->load->view('_layout/main',$this->template);
    }    

    /**
     *  
     */
    public function set_breadcrump($title, $item){
        $newdata = array(
            'title' => $title,
            'item'  => $item
        );
    
        $this->session->set_flashdata($newdata);

    }

    /**
     * 
     */
    public function session_login($username, $useraccess, $userdesc, $idUser){
        $newdata = array(
            'username'   => $username,
            'useraccess' => $useraccess,
            'userdesc'   => $userdesc,
            'iduser'     => $idUser,
        );

        $this->session->set_userdata($newdata);
    }


    public function verifyLogin() {
        if (!$this->session->userdata('username')) {
             redirect('login');   
        } 
    }

    /**
     * 
     */
    public function get_paging($query,$url){

        $config['base_url'] = site_url($url);
        $config['total_rows'] = $this->db->query($query)->num_rows();
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;

        $config['full_tag_open']   = '<ul class="pagination pagination-sm no-margin pull-left">';
        $config['full_tag_close']  = '</ul>';
        
        $config['first_link']      = 'First'; 
        $config['first_tag_open']  = '<li>';
        $config['first_tag_close'] = '</li>';
        
        $config['last_link']       = 'Last'; 
        $config['last_tag_open']   = '<li>';
        $config['last_tag_close']  = '</li>';
        
        $config['next_link']       = ' <i class="glyphicon glyphicon-menu-right"></i> '; 
        $config['next_tag_open']   = '<li>';
        $config['next_tag_close']  = '</li>';
        
        $config['prev_link']       = ' <i class="glyphicon glyphicon-menu-left"></i> '; 
        $config['prev_tag_open']   = '<li>';
        $config['prev_tag_close']  = '</li>';
        
        $config['cur_tag_open']    = '<li class="active"><a href="#">';
        $config['cur_tag_close']   = '</a></li>';
         
        $config['num_tag_open']    = '<li>';
        $config['num_tag_close']   = '</li>';

        $this->pagination->initialize($config); // Set konfigurasi paginationnya
    
        $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;
        $query .= " LIMIT ".$page.", ".$config['per_page'];
        
        $data['limit'] = $config['per_page'];
        $data['total_rows'] = $config['total_rows'];
        $data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas
        $data['records'] = $this->db->query($query)->result();
        
        return $data;
    }

    public function set_stock() {
        $this->db->where('TOTAL_OUT =', 0);
        $this->db->delete('TBL_STOCK_FARM');

        $this->db->select('COUNT(REGISTER_NO) as TOTAL,TYPE_FARM,TYPE_CAGE,TYPE_LIVESTOCK');
        $this->db->from('TBL_FARM');
        $this->db->where('STATUS', STATUS_ACTIVE);
        $this->db->group_by(array("TYPE_FARM", "TYPE_CAGE","TYPE_LIVESTOCK")); 
        $query = $this->db->get();

        foreach ($query->result() as $row)
        {
            $data = array('TYPE_FARM'      => $row->TYPE_FARM,
                          'TYPE_CAGE'      => $row->TYPE_CAGE,
                          'TYPE_LIVESTOCK' => $row->TYPE_LIVESTOCK,
                          'TOTAL_IN'       => $row->TOTAL,
                          'TOTAL_OUT'      => 0,
                          'CREATE_DATE'    => date('Y-m-d H:i:s'),
                          'USR_CRT'        => $this->session->userdata('iduser'),
                    );    
            $this->db->insert('TBL_STOCK_FARM', $data);        
        }
    }

    public  function getRegisterNo($searchTerm = "-"){

        // Fetch users
        $this->db->select('*');
        $this->db->where("REGISTER_NO like '%".$searchTerm."%'  ");
        $this->db->where("STATUS = ".STATUS_ACTIVE);
        $fetched_records = $this->db->get('TBL_FARM');
        $farms = $fetched_records->result_array();
   
        // Initialize Array with fetched data
        $data = array();
        foreach($farms as $farm){
           $data[] = array("id"=>$farm['REGISTER_NO'], "text"=>$farm['REGISTER_NO']);
        }
        return $data;
    }

    public  function getMember($searchTerm = "-"){

        // Fetch users
        $this->db->select('*');
        $this->db->where("MEMBER_NAME like '%".$searchTerm."%'  ");
        $fetched_records = $this->db->get('TBL_MEMBER');
        $farms = $fetched_records->result_array();
   
        // Initialize Array with fetched data
        $data = array();
        foreach($farms as $farm){
           $data[] = array("id"=>$farm['MEMBER_ID'], "text"=>$farm['MEMBER_NAME']);
        }
        return $data;
    }

    public function getPoNumber(){
        $number ="";
        $year   = '%y';
        $month  = '%m';
        $time   = time();  
        if (date("d",(now())) === "01") {
            
            $config['id']     = 0;
            $config['awalan'] = 'PO'.mdate($month, $time);
            $config['digit']  = 5;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', 1);
            $this->db->where('MASTER_CODE', PO_NUMBER);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        } else {
            $this->db->select('RUN_NUMBER');
            $this->db->from('TBL_SEQUENCE_NUMBER');
            $this->db->where('MASTER_CODE', PO_NUMBER);
            $query = $this->db->get();

            $config['id']     = $query->row('RUN_NUMBER');
            $config['awalan'] = 'PO'.mdate($month, $time);
            $config['digit']  = 5;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', $query->row('RUN_NUMBER')+1);
            $this->db->where('MASTER_CODE', PO_NUMBER);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        }
        return $number;

    }

    public function getOrderNo(){
        $number ="";
        $year   = '%y';
        $month  = '%m';
        $time   = time();  
        if (date("d",(now())) === "01") {
            
            $config['id']     = 0;
            $config['awalan'] = TYPE_RUN_ORDER.mdate($month, $time).mdate($year, $time);
            $config['digit']  = 6;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', 1);
            $this->db->where('MASTER_CODE', TYPE_RUN_ORDER);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        } else {
            $this->db->select('RUN_NUMBER');
            $this->db->from('TBL_SEQUENCE_NUMBER');
            $this->db->where('MASTER_CODE', TYPE_RUN_ORDER);
            $query = $this->db->get();

            $config['id']     = $query->row('RUN_NUMBER');
            $config['awalan'] = TYPE_RUN_ORDER.mdate($month, $time).mdate($year, $time);
            $config['digit']  = 6;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', $query->row('RUN_NUMBER')+1);
            $this->db->where('MASTER_CODE', TYPE_RUN_ORDER);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        }

        return $number;
    }

    public function list_user_access(){

        $this->db->select('*');
        $this->db->from('TBL_ACCESS_MENU');
        $this->db->where('ID_USER_ACCESS', $this->session->userdata('useraccess'));
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $idx => $row)
        {
            $record[$row->MENU_NAME] = [
                'menu_name'     => $row->MENU_NAME,
                'menu_list'     => $row->MENU_LIST,
                'menu_add'      => $row->MENU_ADD,
                'menu_edit'     => $row->MENU_EDIT,
                'menu_delete'   => $row->MENU_DELETE,
                'menu_detail'   => $row->MENU_DETAIL,
            ];
            $records = $record;    
        }
        
        $this->privilegeIds = $records;
    }

    public function setError($param){
        $this->result->setresult($param);
    }

    public function getError(){
        $data   = [];
        $data[] = $this->result->getresult();

        return $data;
    }

} 

class variable_error{
    private $result;

    public function setresult($a){
		$this->result = $a;
	}

	public function getresult() {
		return $this->result; 
	}
}