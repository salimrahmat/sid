<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('number_format_trim'))
{
    /**
     * @param int|double|float|null $number
     * @param int $decimals
     * @param string $decimalPoint
     * @param string $thousandsSeparator
     * @return string|null
     */
    function number_format_trim($number, $decimals = 0, $decimalPoint = '.', $thousandsSeparator = ',')
	{
	    if ($number === NULL)
	        return NULL;

		if (is_int($number) || $decimals === 0)
			$value = number_format($number, 0, $decimalPoint, $thousandsSeparator);
		else
		{
			$value = number_format($number, $decimals, $decimalPoint, $thousandsSeparator);
			$value = rtrim($value, 0);
            $value = rtrim($value, $decimalPoint);
		}
		
		if ($value === '')
			$value = '0';

		return $value;
	}
}

if ( ! function_exists('number_to_spelling_bahasa'))
{
    /**
     * @param int $value
     * @return string
     */
    function number_to_spelling_bahasa($value)
	{
        $numberNames = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($value < 12)
            return " " . $numberNames[$value];
        elseif ($value < 20)
            return number_to_spelling_bahasa($value - 10) ." belas";
        elseif ($value < 100)
            return number_to_spelling_bahasa($value / 10) ." puluh" . number_to_spelling_bahasa($value % 10);
        elseif ($value < 200)
            return " seratus" . number_to_spelling_bahasa($value - 100);
        elseif ($value < 1000)
            return number_to_spelling_bahasa($value / 100) ." ratus" . number_to_spelling_bahasa($value % 100);
        elseif ($value < 2000)
            return " seribu" . number_to_spelling_bahasa($value - 1000);
        elseif ($value < 1000000)
            return number_to_spelling_bahasa($value / 1000) ." ribu" . number_to_spelling_bahasa($value % 1000);
        elseif ($value < 1000000000)
            return number_to_spelling_bahasa($value / 1000000) ." juta" . number_to_spelling_bahasa($value % 1000000);
        elseif ($value >= 1000000000)
            return number_to_spelling_bahasa($value / 1000000000) ." miliar" . number_to_spelling_bahasa($value % 1000000000);
        return null;
	}
}

if ( ! function_exists('generate_uniq_id'))
{
    function generate_uniq_id()
    {
        try {
            $uniq_id = strtoupper(uniqid() . random_int(100, 999));
        } catch (Exception $e) {
            $uniq_id = strtoupper(uniqid() . '000');
        }
        return $uniq_id;
    }
}


/* End of file MY_string_helper.php */
/* Location: ./application/helpers/MY_string_helper.php */