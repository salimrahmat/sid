<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('url_get_return'))
{
    /**
     * @param string|null $defaultUrl
     * @return string|null
     */
    function url_get_return($defaultUrl = NULL)
	{
        /** @var MY_Controller $CI */
        $CI =& get_instance();

        /** @var string|null $returnUrl */
        $returnUrl = $CI->input->get_post('return_url');

        if ($returnUrl === NULL || $returnUrl === '')
            $returnUrl = $defaultUrl !== NULL ? $defaultUrl : uri_string();

        return $returnUrl;
	}
}

if ( ! function_exists('url_return_query'))
{
    /**
     * @param string|null $defaultUrl
     * @return string
     */
    function url_return_query($defaultUrl = NULL)
    {
        return http_build_query([
            'return_url' => url_get_return($defaultUrl)]);
    }
}

if ( ! function_exists('url_get_create_return_query'))
{
    /**
     * @return string
     */
    function url_get_create_return_query()
    {
        /** @var MY_Controller $CI */
        $CI =& get_instance();

        /** @var array $httpQueryInput */
        $httpQueryInput = $CI->input->get(NULL, TRUE);
        $returnUrl = '/' . uri_string() .'?'. http_build_query($httpQueryInput);

        return $returnUrl;
    }
}

if ( ! function_exists('url_create_return_query'))
{
    /**
     * @return string
     */
    function url_create_return_query()
    {
        return http_build_query([
            'return_url' => url_get_create_return_query()]);
    }
}

if ( ! function_exists('url_set_queries'))
{
    /**
     * @param string $url
     * @param array $queries
     * @return string
     */
    function url_set_queries($url, $queries)
    {
        $parsedUrl = parse_url($url);

        $buildQueries = [];
        if (array_key_exists('query', $parsedUrl))
            parse_str($parsedUrl['query'], $buildQueries);

        foreach ($queries as $key => $value)
            $buildQueries[$key] = $value;

        if (count($buildQueries) > 0)
            $parsedUrl['query'] = http_build_query($buildQueries);

        return url_reverse_parse($parsedUrl);
    }
}

if ( ! function_exists('url_reverse_parse'))
{
    /**
     * @param array $parsedUrl
     * @return string
     */
    function url_reverse_parse($parsedUrl)
    {
        $scheme     = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $host       = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
        $port       = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
        $user       = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
        $pass       = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass']  : '';
        $pass       = ($user || $pass) ? "$pass@" : '';
        $path       = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
        $query      = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
        $fragment   = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }
}

/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */