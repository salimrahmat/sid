<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('datetime_convert'))
{
    /**
     * @param string $datetimeString
     * @param string|null $timezoneFrom
     * @param string|null $timezoneTo
     * @param string $format
     * @return string|null
     */
    function datetime_convert($datetimeString, $timezoneFrom = NULL, $timezoneTo = NULL, $format = "Y-m-d H:i:s")
    {
        if (empty($datetimeString))
            return NULL;

        if (empty($timezoneFrom))
            $timezoneFrom = config_item('time_reference');

        if (empty($timezoneTo))
            $timezoneTo = config_item('time_reference');

        $result = NULL;

        try {
            $datetime = new DateTime($datetimeString, new DateTimeZone($timezoneFrom));
            $datetime->setTimeZone(new DateTimeZone($timezoneTo));
            $result = $datetime->format($format);
        } catch (Exception $ex) {

        }

        return $result;
    }
}

if ( ! function_exists('datetime_to_utc'))
{
    /**
     * @param string $datetimeString
     * @param string $format
     * @return string|null
     */
    function datetime_to_utc($datetimeString, $format = "Y-m-d H:i:s")
    {
        if (empty($datetimeString))
            return NULL;

        $result = NULL;

        try {
            $datetime = new DateTime($datetimeString);
            $datetime->setTimeZone(new DateTimeZone('UTC'));
            $result = $datetime->format($format);
        } catch (Exception $ex) {

        }

        return $result;
    }
}

if ( ! function_exists('add_date'))
{
    /**
     * @param string $datetimeString
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int $hour
     * @param int $minute
     * @param int $second
     * @return false|string
     */
    function add_date($datetimeString, $day = 0, $month = 0, $year = 0, $hour = 0, $minute = 0, $second = 0)
    {
        $unixTime = strtotime($datetimeString);
        $newDatetimeString = date('Y-m-d H:i:s',
            mktime(
                date('H', $unixTime) + $hour,
                date('i', $unixTime) + $minute,
                date('s', $unixTime) + $second,
                date('m', $unixTime) + $month,
                date('d', $unixTime) + $day,
                date('Y', $unixTime) + $year
            )
        );

        return $newDatetimeString;
    }
}

if ( ! function_exists('date_to_spelling_bahasa'))
{
    /**
     * @param string $dateString
     * @return array
     */
    function date_to_spelling_bahasa($dateString)
	{
        /** @var CI_Controller $CI */
        $CI =& get_instance();
		$CI->load->helper('string');
		
		$unixTime = strtotime($dateString);
		$day = date('j', $unixTime);
		$dayNumber = date('N', $unixTime) - 1;
		$monthNumber = date('n', $unixTime) - 1;
		$year = date('Y', $unixTime);
		
		$dayNames = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];
		$monthNames = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		
		$spellDayNumber = $dayNames[$dayNumber];
		$spellDay = number_to_spelling_bahasa($day);
		$spellMonthNumber = $monthNames[$monthNumber];
		$spellYear = number_to_spelling_bahasa($year);
		
		return [
			  'day_number'	=> $spellDayNumber
			, 'day'			=> $spellDay
			, 'month'		=> $spellMonthNumber
			, 'year'		=> $spellYear];
	}
}

if ( ! function_exists('date_diff_by_day'))
{
    /**
     * @param string $fromDatetimeString
     * @param string $toDatetimeString
     * @return int
     */
    function date_diff_by_day($fromDatetimeString, $toDatetimeString)
	{
		$toUnixTime = strtotime($toDatetimeString);
		$fromUnixTime = strtotime($fromDatetimeString);
		$timeDiff = $toUnixTime - $fromUnixTime;
		return (int) floor($timeDiff / (60 * 60 * 24));
	}
}

if ( ! function_exists('date_reformat'))
{
    /**
     * @param string $dateStr
     * @param string $format
     * @param mixed|null $default
     * @return string|null|mixed
     */
    function date_reformat($dateStr, $format = 'd-m-Y', $default = NULL)
    {
        if ($dateStr !== NULL)
            return date($format, strtotime($dateStr));
        return $default;
    }
}