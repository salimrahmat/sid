<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_libraries extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function home()
    {
        $this->page = "index";
        $this->layout();
    }
}