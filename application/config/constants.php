<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


defined('SITE_NAME') OR define('SITE_NAME','Farm'); 
defined('REGISTRASI_LIST') OR define('REGISTRASI_LIST','SELECT a.REGISTER_NO,a.REGISTER_DATE,a.ROOM_NUMBER,a.WEIGHT_IN,b.DESCR AS TIPE_TERNAK,c.DESCR AS JENIS_TERNAK,d.DESCR AS TIPE_KANDANG,a.ID_FARM, a.IMAGE 
FROM TBL_FARM a 
     INNER JOIN GENERAL_SETTING b on a.TYPE_LIVESTOCK = b.MASTER_CODE
     INNER JOIN GENERAL_SETTING c on a.TYPE_FARM = c.MASTER_CODE
     INNER JOIN GENERAL_SETTING d on a.TYPE_CAGE = d.MASTER_CODE ');


defined('REGISTRASI_SEARCH') OR define('REGISTRASI_SEARCH','SELECT a.REGISTER_NO,a.REGISTER_DATE,a.ROOM_NUMBER,a.WEIGHT_IN,b.DESCR AS TIPE_TERNAK,c.DESCR AS JENIS_TERNAK,d.DESCR AS TIPE_KANDANG,a.ID_FARM, a.IMAGE 
FROM TBL_FARM a 
     INNER JOIN GENERAL_SETTING b on a.TYPE_LIVESTOCK = b.MASTER_CODE
     INNER JOIN GENERAL_SETTING c on a.TYPE_FARM = c.MASTER_CODE
     INNER JOIN GENERAL_SETTING d on a.TYPE_CAGE = d.MASTER_CODE
');


defined('PEMBERIAN_PAKAN_LIST') OR define('PEMBERIAN_PAKAN_LIST','SELECT a.ID_FOOD_FARM, b.DESCR AS SCEDULE_FOOD, c.DESCR as TYPE_FOOD, d.DESCR AS TYPE_CAGE, a.FOOD_DATE, a.TOTAL_WEIGHT, a.TOTAL_FOOD,a.CREATE_DATE, a.UPDATE_DATE, a.USR_CRT, a.USR_UPD
FROM TBL_FOOD_FARM a 
     INNER JOIN GENERAL_SETTING b on a.SCEDULE_FOOD = b.MASTER_CODE
     INNER JOIN GENERAL_SETTING c on a.TYPE_FOOD = c.MASTER_CODE
     INNER JOIN GENERAL_SETTING d on a.TYPE_CAGE = d.MASTER_CODE
ORDER BY a.ID_FOOD_FARM DESC ');


defined('PEMBERIAN_PAKAN_SEACRH') OR define('PEMBERIAN_PAKAN_SEACRH','SELECT a.ID_FOOD_FARM, b.DESCR AS SCEDULE_FOOD, c.DESCR as TYPE_FOOD, d.DESCR AS TYPE_CAGE, a.FOOD_DATE, a.TOTAL_WEIGHT, a.TOTAL_FOOD,a.CREATE_DATE, a.UPDATE_DATE, a.USR_CRT, a.USR_UPD
FROM TBL_FOOD_FARM a 
     INNER JOIN GENERAL_SETTING b on a.SCEDULE_FOOD = b.MASTER_CODE
     INNER JOIN GENERAL_SETTING c on a.TYPE_FOOD = c.MASTER_CODE
     INNER JOIN GENERAL_SETTING d on a.TYPE_CAGE = d.MASTER_CODE
WHERE ');
 
defined('PENJUALAN_LIST_JUAL') OR define('PENJUALAN_LIST_JUAL','SELECT A.ORDER_NO, A.CONFIRM_SALE, A.OUT_DATE, A.INVOICE_NO, B.TOTAL_NON_JOGROG, B.TOTAL_JOGROG 
FROM TBL_SALE_HEADER A
     INNER JOIN 
      (SELECT * FROM (
          SELECT ORDER_NO,SUM(CASE WHEN TYPE_SALE NOT IN ("JG") THEN WEIGHT_CUSTOMER * TOTAL_NON_JOGROG ELSE 0 END) AS TOTAL_NON_JOGROG,
                 SUM(TOTAL_JOGROG) AS TOTAL_JOGROG  
          FROM TBL_SALE_FARM
          GROUP BY ORDER_NO
      )AS A) B ON A.ORDER_NO = B.ORDER_NO');

defined('DAILY_WEIGHT_LIST') OR define('DAILY_WEIGHT_LIST','SELECT a.ID_DAILY_WEIGHT, a.REGISTER_NO, a.WEIGHT_DATE, a.WEIGHT, a.ADD_VITAMIN, a.DTM_CRT, a.DTM_UPD, a.USR_CRT, a.USR_UPD, b.WEIGHT_IN FROM TBL_DAILY_WEIGHT a INNER JOIN TBL_FARM b on a.REGISTER_NO = b.REGISTER_NO ');
defined('DAILY_WEIGHT_SEARCH') OR define('DAILY_WEIGHT_SEARCH','SELECT a.ID_DAILY_WEIGHT, a.REGISTER_NO, a.WEIGHT_DATE, a.WEIGHT, a.ADD_VITAMIN, a.DTM_CRT, a.DTM_UPD, a.USR_CRT, a.USR_UPD, b.WEIGHT_IN FROM TBL_DAILY_WEIGHT a INNER JOIN TBL_FARM b on a.REGISTER_NO = b.REGISTER_NO ');


defined('ORDER_PAKAN_LIST') OR define('ORDER_PAKAN_LIST','SELECT a.ID_STOCK_FOOD, a.TYPE_FOOD, a.ORDER_DATE, a.ORDER_TOTAL, a.ARRIVAL_DATE, a.CREATE_DATE, a.UPDATE_DATE, a.USR_CRT, a.USR_UPD, b.DESCR, a.PRICE_FOOD, a.PRICE_KG, a.TOTAL, a.BIAYA_LAIN, c.VENDOR_NAME ,e.DESCR as STATUS, d.ACTUAL_DATE 
                                                            FROM TBL_ORDER_FOOD a 
                                                                INNER JOIN GENERAL_SETTING b on a.TYPE_FOOD = b.MASTER_CODE 
                                                                INNER JOIN TBL_VENDOR c ON c.ID_VENDOR = a.ID_VENDOR 
                                                                INNER JOIN TBL_ORDER_HEADER_FOOD d ON d.ORDER_NO = a.ORDER_NO
                                                                INNER JOIN GENERAL_SETTING e ON e.MASTER_CODE = d.STATUS 
                                                            ORDER BY a.ID_STOCK_FOOD DESC');
defined('ORDER_PAKAN_SEARCH') OR define('ORDER_PAKAN_SEARCH','SELECT a.ID_STOCK_FOOD, a.TYPE_FOOD, a.ORDER_DATE, a.ORDER_TOTAL, a.ARRIVAL_DATE, a.CREATE_DATE, a.UPDATE_DATE, a.USR_CRT, a.USR_UPD, b.DESCR, a.PRICE_FOOD, a.PRICE_KG, a.TOTAL, a.BIAYA_LAIN, c.VENDOR_NAME,e.DESCR as STATUS, d.ACTUAL_DATE 
                                                                FROM TBL_ORDER_FOOD a 
                                                                    INNER JOIN GENERAL_SETTING b on a.TYPE_FOOD = b.MASTER_CODE 
                                                                    INNER JOIN TBL_VENDOR c ON c.ID_VENDOR = a.ID_VENDOR 
                                                                    INNER JOIN TBL_ORDER_HEADER_FOOD d ON d.ORDER_NO = a.ORDER_NO
                                                                    INNER JOIN GENERAL_SETTING e ON e.MASTER_CODE = d.STATUS WHERE');

defined('ORDER_PAKAN_PO_LIST') OR define('ORDER_PAKAN_PO_LIST','SELECT D.ORDER_NO,A.ID_VENDOR , B.VENDOR_NAME, A.ORDER_DATE, SUM(ORDER_TOTAL) AS TOTAL_ORDER ,
                                                                    SUM(PRICE_FOOD) AS TOTAL_HARGA_KG, SUM(A.TOTAL) AS TOTAL , SUM(BIAYA_LAIN) AS BIAYA_LAIN, E.DESCR AS STATUS, D.ACTUAL_DATE 
                                                                    FROM TBL_ORDER_FOOD A INNER JOIN TBL_VENDOR B ON A.ID_VENDOR = B.ID_VENDOR
                                                                    INNER JOIN TBL_ORDER_HEADER_FOOD D ON D.ORDER_NO = A.ORDER_NO
                                                                    INNER JOIN GENERAL_SETTING E ON E.MASTER_CODE = D.STATUS 
                                                                    WHERE A.STATUS IN ("PPO","ODR")
                                                                    GROUP BY A.ID_VENDOR , B.VENDOR_NAME, A.ORDER_DATE, E.DESCR, D.ORDER_NO, D.ACTUAL_DATE');

defined('SALE_FARM_LIST') OR define('SALE_FARM_LIST','SELECT a.REGISTER_NO,a.OUTSIDE_DATE as tanggal_keluar,a.WEIGHT_CAGE,a.WEIGHT_CUSTOMER,c.DESCR as jenis_jual, d.DESCR as type_ternak,e.DESCR as jenis_ternak, f.DESCR as tipe_kandang, a.CREATE_DATE as proses_jual 
FROM TBL_SALE_FARM a 
INNER JOIN TBL_FARM b on a.REGISTER_NO = b.REGISTER_NO 
INNER JOIN GENERAL_SETTING c on c.MASTER_CODE = a.TYPE_SALE
INNER JOIN GENERAL_SETTING d on d.MASTER_CODE = b.TYPE_LIVESTOCK
INNER JOIN GENERAL_SETTING e on e.MASTER_CODE = b.TYPE_FARM
INNER JOIN GENERAL_SETTING f on f.MASTER_CODE = b.TYPE_CAGE');

defined('SETTING_LIST') OR define('SETTING_LIST','SELECT B.GENERAL_SETTING_ID, A.DESCR AS TIPE,B.MASTER_CODE AS CODE, B.DESCR AS DETAIL_TIPE, CASE WHEN B.IS_ACTIVE =1 THEN "Active" ELSE "Non Active" END AS STATUS, B.IS_ACTIVE FROM GENERAL_SETTING_TYPE A INNER JOIN GENERAL_SETTING B ON A.GENERAL_SETTING_CODE = B.GENERAL_SETTING_CODE WHERE B.IS_ACTIVE=1');

defined('VENDOR_LIST') OR define('VENDOR_LIST','SELECT ID_VENDOR,VENDOR_NAME,VENDOR_ADDRESS,VENDOR_PHONE,VENDOR_PIC,IS_ACTIVE,CASE WHEN IS_ACTIVE = 1 THEN "Active" ELSE "InActive" END AS STATUS FROM TBL_VENDOR');

defined('MEMBER_LIST') OR define('MEMBER_LIST','SELECT MEMBER_ID,MEMBER_NAME,MEMBER_EMAIL,MEMBER_ALAMAT,MEMBER_HP FROM TBL_MEMBER');

defined('PENJUALAN_LIST') OR define('PENJUALAN_LIST','SELECT A.REGISTER_NO,C.DESCR AS TYPE_TERNAK,B.DESCR AS JENIS_TERNAK,A.WEIGHT_IN
FROM TBL_FARM A
INNER JOIN GENERAL_SETTING B ON A.TYPE_FARM = B.MASTER_CODE 
INNER JOIN GENERAL_SETTING C ON A.TYPE_LIVESTOCK = C.MASTER_CODE');

defined('USER_LIST') OR define('USER_LIST','
SELECT A.*, B.DESCR 
 FROM TBL_USER A
	  INNER JOIN GENERAL_SETTING B ON A.USER_ACCESS = B.MASTER_CODE');

defined('USER_ACCESS_LIST') OR define('USER_ACCESS_LIST','SELECT * FROM TBL_ACCESS_MENU A INNER JOIN GENERAL_SETTING B ON A.ID_USER_ACCESS = B.MASTER_CODE');

defined('ORDER_LIST_CANCEL') OR define('ORDER_LIST_CANCEL','SELECT A.ORDER_NO,C.MEMBER_NAME,DATE_FORMAT(A.OUT_DATE, "%d-%m-%Y") AS OUT_DATE,SUM(TOTAL_JOGROG) AS TOTAL_JOGROG, SUM(TOTAL_NON_JOGROG) AS TOTAL_NON_JOGROG, SUM(IFNULL(DISCOUNT, 0)) AS DISCOUNT  
FROM TBL_SALE_HEADER A 
  INNER JOIN TBL_SALE_FARM B 
        ON A.ORDER_NO = B.ORDER_NO	
  INNER JOIN TBL_MEMBER C 
        ON C.MEMBER_ID = A.MEMBER_ID	
WHERE A.CONFIRM_SALE="C"
GROUP BY OUT_DATE,A.OUT_DATE,A.ORDER_NO');

defined('ORDER_FARM_LIST') OR define('ORDER_FARM_LIST','SELECT tohf.ORDER_NO, tv.VENDOR_NAME, tohf.ORDER_DATE, tohf.ACTUAL_DATE, tof.ID_FARM_DETAIL,
gs.DESCR AS NAME_TYPE_FARM, gs2.DESCR AS NAME_JENIS_FARM, tof.QTY, tof.PRICE_KG, tof.TOTAL, tof.BIAYA_LAIN,
gs3.DESCR AS STATUS, tohf.ESTIMATION_DATE, tof.ID_FARM_DETAIL 
FROM TBL_ORDER_HEADER_FARM tohf 
  INNER JOIN TBL_VENDOR tv ON tohf.ID_VENDOR = tv.ID_VENDOR 
  INNER JOIN TBL_ORDER_FARM tof ON tof.ORDER_NO = tohf.ORDER_NO 
  INNER JOIN GENERAL_SETTING gs ON gs.MASTER_CODE = tof.TYPE_FARM 
  INNER JOIN GENERAL_SETTING gs2 ON gs2.MASTER_CODE = tof.JENIS_FARM 
  INNER JOIN GENERAL_SETTING gs3 ON gs3.MASTER_CODE = tohf.STATUS');

defined('ORDER_LIST_YES') OR define('ORDER_LIST_YES','SELECT A.ORDER_NO,C.MEMBER_NAME,DATE_FORMAT(A.OUT_DATE, "%d-%m-%Y") AS OUT_DATE,SUM(TOTAL_JOGROG) AS TOTAL_JOGROG, SUM(TOTAL_NON_JOGROG) AS TOTAL_NON_JOGROG, SUM(IFNULL(DISCOUNT, 0)) AS DISCOUNT  
FROM TBL_SALE_HEADER A 
  INNER JOIN TBL_SALE_FARM B 
        ON A.ORDER_NO = B.ORDER_NO	
  INNER JOIN TBL_MEMBER C 
        ON C.MEMBER_ID = A.MEMBER_ID	
WHERE A.CONFIRM_SALE="Y"
GROUP BY OUT_DATE,A.OUT_DATE,A.ORDER_NO');

defined('PROGRESS_FARM') OR define('PROGRESS_FARM','SELECT b.DESCR AS tipe_kandang, a.ROOM_NUMBER as nomor_kamar, a.REGISTER_NO as no_registrasi,
a.REGISTER_DATE as tanggal_register, a.WEIGHT_IN as berat_masuk, c.WEIGHT_DATE as tanggal_timbang, 
c.WEIGHT as berat_timbang,c.WEIGHT - a.WEIGHT_IN as total_naik,DATEDIFF(CURDATE() , a.REGISTER_DATE) as total_hari,CURDATE()
FROM TBL_FARM a 
INNER JOIN GENERAL_SETTING b on a.TYPE_CAGE = b.MASTER_CODE
INNER JOIN TBL_DAILY_WEIGHT c on a.REGISTER_NO = c.REGISTER_NO');

defined('CHECK_TERNAK') OR define('CHECK_TERNAK', 'SELECT A.REGISTER_NO, C.DESCR AS TIPE_KANDANG, A.ROOM_NUMBER AS NOMOR_KAMAR, B.WEIGHT AS BERAT_BADAN, B.WEIGHT_DATE as TANGGAL_TIMBANG 
  FROM TBL_FARM A  
  INNER JOIN TBL_DAILY_WEIGHT B on A.REGISTER_NO = B.REGISTER_NO
  INNER JOIN GENERAL_SETTING C on A.TYPE_CAGE = C.MASTER_CODE');

defined('STOCK_FOOD') OR define('STOCK_FOOD','SELECT b.DESCR ,SUM(ORDER_TOTAL) AS STOCK,IFNULL(SUM(TOTAL_FOOD), 0) AS PAKAN_KELUAR,d.DESCR AS JADWAL_PAKAN, SUM(ORDER_TOTAL) - IFNULL(SUM(TOTAL_FOOD), 0) AS STOCK_TERSEDIA 
FROM TBL_ORDER_FOOD a 
INNER JOIN GENERAL_SETTING b on a.TYPE_FOOD = b.MASTER_CODE 
LEFT JOIN TBL_FOOD_FARM c on a.TYPE_FOOD = c.TYPE_FOOD 
LEFT JOIN GENERAL_SETTING d on c.SCEDULE_FOOD = d.MASTER_CODE
INNER JOIN TBL_ORDER_HEADER_FOOD e ON e.ORDER_NO = a.ORDER_NO 
WHERE e.STATUS="RCV"
GROUP BY b.DESCR , d.DESCR');

defined('ORDER_FARM_LIST_PO') OR define('ORDER_FARM_LIST_PO','SELECT tv.VENDOR_NAME, tohf.ORDER_DATE, tohf.ESTIMATION_DATE,
SUM(tof.QTY) AS QTY , SUM(tof.PRICE_KG) AS PRICE_KG ,
SUM(tof.TOTAL) AS TOTAL , SUM(tof.BIAYA_LAIN) AS BIAYA_LAIN,
tof.ORDER_NO, gs.DESCR AS STATUS,tv.ID_VENDOR 
FROM TBL_ORDER_HEADER_FARM tohf 
INNER JOIN TBL_VENDOR tv ON tohf.ID_VENDOR  = tv.ID_VENDOR 
INNER JOIN TBL_ORDER_FARM tof ON tof.ORDER_NO = tohf.ORDER_NO 
INNER JOIN GENERAL_SETTING gs ON gs.MASTER_CODE = tohf.STATUS 
GROUP BY tv.VENDOR_NAME, tohf.ORDER_DATE, tohf.ESTIMATION_DATE,tof.ORDER_NO,gs.DESCR');

defined('URL_REGISTRASI') OR define('URL_REGISTRASI','registrasi/lists');    
defined('URL_PENIMBANGAN') OR define('URL_PENIMBANGAN','penimbangan/lists');   
defined('URL_PEMBERIAN_PAKAN') OR define('URL_PEMBERIAN_PAKAN','pemberian_pakan/lists');    
defined('URL_ORDER_PAKAN') OR define('URL_ORDER_PAKAN','order_pakan/lists');
defined('URL_SALE_FARM') OR define('URL_SALE_FARM','penjualan_domba/lists');
defined('URL_PROGRESS_TERNAK') OR define('URL_PROGRESS_TERNAK','review/progres_ternak');
defined('URL_CHECK_TERNAL_LIST') OR define ('URL_CHECK_TERNAL_LIST','review/check_ternak/lists');
defined('URL_SETTING_LIST') OR define ('URL_SETTING_LIST','general_setting/lists');
defined('URL_PAKAN_PO_LIST') OR define ('URL_PAKAN_PO_LIST','order_pakan/listPo');
defined('URL_MEMBER_LIST') OR define ('URL_MEMBER_LIST','member/lists');
defined('URL_PENJUALAN_LIST') OR define ('URL_PENJUALAN_LIST','penjualan_domba/lists');
defined('URL_PENJUALAN_LIST_JUAL') OR define ('URL_PENJUALAN_LIST_JUAL','penjualan_domba/lists_jual');
defined('URL_USER_LIST') OR define ('URL_USER_LIST','user/lists');
defined('URL_PRIVILAGE') OR define('URL_PRIVILAGE','privilege/lists');
defined('URL_ORDER_CANCEL') OR define('URL_ORDER_CANCEL','home/order_cancel');
defined('URL_ORDER_YES') OR define('URL_ORDER_YES','home/order_yes');
defined('URL_ORDER_FARM') OR define('URL_ORDER_FARM', 'registrasi/lists_order');
defined('URL_ORDER_FARM_PO') OR define('URL_ORDER_FARM_PO', 'registrasi/lists_po');


defined('TYPE_LOG_GANTI_KANDANG')  OR define('TYPE_LOG_GANTI_KANDANG', 10);
defined('TYPE_LOG_DAILY_TIMBANG')  OR define('TYPE_LOG_DAILY_TIMBANG', 20);
defined('TYPE_LOGS')            OR define('TYPE_LOGS',              [
     TYPE_LOG_GANTI_KANDANG     => [
        'id'    => TYPE_LOG_GANTI_KANDANG,
        'name'  => "Ganti Kandang",
    ],
    TYPE_LOG_DAILY_TIMBANG => [
        'id'    => TYPE_LOG_DAILY_TIMBANG,
        'name'  => "Belum Dimulai",
    ],
]);

defined('STATUS_ACTIVE')  OR define('STATUS_ACTIVE', 80);
defined('STATUS_INACTIVE')  OR define('STATUS_INACTIVE', 90);
defined('STATUS_SOLD')  OR define('STATUS_SOLD', 100);
defined('STATUS') OR define('STATUS',              [
    STATUS_ACTIVE     => [
        'id'    => STATUS_ACTIVE,
        'name'  => "Ganti Kandang",
    ],
    STATUS_INACTIVE => [
        'id'    => STATUS_INACTIVE,
        'name'  => "Belum Dimulai",
    ],
    STATUS_SOLD => [
        'id'    => STATUS_SOLD,
        'name'  => "Belum Dimulai",
    ],
]);

defined('SEARCH_PENJUALAN')  OR define('SEARCH_PENJUALAN', 'Katalog Penjualan Hewan Ternak');
defined('SEARCH_CATEGORY') OR define('SEARCH_CATEGORY','SELECT MASTER_CODE FROM TBL_KATEGORI');
defined('CHART_CATEGORY') OR define('CHAT_CATEGORY','SELECT COUNT(A.REGISTER_NO) AS TOTAL,C.DESCR AS CATEGORY
FROM TBL_FARM A 
INNER JOIN TBL_DAILY_WEIGHT B on A.REGISTER_NO = B.REGISTER_NO
INNER JOIN GENERAL_SETTING C on C.MASTER_CODE = B.TYPE_CATEGORY');

defined('CHART_TIPE_TERNAK') OR define('CHART_TIPE_TERNAK','SELECT COUNT(A.REGISTER_NO) AS TOTAL,B.DESCR AS CATEGORY
FROM TBL_FARM A 
INNER JOIN GENERAL_SETTING B on B.MASTER_CODE = A.TYPE_LIVESTOCK');

defined('CHART_JENIS_TERNAK') OR define('CHART_JENIS_TERNAK','SELECT COUNT(A.REGISTER_NO) AS TOTAL,B.DESCR AS CATEGORY
FROM TBL_FARM A 
INNER JOIN GENERAL_SETTING B on B.MASTER_CODE = A.TYPE_FARM');

defined('CHART_TIPE_KANDANG') OR define('CHART_TIPE_KANDANG','SELECT COUNT(A.REGISTER_NO) AS TOTAL,B.DESCR AS CATEGORY
FROM TBL_FARM A 
INNER JOIN GENERAL_SETTING B on B.MASTER_CODE = A.TYPE_CAGE');

defined('NOMOR_KANDANG') OR define('NOMOR_KANDANG','SELECT COUNT(A.REGISTER_NO) AS TOTAL,A.ROOM_NUMBER AS CATEGORY
FROM TBL_FARM A ');

defined('CHART_PENIMBANGAN')  OR define('CHART_PENIMBANGAN', 1);
defined('CHART_PEMBERIAN')  OR define('CHART_PEMBERIAN', 2);
defined('CHART_PENJUALAN')  OR define('CHART_PENJUALAN', 3);
defined('CHART_BARS') OR define('CHART_BARS',              [
    CHART_PENIMBANGAN     => [
        'id'    => CHART_PENIMBANGAN,
        'name'  => "Chart Penimbangan",
    ],
    CHART_PEMBERIAN => [
        'id'    => CHART_PEMBERIAN,
        'name'  => "Chart Pemberian",
    ],
    CHART_PENJUALAN => [
        'id'    => CHART_PENJUALAN,
        'name'  => "Chart Penjualan",
    ],
]);

defined('TYPE_RUN_ORDER')  OR define('TYPE_RUN_ORDER', 'FM');
defined('TYPE_INVOICE')  OR define('TYPE_INVOICE', 'INV');
defined('PO_NUMBER')  OR define('PO_NUMBER', 'PON');
/**
 * Privilages Access User
 */

defined('PRIVILEGE_REGISTRASI') OR define('PRIVILEGE_REGISTRASI', 'Registrasi');
defined('PRIVILEGE_ORDER_DOMBA') OR define('PRIVILEGE_ORDER_DOMBA', 'Order Domba');
defined('PRIVILEGE_PENIMBANGAN') OR define('PRIVILEGE_PENIMBANGAN', 'Penimbangan harian');
defined('PRIVILEGE_PEMBERIAN_PAKAN') OR define('PRIVILEGE_PEMBERIAN_PAKAN', 'pemberian pakan');
defined('PRIVILEGE_PENJUALAN') OR define('PRIVILEGE_PENJUALAN', 'penjualan');
defined('PRIVILEGE_LIST_PENJUALAN') OR define('PRIVILEGE_LIST_PENJUALAN', 'list penjualan');
defined('PRIVILEGE_STOK_PAKAN') OR define('PRIVILEGE_STOK_PAKAN', 'stock pakan');
defined('PRIVILEGE_ORDER_PAKAN') OR define('PRIVILEGE_ORDER_PAKAN', 'order pakan');
defined('PRIVILEGE_REVIEW') OR define('PRIVILEGE_REVIEW', 'review check ternak');
defined('PRIVILEGE_SETTING') OR define('PRIVILEGE_SETTING', 'setting');
defined('PRIVILEGE_VENDOR') OR define('PRIVILEGE_VENDOR', 'vendor');
defined('PRIVILEGE_MEMBER') OR define('PRIVILEGE_MEMBER', 'member');
defined('PRIVILEGE_STOCK_TERNAK') OR define('PRIVILEGE_STOCK_TERNAK', 'Stock Ternak');
defined('PRIVILEGE_CHART_TERNAK') OR define('PRIVILEGE_CHART_TERNAK', 'Chart Ternak');
defined('PRIVILEGE_CHECK_TERNAK') OR define('PRIVILEGE_CHECK_TERNAK', 'Check Ternak');
defined('PRIVILEGE_PROGRES_TERNAK') OR define('PRIVILEGE_PROGRES_TERNAK', 'Progres Ternak');
defined('PRIVILEGE_USER') OR define('PRIVILEGE_USER', 'User');
defined('PRIVILEGE') OR define('PRIVILEGE', 'Privilage');
defined('PRIVILEGE_CREATE') OR define('PRIVILEGE_CREATE', 'menu_add');
defined('PRIVILEGE_UPDATE') OR define('PRIVILEGE_UPDATE', 'menu_edit');
defined('PRIVILEGE_DELETE') OR define('PRIVILEGE_DELETE', 'menu_delete');
defined('PRIVILEGE_LIST')   OR define('PRIVILEGE_LIST', 'menu_list');
defined('PRIVILEGE_DETAIL') OR define('PRIVILEGE_DETAIL', 'menu_detail');
