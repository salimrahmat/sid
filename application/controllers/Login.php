<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
    }

	public function index()
	{
		$data['error'] = [
			'email'     => NULL ,
			'password'  => NULL ,
			'value'     => NULL
		];
		
		$this->load->database();
		$this->load->helper('url');
		$this->load->view('login',$data);
	}
}
