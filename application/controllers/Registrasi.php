<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Registrasi extends MY_Controller {
    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }


        /**
     * @throws APIErrorException
     */
    public function lists() {
        $data=[
            'registrasi' => NULL,
            'jenis_ternak' => NULL,
            'tipe_kandang' => NULL, 
            'tipe_ternak'  => NULL, 
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_LIVESTOCK');
        $query2 = $this->db->get();

        $records = [];
        foreach ($query2->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_ternak'] = $records;
        $query=REGISTRASI_LIST.' WHERE a.STATUS ='.STATUS_ACTIVE.' ORDER BY a.ID_FARM DESC';
        $data['registrasi'] = $this->get_paging($query,URL_REGISTRASI);
        $this->data['data'] = $data;
        $this->set_breadcrump('Registrasi' , 'List Data');
        $this->page = "registrasi/index";
        $this->layout();
    }

    /**
     * Form Registrasi
     * @param integer
     */
    public function form($id = NULL){
        $data = [
             'jenis_ternak' => NULL,
             'tipe_kandang' => NULL, 
             'tipe_ternak'  => NULL, 
             'flow_data'        => [
                'id_farm'       => NULL, 
                'register_date' => NULL,
                'tipe_ternak'   => NULL,
                'jenis_ternak'  => NULL,
                'weight'        => NULL,
                'tipe_kandang'  => NULL,
                'noroom'        => NULL,
                'noregis'       => NULL,
                'exis_kandang'  => NULL,
                'value'         => NULL,
             ]
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_LIVESTOCK');
        $query2 = $this->db->get();

        $records = [];
        foreach ($query2->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_ternak'] = $records;
        if ($id === NULL) {
            $value=NULL;
            if($this->getError()!== NULL){
                $value=$this->getError();
            } else {
                $value = $this->form_validation->error_array();
            };


            $data['flow_data'] = [
                'id_farm'       => NULL,
                'register_date' => $this->input->post('register_date'),
                'tipe_ternak'   => $this->input->post('tipe_ternak'),
                'jenis_ternak'  => $this->input->post('jenis_ternak'),
                'weight'        => $this->input->post('weight'),
                'tipe_kandang'  => $this->input->post('tipe_kandang'),
                'noroom'        => $this->input->post('noroom'),
                'noregis'       => $this->input->post('noregis'),
                'exis_kandang'  => NULL,
                'value'         => $value,
            ];   
            
            $this->data['data'] = $data;
        } else {
            $this->db->select('ID_FARM, REGISTER_NO, REGISTER_DATE, TYPE_FARM, WEIGHT_IN, TYPE_CAGE, TYPE_LIVESTOCK, ROOM_NUMBER, CREATE_DATE, UPDATE_DATE, USR_CRT, USR_UPD');
            $this->db->from('TBL_FARM');
            $this->db->where('ID_FARM', $id);
            $query = $this->db->get();

            $this->db->select('B.DESCR, A.ID_FARM, A.REGISTER_NO, A.REGISTER_DATE, A.TYPE_FARM, A.WEIGHT_IN, A.TYPE_CAGE, A.TYPE_LIVESTOCK, A.ROOM_NUMBER, A.CREATE_DATE, A.UPDATE_DATE, A.USR_CRT, A.USR_UPD');
            $this->db->from('TBL_FARM A');
            $this->db->join('GENERAL_SETTING B', 'B.MASTER_CODE = A.TYPE_CAGE' ,'INNER');
            $this->db->where('A.ID_FARM', $id);
            $query1 = $this->db->get();

            $data['flow_data'] = [
                'id_farm'       => $query->row('ID_FARM'),
                'register_date' => date("d/m/Y", strtotime($query->row('REGISTER_DATE'))),
                'tipe_ternak'   => $query->row('TYPE_LIVESTOCK'),
                'jenis_ternak'  => $query->row('TYPE_FARM'),
                'weight'        => $query->row('WEIGHT_IN'),
                'tipe_kandang'  => $query->row('TYPE_CAGE'),
                'noroom'        => $query->row('ROOM_NUMBER'),
                'noregis'       => $query->row('REGISTER_NO'),
                'exis_kandang'  => $query1->row('DESCR'),
                'value'         => $this->form_validation->error_array(),
            ];   
            
            $this->data['data'] = $data;
        }
        $this->set_breadcrump('Registrasi' , 'Input data');
        $this->page = "registrasi/form";
        $this->layout();
    }

    public function save() {
        $this->form_validation
        ->set_rules('register_date', "Tanggal Registrasi", 'trim|required')
        ->set_rules('tipe_ternak', "Tipe Ternak", 'trim|required')
        ->set_rules('jenis_ternak', "Jenis Ternak", 'trim|required')
        ->set_rules('weight', "Berat Masuk", ['trim','decimal','required'])
        ->set_rules('tipe_kandang', "Tipe Kandang", 'trim|required')
        ->set_rules('noroom', "Nomor Kamar", ['trim','required', 'max_length[5]'])
        ->set_rules('noregis', "No Registrasi", ['trim','required', 'max_length[11]']);


        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        try {
            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = './assets/'; //string, the default is application/cache/
            $config['errorlog']     = './assets/'; //string, the default is application/logs/
            $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224,255,255); // array, default is array(255,255,255)
            $config['white']        = array(70,130,180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);
     
            $image_name=$this->input->post('noregis').'.png'; //buat name dari qr code sesuai dengan nim
     
            $params['data'] = $this->input->post('noregis'); //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            // var_dump(date("Y-m-d", );die();

            $data = array(
                'REGISTER_NO'   => $this->input->post('noregis'),
                'REGISTER_DATE' => date("Y-m-d", strtotime($this->input->post('register_date'))),
                'TYPE_FARM'     => $this->input->post('jenis_ternak'),
                'WEIGHT_IN'     => $this->input->post('weight'),
                'TYPE_CAGE'     => $this->input->post('tipe_kandang'),
                'TYPE_LIVESTOCK' => $this->input->post('tipe_ternak'),
                'ROOM_NUMBER'    => $this->input->post('noroom'),
                'STATUS'         => STATUS_ACTIVE,   
                'CREATE_DATE'    => date('Y-m-d H:i:s'),
                'USR_CRT'        => $this->session->userdata('iduser'),
                'IMAGE'          => $image_name,
            );

            if ($this->checkNoreg($this->input->post('noregis'))) {
                $this->setError($this->input->post('noregis')." Already save on system");
                throw new Exception("Duplicate input noregistrasi");
            }
            
            if($this->validateDate($this->input->post('register_date'))){
                $this->setError(" Date format invalid please check again");
                throw new Exception("Date Format invalid");
            }

            $this->db->insert('TBL_FARM', $data);
            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
            $this->set_stock();
           
        } catch (Exception $e) {
            // log_message('error ',"asdasd");
            $this->form();
            return;
        }
        
        redirect('registrasi/lists');
    }

    public function set_noregis($type){
        $this->db->select('RUN_NUMBER');
        $this->db->from('TBL_SEQUENCE_NUMBER ');
        $this->db->where('MASTER_CODE', $type);
        $query = $this->db->get();
        $query->row('RUN_NUMBER');

        $year   = '%y';
        $month  = '%m';
        $time   = time();


        $config['id'] = $query->row('RUN_NUMBER');
        $config['awalan'] = $type.mdate($month, $time).mdate($year, $time);
        $config['digit'] = 5;
        $this->auto_number->config($config);
        $number = $this->auto_number->generate_id();
        
        $this->db->set('RUN_NUMBER', $query->row('RUN_NUMBER')+1);
        $this->db->where('MASTER_CODE', $type);
        $this->db->update('TBL_SEQUENCE_NUMBER');
        return $number;
    }

    public function update($id) {
        $this->form_validation
        ->set_rules('register_date', "Tanggal Registrasi", 'trim|required')
        ->set_rules('tipe_ternak', "Tipe Ternak", 'trim|required')
        ->set_rules('jenis_ternak', "Jenis Ternak", 'trim|required')
        ->set_rules('weight', "Berat Masuk", ['trim','decimal','required'])
        ->set_rules('tipe_kandang', "Tipe Kandang", 'trim|required')
        ->set_rules('noroom', "Nomor Kamar", ['trim','required', 'max_length[5]'])
        ->set_rules('noregis', "No Registrasi", ['trim','required', 'max_length[11]']);

        if ($this->form_validation->run() === FALSE)
        {
            $this->form($id);
            return;
        }  

        try {

            if($this->validateDate($this->input->post('register_date'))){
                $this->setError(" Date format invalid please check again");
                throw new Exception("Date Format invalid");
            }

            $data = array(
                // 'REGISTER_NO'   => $this->set_noregis($this->input->post('tipe_ternak')),
                'REGISTER_DATE' => date("Y-m-d", strtotime($this->input->post('register_date'))),
                'TYPE_FARM'     => $this->input->post('jenis_ternak'),
                'WEIGHT_IN'     => $this->input->post('weight'),
                'TYPE_CAGE'     => $this->input->post('tipe_kandang'),
                'TYPE_LIVESTOCK' => $this->input->post('tipe_ternak'),
                'ROOM_NUMBER'    => $this->input->post('noroom'),
                'UPDATE_DATE'    => date('Y-m-d H:i:s'),
                'USR_UPD'        => $this->session->userdata('iduser'),
            );

            $this->db->where('ID_FARM', $id);
            $this->db->update('TBL_FARM', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
            
            $LOG_FARM = array(
                'NO_REGISTRASI' => $this->input->post('noregis'),
                'TYPE'          => TYPE_LOG_GANTI_KANDANG,
                'FROM'          => $this->input->post('exis_kandang'),
                'TO'            => $this->input->post('tipe_kandang'),
                'USR_CRT'       => $this->session->userdata('iduser'),
                'DTM_CRT'       => date('Y-m-d H:i:s'),
            );
            $this->db->insert('TBL_LOG_FARM', $LOG_FARM);
            $this->set_stock();

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            // log_message('error ',$e->getMessage());
            $this->form($id);
            return;
        }
        redirect('registrasi/lists');
    }

    public function search(){
        $data=[
            'registrasi' => NULL,
            'jenis_ternak' => NULL,
            'tipe_kandang' => NULL, 
            'tipe_ternak'  => NULL, 
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_LIVESTOCK');
        $query2 = $this->db->get();

        $records = [];
        foreach ($query2->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_ternak'] = $records;

        $search  = ($this->input->post("table_search"))? $this->input->post("table_search") : NULL;
        $search1 = ($this->input->post("tipe_ternak"))? $this->input->post("tipe_ternak") : NULL;
        $search2 = ($this->input->post("jenis_ternak"))? $this->input->post("jenis_ternak") : NULL;
        $search3 = ($this->input->post("tipe_kandang"))? $this->input->post("tipe_kandang") : NULL;
        $query=REGISTRASI_SEARCH.'  WHERE a.STATUS='.STATUS_ACTIVE.' and ';
        if ($search !== NULL) {
            $data['registrasi'] = $this->get_paging($query." a.REGISTER_NO like '%".$search."%'",URL_REGISTRASI);            
        } else {
            $data['registrasi'] = $this->get_paging($query." a.REGISTER_NO like '% NULL %'",URL_REGISTRASI);            
        }

        if ($search1 !== NULL) {
            $data['registrasi'] = $this->get_paging($query." a.TYPE_LIVESTOCK like '%".$search1."%'",URL_REGISTRASI);            
        } else {
            $search1['registrasi'] = $this->get_paging($query." a.TYPE_LIVESTOCK like '% NULL %'",URL_REGISTRASI);            
        }

        if ($search2 !== NULL) {
            $data['registrasi'] = $this->get_paging($query." a.TYPE_FARM like '%".$search2."%'",URL_REGISTRASI);            
        } else {
            $search2['registrasi'] = $this->get_paging($query." a.TYPE_FARM like '% NULL %'",URL_REGISTRASI);            
        }

        if ($search3 !== NULL) {
            $data['registrasi'] = $this->get_paging($query." a.TYPE_CAGE like '%".$search3."%'",URL_REGISTRASI);            
        } else {
            $search3['registrasi'] = $this->get_paging($query." a.TYPE_CAGE like '% NULL %'",URL_REGISTRASI);            
        }

        $this->data['data'] = $data;
        $this->set_breadcrump('Registrasi' , 'List Data');
        $this->page = "registrasi/index";
        $this->layout();
    }

    public function cage($id){
        $data = [
            'tipe_kandang' => NULL,  
            'flow_data'        => [
               'id_farm'       => NULL, 
               'tipe_kandang'  => NULL,
               'exis_kandang'  => NULL,
               'value'         => NULL,
            ]
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang'] = $records;
        $this->db->select('B.DESCR, A.ID_FARM, A.REGISTER_NO, A.REGISTER_DATE, A.TYPE_FARM, A.WEIGHT_IN, A.TYPE_CAGE, A.TYPE_LIVESTOCK, A.ROOM_NUMBER, A.CREATE_DATE, A.UPDATE_DATE, A.USR_CRT, A.USR_UPD');
        $this->db->from('TBL_FARM A');
        $this->db->join('GENERAL_SETTING B', 'B.MASTER_CODE = A.TYPE_CAGE' ,'INNER');
        $this->db->where('A.ID_FARM', $id);
        $query = $this->db->get();

        $data['flow_data'] = [
            'id_farm'       => $query->row('ID_FARM'), 
            'tipe_kandang'  => $query->row('TYPE_CAGE'),
            'exis_kandang'  => $query->row('DESCR'),
            'no_registrasi' => $query->row('REGISTER_NO'),
            'value'         => $this->form_validation->error_array(),
        ];   
        
        $this->data['data'] = $data;
        $this->set_breadcrump('Registrasi' , 'Ganti Kandang');
        $this->page = "registrasi/cage";
        $this->layout();
    }

    public function updatecage($id){
        $this->form_validation
        ->set_rules('tipe_kandang', "Pilih Tipe Kandang", 'trim|required')
        ->set_rules('exis_kandang', "Kandang Sebelumnya", 'trim|required')
        ->set_rules('no_registrasi', "No Registrasi", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->cage($id);
            return;
        }  
        
        try {
                $data = array(
                    'TYPE_CAGE'     => $this->input->post('tipe_kandang'),
                    'UPDATE_DATE'    => date('Y-m-d H:i:s'),
                    'USR_UPD'        => $this->session->userdata('iduser'),
                );
    
                $this->db->where('ID_FARM', $id);
                $this->db->update('TBL_FARM', $data);
    
                $logs = array(
                    'USR_CRT' => $this->session->userdata('iduser'),
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'DESCR'   => json_encode($data), 
                );
                $this->db->insert('TBL_LOG', $logs);

                $LOG_FARM = array(
                    'NO_REGISTRASI' => $this->input->post('no_registrasi'),
                    'TYPE'          => TYPE_LOG_GANTI_KANDANG,
                    'FROM'          => $this->input->post('exis_kandang'),
                    'TO'            => $this->input->post('tipe_kandang'),
                    'USR_CRT'       => $this->session->userdata('iduser'),
                    'DTM_CRT'       => date('Y-m-d H:i:s'),
                );
                $this->db->insert('TBL_LOG_FARM', $LOG_FARM);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form($id);
        }
        redirect('registrasi/lists');
    }

    public function getLogKandang(){

        $this->db->select('a.NO_REGISTRASI, a.FROM, b.DESCR, a.DTM_CRT');
        $this->db->from('TBL_LOG_FARM a');
        $this->db->join('GENERAL_SETTING b','a.TO = b.MASTER_CODE' ,'INNER' );        
        $this->db->where('a.NO_REGISTRASI', $this->input->post('searchTerm'));
        $this->db->where('a.TYPE', TYPE_LOG_GANTI_KANDANG);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'noregistrasi' => $row->NO_REGISTRASI,
                'from'         => $row->FROM,
                'descr'        => $row->DESCR,
                'dtm_crt'      => date("d-m-Y H:i:s", strtotime($row->DTM_CRT)) 
            ];
                
        }

        echo json_encode($records);
    }

    public function export(){
        $query=REGISTRASI_LIST.' WHERE a.STATUS ='.STATUS_ACTIVE.' ORDER BY a.ID_FARM DESC';
        $result = $this->db->query($query)->result();

        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $sheet
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'No Registrasi')
                      ->setCellValue('C1', 'Tanggal Registrasi')
                      ->setCellValue('D1', 'Nomor Kamar')
                      ->setCellValue('E1', 'Berat Masuk')
                      ->setCellValue('F1', 'Tipe Ternak')
                      ->setCellValue('G1', 'Jenis Ternak')
                      ->setCellValue('H1', 'Tipe Kandang');
        $kolom = 2;
        $nomor = 1;     

        foreach($result as $idx => $record) {

            $sheet
                        ->setCellValue('A' . $kolom, $nomor)
                        ->setCellValue('B' . $kolom, $record->REGISTER_NO)
                        ->setCellValue('C' . $kolom, date_reformat($record->REGISTER_DATE, 'j M Y', '&ndash;'))
                        ->setCellValue('D' . $kolom, $record->ROOM_NUMBER)
                        ->setCellValue('E' . $kolom, $record->WEIGHT_IN)
                        ->setCellValue('F' . $kolom, $record->TIPE_TERNAK)
                        ->setCellValue('G' . $kolom, $record->JENIS_TERNAK)
                        ->setCellValue('H' . $kolom, $record->TIPE_KANDANG);

            $kolom++;
            $nomor++;

       }

       $writer = new Xlsx($spreadsheet);

       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Registrasi.xlsx"');
       header('Cache-Control: max-age=0');

       $writer->save('php://output');
    }

    public function checkNoreg($noreg){
        $result = FALSE;

        $this->db->select('*');
        $this->db->from('TBL_FARM A');
        $this->db->where('A.REGISTER_NO', $noreg);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $result=TRUE;
        }

        return $result;
    }

    private function validateDate($param){
        $result=FALSE;
        if(date("Y", strtotime($param))=== "1970"){
            $result=TRUE;
        };

        return $result;
    }

    public function lists_order(){
        $query=ORDER_FARM_LIST;
        $data['order_farm'] = $this->get_paging($query,URL_ORDER_FARM);
        $this->data['data'] = $data;
        $this->set_breadcrump('Order Domba' , 'List Data');
        $this->page = "registrasi/lists_order";
        $this->layout();
    }

    public function form_order($id = NULL){
        $data = [
            'jenis_ternak' => NULL,
            'tipe_ternak'  => NULL, 
            'id_vendor'    => NULL,
            'flow_data'    => [
                'id_order_farm' => NULL,
                'tipe_ternak'   => NULL,
                'jenis_ternak'  => NULL, 
                'id_vendor'     => NULL,
                'nama_vendor'   => NULL,
                'alamat_vendor' => NULL,
                'phone_vendor'  => NULL,
                'pic_vendor'    => NULL,
                'estimation_date'  => NULL,
                'order_date'    => NULL,
                'tax_value'     => NULL,
                'tax'           => NULL,
                'value'         => [],
            ],
            'orders' => [],
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_LIVESTOCK');
        $query2 = $this->db->get();

        $records = [];
        foreach ($query2->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_ternak'] = $records;

        $this->db->select('ID_VENDOR, VENDOR_NAME');
        $this->db->from('TBL_VENDOR');
        $this->db->where('IS_ACTIVE', 1);
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->VENDOR_NAME,
                'master_code' => $row->ID_VENDOR
            ];
                
        }

        if ($id === NULL) {
            $data['flow_data'] = [
                   'id_order_farm' => NULL, 
                   'tipe_ternak'   => NULL,
                   'jenis_ternak'  => NULL, 
                   'id_vendor'     => $this->input->post('id_vendor'),
                   'estimation_date'  => $this->input->post('estimation_date'),
                   'order_date'    => $this->input->post('order_date'),
                   'tax_value'     => $this->input->post('tax_value'),
                   'tax'           => $this->input->post('tax'),
                   'alamat_vendor' => NULL,
                   'phone_vendor'  => NULL,
                   'pic_vendor'    => NULL,
                   'value'         => $this->form_validation->error_array(),
            ];

            if ($this->input->post('tipe_ternak') !== NULL) {
                $examCount = count($this->input->post('tipe_ternak'));
                for ($examIdx = 0; $examIdx < $examCount; $examIdx++) {
                     $orderall = [
                          'tipe_ternak'  =>$this->input->post('tipe_ternak')[$examIdx],
                          'jenis_ternak' =>$this->input->post('jenis_ternak')[$examIdx],
                          'qty'          =>$this->input->post('qty')[$examIdx],
                          'price_farm'   =>$this->input->post('price_farm')[$examIdx],
                          'total'        =>$this->input->post('total')[$examIdx],
                          'biaya_lain'   =>$this->input->post('biaya_lain')[$examIdx],
                     ];
                    $data['orders'][] = $orderall;
                };
            }

            $this->data['data'] = $data;
        } else {
            $this->db->select('tohf.ORDER_NO, tof.ID_FARM_DETAIL, tof.TYPE_FARM, tof.JENIS_FARM, tohf.ID_VENDOR,
	                           tv.VENDOR_ADDRESS, tv.VENDOR_PHONE, tv.VENDOR_PIC, tv.VENDOR_NAME,
	                           tohf.ACTUAL_DATE, tohf.TAX_VALUE, tohf.TAX, tof.QTY, tof.PRICE_KG, tof.TOTAL, tof.BIAYA_LAIN,
                               gs.DESCR AS NAME_TYPE_FARM, gs2.DESCR AS NAME_JENIS_FARM,tohf.ESTIMATION_DATE,tohf.ORDER_DATE'); 
            $this->db->from('TBL_ORDER_HEADER_FARM tohf');
            $this->db->join('TBL_ORDER_FARM tof', 'tohf.ORDER_NO = tof.ORDER_NO','INNER'); 
            $this->db->join('TBL_VENDOR tv', 'tv.ID_VENDOR = tohf.ID_VENDOR','INNER'); 
            $this->db->join('GENERAL_SETTING gs', 'gs.MASTER_CODE = tof.TYPE_FARM','INNER');
            $this->db->join('GENERAL_SETTING gs2', 'gs2.MASTER_CODE = tof.JENIS_FARM','INNER');
            $this->db->where('tof.ID_FARM_DETAIL',$id);
            $query = $this->db->get();
            $data['flow_data'] = [
                'id_order_farm' => $query->row('ID_FARM_DETAIL'),
                'tipe_ternak'   => $query->row('TYPE_FARM'),
                'jenis_ternak'  => $query->row('JENIS_FARM'), 
                'id_vendor'     => $query->row('ID_VENDOR'),
                'alamat_vendor' => $query->row('VENDOR_ADDRESS'),
                'phone_vendor'  => $query->row('VENDOR_PHONE'),
                'pic_vendor'    => $query->row('VENDOR_PIC'),
                'estimation_date'  => date("d/m/Y", strtotime($query->row('ESTIMATION_DATE'))),
                'order_date'    => date("d/m/Y", strtotime($query->row('tohf.ORDER_DATE'))),
                'tax_value'     => $query->row('TAX_VALUE'),
                'tax'           => $query->row('TAX'),
                'value'         => $this->form_validation->error_array(),
            ];

            $orderall=[
                'tipe_ternak' => $query->row('TYPE_FARM'),
                'jenis_ternak'=> $query->row('JENIS_FARM'),
                'qty'         => $query->row('QTY'),
                'price_farm'  => $query->row('PRICE_KG'),
                'total'       => $query->row('TOTAL'),
                'biaya_lain'  => $query->row('BIAYA_LAIN'),
                'name_tipe_ternak' =>$query->row('NAME_TYPE_FARM'),
                'name_jenis_ternak' =>$query->row('NAME_JENIS_FARM'),
                'tipe_ternak'   => $query->row('TYPE_FARM'),
                'jenis_ternak'  => $query->row('JENIS_FARM'), 
            ];
            $data['orders'][] = $orderall;
        }

        $data['id_vendor'] = $records;
        $this->data['data'] = $data;
        $this->set_breadcrump('Order Domba' , 'Input data');
        $this->page = "registrasi/form_order";
        $this->layout();

    }

    public function save_order($id = null){
        $this->form_validation
        ->set_rules('id_vendor', "Vendor Name", 'trim|required')
        ->set_rules('estimation_date', "Estimasi Kedatangan", 'trim|required')
        ->set_rules('order_date', "Tanggal Order", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form_order();
            return;
        } 

        try {
            $header = array(
                'STATUS'    => 'ODR',
                'INVOICE'   => 'NULL', 
                'USR_CRT'   => $this->session->userdata('iduser'),
                'DTM_CRT'   => date('Y-m-d H:i:s'),
                'ID_VENDOR' => $this->input->post('id_vendor'),
                'ESTIMATION_DATE' => date("Y-m-d", strtotime($this->input->post('estimation_date'))),
                'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date'))),
                'TAX'         => $this->input->post('tax_value'),
                'TAX_VALUE'   => $this->input->post('tax'),
                'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date')))
            );
            $this->db->insert('TBL_ORDER_HEADER_FARM', $header);

            $this->db->select('ORDER_NO');
            $this->db->from('TBL_ORDER_HEADER_FARM');
            $this->db->order_by("ORDER_NO", "DESC");
            $this->db->limit(1);
            $query = $this->db->get();

            for ($sessionIdx=0; $sessionIdx<count($this->input->post('tipe_ternak')); $sessionIdx++) {
                $data = array(
                    'ORDER_NO'  =>$query->row('ORDER_NO'),
                    'STATUS'    =>'ODR',
                    'TYPE_FARM' =>$this->input->post('tipe_ternak')[$sessionIdx],
                    'JENIS_FARM' =>$this->input->post('jenis_ternak')[$sessionIdx],
                    'QTY'        =>$this->input->post('qty')[$sessionIdx],
                    'PRICE_KG'   =>$this->input->post('price_farm')[$sessionIdx],
                    'TOTAL'      =>$this->input->post('total')[$sessionIdx],
                    'BIAYA_LAIN' =>$this->input->post('biaya_lain')[$sessionIdx],
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'USR_CRT' => $this->session->userdata('iduser'),
                );
                $this->db->insert('TBL_ORDER_FARM', $data);
            };


        }catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form_order();
        }
        
        redirect('registrasi/lists_order');
    }

    public function update_order($id){
        $this->form_validation
        ->set_rules('id_vendor', "Vendor Name", 'trim|required')
        ->set_rules('estimation_date', "Estimasi Kedatangan", 'trim|required')
        ->set_rules('order_date', "Tanggal Order", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form_order($id);
            return;
        }   

        try {

            $this->db->select('ORDER_NO');
            $this->db->from('TBL_ORDER_FARM');
            $this->db->where("ID_FARM_DETAIL", $id);
            $query1 = $this->db->get();
            $header = array(
                'STATUS'    => 'ODR',
                'INVOICE'   => 'NULL', 
                'USR_CRT'   => $this->session->userdata('iduser'),
                'DTM_CRT'   => date('Y-m-d H:i:s'),
                'ID_VENDOR' => $this->input->post('id_vendor'),
                'ESTIMATION_DATE' => date("Y-m-d", strtotime($this->input->post('estimation_date'))),
                'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date'))),
                'TAX'         => $this->input->post('tax_value'),
                'TAX_VALUE'   => $this->input->post('tax'),
                'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date')))
            );
            $this->db->where('ORDER_NO', $query1->row('ORDER_NO'));
            $this->db->update('TBL_ORDER_HEADER_FARM', $header);

            for ($sessionIdx=0; $sessionIdx<count($this->input->post('tipe_ternak')); $sessionIdx++) {
                $data = array(
                    'STATUS'    =>'ODR',
                    'TYPE_FARM' =>$this->input->post('tipe_ternak')[$sessionIdx],
                    'JENIS_FARM' =>$this->input->post('jenis_ternak')[$sessionIdx],
                    'QTY'        =>$this->input->post('qty')[$sessionIdx],
                    'PRICE_KG'   =>$this->input->post('price_farm')[$sessionIdx],
                    'TOTAL'      =>$this->input->post('total')[$sessionIdx],
                    'BIAYA_LAIN' =>$this->input->post('biaya_lain')[$sessionIdx],
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'USR_CRT' => $this->session->userdata('iduser'),
                );
                $this->db->where('ID_FARM_DETAIL', $id);
                $this->db->update('TBL_ORDER_FARM', $data);
            };


        }catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form_order($id);
        }
        redirect('registrasi/lists_order');
    }

    public function listPo() {
        $data=[
            'order_pakan_po' => NULL,
        ];

        $query=ORDER_FARM_LIST_PO;
        $data['order_farm_po'] = $this->get_paging($query,ORDER_FARM_LIST_PO);
        $this->data['data'] = $data;
        $this->set_breadcrump('PO' , 'List Data');
        $this->page = "registrasi/lists_po";
        $this->layout();
    }

    public function pdf_po($id_vendor, $order_date,$order_no){

        $data=[
            'header' => NULL,
            'detail' => NULL,   
        ];

        $this->db->select('*');
        $this->db->from('TBL_ORDER_HEADER_FARM');
        $this->db->where('ORDER_NO',$order_no);
        $this->db->where('INVOICE','NULL');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = array(
                'INVOICE'   => $this->getPoNumber(), 
                'STATUS'    => 'PPO'
            );
            $this->db->where('ORDER_NO', $order_no);
            $this->db->update('TBL_ORDER_HEADER_FARM', $data);

        } 

        $this->db->select('VENDOR_NAME,VENDOR_ADDRESS,VENDOR_PHONE,VENDOR_PIC');
        $this->db->from('TBL_VENDOR');
        $this->db->where('ID_VENDOR', $id_vendor);
        $query1 = $this->db->get();

        $this->db->select('INVOICE');
        $this->db->from('TBL_ORDER_HEADER_FARM');
        $this->db->where('ORDER_NO', $order_no);
        $query2 = $this->db->get();

        $data['header'] = [
            'vendor_name' =>$query1->row('VENDOR_NAME'),
            'vendor_address' =>$query1->row('VENDOR_ADDRESS'),
            'vendor_phone' =>$query1->row('VENDOR_PHONE'),
            'vendor_pic'=>$query1->row('VENDOR_PIC'),
            'invoice'   =>$query2->row('INVOICE'),
        ];

        $this->db->select('gs.DESCR AS TYPE_FARM, gs2.DESCR AS JENIS_FARM, tof.QTY, tof.PRICE_KG, tof.TOTAL, tof.BIAYA_LAIN,
                           tohf.TAX, tohf.TAX_VALUE'); 
        $this->db->from(' TBL_ORDER_FARM tof');
        $this->db->join('GENERAL_SETTING gs', 'gs.MASTER_CODE = tof.TYPE_FARM','INNER'); 
        $this->db->join('GENERAL_SETTING gs2','gs2.MASTER_CODE = tof.JENIS_FARM','INNER'); 
        $this->db->join('TBL_ORDER_HEADER_FARM tohf', 'tohf.ORDER_NO = tof.ORDER_NO','INNER'); 
        $this->db->where('tof.ORDER_NO',$order_no); 
        $query3 = $this->db->get();
        $records = [];
        foreach ($query3->result() as $row)
        {
            $records[] = [
                'type_farm' => $row->TYPE_FARM,
                'jenis_farm'=> $row->JENIS_FARM,
                'qty'       => $row->QTY,
                'price_kg'  => $row->PRICE_KG,
                'total'     => $row->TOTAL,
                'biaya_lain'=> $row->BIAYA_LAIN,
                'tax'       => $row->TAX,
                'tax_value' => $row->TAX_VALUE,
            ];
                
        }
        $data['detail']  = $records;

        $html = $this->load->view('registrasi/pdf_po',$data);

    }

    public function receive($order_no) {
        $data = [
            'orders'       => [],
            'tipe_kandang' => NULL,
            'flow_data' => [
              'order_no'=> $order_no,
              'value'   => $this->form_validation->error_array()
            ]
        ];
        $this->db->select('tof.TYPE_FARM, tof.JENIS_FARM, gs.DESCR AS NAME_TYPE_FARM, gs2.DESCR AS NAME_JENIS_FARM'); 
        $this->db->from('TBL_ORDER_HEADER_FARM tohf'); 
	    $this->db->join('TBL_ORDER_FARM tof','tohf.ORDER_NO = tof.ORDER_NO','INNER'); 
	    $this->db->join('GENERAL_SETTING gs','gs.MASTER_CODE = tof.TYPE_FARM','INNER');  
	    $this->db->join('GENERAL_SETTING gs2','gs2.MASTER_CODE = tof.JENIS_FARM','INNER');
        $this->db->where('tohf.ORDER_NO',$order_no);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'type_farm'     => $row->TYPE_FARM,
                'jenis_farm'    => $row->JENIS_FARM,
                'name_type_farm' =>$row->NAME_TYPE_FARM,
                'name_jenis_farm'=>$row->NAME_JENIS_FARM
            ];
                
        }
        $data['orders']=$records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang']   = $records;
        $value=NULL;
        if($this->getError()!== NULL){
            $value=$this->getError();
        } else {
            $value = $this->form_validation->error_array();
        };
        $data['value']          = $value;
        $this->data['data']     = $data;
        $this->set_breadcrump('Receive' , 'Form Data');
        $this->page = "registrasi/receive";
        $this->layout();
    }

    public function proses_receive($order_no){
        $this->form_validation
        ->set_rules('actual_date', "Tanggal Kedatangan", 'trim|required')
        ->set_rules('type_farm_1[]', "Type Domba", 'trim|required')
        ->set_rules('jenis_farm_1[]', "Jenis Domba", 'trim|required')
        ->set_rules('noregis[]', "No Registrasi", ['trim','required', 'max_length[11]'])
        ->set_rules('register_date[]', "Tanggal Registrasi", 'trim|required')
        ->set_rules('weight[]', "Berat Masuk", ['trim','required'])
        ->set_rules('tipe_kandang[]', "Tipe Kandang", 'trim|required')
        ->set_rules('noroom[]', "Nomor Kamar", ['trim','required', 'max_length[5]']);
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->receive($order_no);
            return;
        } 

        try {
            for ($sessionIdx=0; $sessionIdx<count($this->input->post('noregis')); $sessionIdx++) {
                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = './assets/'; //string, the default is application/cache/
                $config['errorlog']     = './assets/'; //string, the default is application/logs/
                $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224,255,255); // array, default is array(255,255,255)
                $config['white']        = array(70,130,180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name=$this->input->post('noregis')[$sessionIdx].'.png'; //buat name dari qr code sesuai dengan nim
     
                $params['data'] = $this->input->post('noregis')[$sessionIdx]; //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
                // var_dump(date("Y-m-d", );die();

                $data = array(
                    'REGISTER_NO'   => $this->input->post('noregis')[$sessionIdx],
                    'REGISTER_DATE' => date("Y-m-d", strtotime($this->input->post('register_date')[$sessionIdx])),
                    'TYPE_FARM'     => $this->input->post('jenis_farm_1')[$sessionIdx],
                    'WEIGHT_IN'     => $this->input->post('weight')[$sessionIdx],
                    'TYPE_CAGE'     => $this->input->post('tipe_kandang')[$sessionIdx],
                    'TYPE_LIVESTOCK' => $this->input->post('type_farm_1')[$sessionIdx],
                    'ROOM_NUMBER'    => $this->input->post('noroom')[$sessionIdx],
                    'STATUS'         => STATUS_ACTIVE,   
                    'CREATE_DATE'    => date('Y-m-d H:i:s'),
                    'USR_CRT'        => $this->session->userdata('iduser'),
                    'IMAGE'          => $image_name,
                );

                if ($this->checkNoreg($this->input->post('noregis')[$sessionIdx])) {
                    $this->setError($this->input->post('noregis')[$sessionIdx]." Already save on system");
                    throw new Exception("Duplicate input noregistrasi");
                }
                
                if($this->validateDate($this->input->post('register_date')[$sessionIdx])){
                    $this->setError(" Date format invalid please check again");
                    throw new Exception("Date Format invalid");
                }
    
                $this->db->insert('TBL_FARM', $data);
                $logs = array(
                    'USR_CRT' => $this->session->userdata('iduser'),
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'DESCR'   => json_encode($data), 
                );
                $this->db->insert('TBL_LOG', $logs);
                $this->set_stock();

            }
            $updates = array(
               'STATUS' =>'RCV',
               'ACTUAL_DATE' => date("Y-m-d", strtotime($this->input->post('actual_date'))),
            );

            $this->db->where('ORDER_NO', $order_no);
            $this->db->update('TBL_ORDER_HEADER_FARM', $updates);

           
        } catch (Exception $e) {
            // log_message('error ',"asdasd");
            $this->receive($order_no);
            return;
        }
        redirect('registrasi/listPo');
    }
}

