<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penimbangan extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists() {
        $data=[
            'penimbangan' => NULL,
        ];
        $query=DAILY_WEIGHT_LIST.' WHERE b.STATUS='.STATUS_ACTIVE.' ORDER BY ID_DAILY_WEIGHT DESC';
        $data['penimbangan'] = $this->get_paging($query,URL_PENIMBANGAN);
        $this->data['data'] = $data;
        $this->set_breadcrump('Penimbangan' , 'List Data');
        $this->page = "penimbangan/index";
        $this->layout();
    }


    public function getSelectRegisterNo() {
        $searchTerm = $this->input->post('searchTerm');

        $response   = $this->getRegisterNo($searchTerm);
        echo json_encode($response);
    }
    /**
     * Form Penimbangan Harian
     * @param integer
     */
    public function form($id = NULL){
        $data = [
            'flow_data'        => [
               'ID_DAILY_WEIGHT' => NULL, 
               'REGISTER_NO'     => NULL,
               'WEIGHT_DATE'     => NULL,
               'WEIGHT'          => NULL,
               'ADD_VITAMIN'     => NULL,
               'value'           => NULL,
            ]
        ];
        if ($id === NULL) {
            $data['flow_data'] = [
                'ID_DAILY_WEIGHT' => NULL, 
                'REGISTER_NO'     => $this->input->post('registrasi_no'),
                'WEIGHT_DATE'     => $this->input->post('weight_date'),
                'WEIGHT'          => $this->input->post('weight'),
                'ADD_VITAMIN'     => $this->input->post('add_vitamin'),
                'value'           => $this->form_validation->error_array(),
            ];   
            
            $this->data['data'] = $data;
        } else {
            $this->db->select('ID_DAILY_WEIGHT, REGISTER_NO, WEIGHT_DATE, WEIGHT, ADD_VITAMIN, DTM_CRT, DTM_UPD, USR_CRT, USR_UPD');
            $this->db->from('TBL_DAILY_WEIGHT');
            $this->db->where('ID_DAILY_WEIGHT', $id);
            $query = $this->db->get();
            $data['flow_data'] = [
                'ID_DAILY_WEIGHT' => $query->row('ID_DAILY_WEIGHT'), 
                'REGISTER_NO'     => $query->row('REGISTER_NO'),
                'WEIGHT_DATE'     => date("d/m/Y", strtotime($query->row('WEIGHT_DATE'))),
                'WEIGHT'          => $query->row('WEIGHT'),
                'ADD_VITAMIN'     => $query->row('ADD_VITAMIN'),
                'value'           => $this->form_validation->error_array(),
            ];   
            $this->data['data'] = $data;
        }
        $this->set_breadcrump('Penimbangan' , 'Input data');
        $this->page = "penimbangan/form";
        $this->layout();
    }

    public function save () {
        $this->form_validation
        ->set_rules('registrasi_no', "No Registrasi", 'trim|required')
        ->set_rules('weight_date', "Tanggal Timbang", 'trim|required')
        ->set_rules('weight', "Berat Timbang", ['trim','decimal','required'])
        ->set_rules('add_vitamin', "Vitamin", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        $query2 = SEARCH_CATEGORY.' WHERE '.$this->input->post('weight').' >= RANGE_FIRST AND '.$this->input->post('weight').'<= RANGE_TWO';
        $result = $this->db->query($query2)->result();
        try {

            $this->db->select('A.REGISTER_NO, A.WEIGHT, A.ID_DAILY_WEIGHT');
            $this->db->from('TBL_DAILY_WEIGHT A');
            $this->db->where('A.REGISTER_NO', $this->input->post('registrasi_no'));
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                
                $LOG_FARM = array(
                    'NO_REGISTRASI' => $this->input->post('registrasi_no'),
                    'TYPE'          => TYPE_LOG_DAILY_TIMBANG,
                    'FROM'          => $query->row('WEIGHT'),
                    'TO'            => $this->input->post('weight'),
                    'DTM_OTHER'     => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                    'USR_CRT'       => $this->session->userdata('iduser'),
                    'DTM_CRT'       => date('Y-m-d H:i:s'),
                );
                $this->db->insert('TBL_LOG_FARM', $LOG_FARM);
                
                $data = array(
                    'REGISTER_NO' => $this->input->post('registrasi_no'),
                    'WEIGHT_DATE' => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                    'WEIGHT'      => $this->input->post('weight'),
                    'ADD_VITAMIN' => $this->input->post('add_vitamin'),
                    // 'TYPE_CATEGORY' => $query2->row('MASTER_CODE'),
                    'DTM_CRT'     => date('Y-m-d H:i:s'),
                    'USR_CRT'     => $this->session->userdata('iduser'),
                );
    
                $this->db->where('ID_DAILY_WEIGHT', $query->row('ID_DAILY_WEIGHT'));
                $this->db->update('TBL_DAILY_WEIGHT', $data);
    
                $logs = array(
                    'USR_CRT' => $this->session->userdata('iduser'),
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'DESCR'   => json_encode($data), 
                );
                $this->db->insert('TBL_LOG', $logs);

            } else {
                $LOG_FARM = array(
                    'NO_REGISTRASI' => $this->input->post('registrasi_no'),
                    'TYPE'          => TYPE_LOG_DAILY_TIMBANG,
                    'FROM'          => "-",
                    'TO'            => $this->input->post('weight'),
                    'DTM_OTHER'     => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                    'USR_CRT'       => $this->session->userdata('iduser'),
                    'DTM_CRT'       => date('Y-m-d H:i:s'),
                );
                $this->db->insert('TBL_LOG_FARM', $LOG_FARM);

                $data = array(
                    'REGISTER_NO' => $this->input->post('registrasi_no'),
                    'WEIGHT_DATE' => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                    'WEIGHT'      => $this->input->post('weight'),
                    'ADD_VITAMIN' => $this->input->post('add_vitamin'),
                    'TYPE_CATEGORY' => $result[0]->MASTER_CODE,
                    'DTM_CRT'     => date('Y-m-d H:i:s'),
                    'USR_CRT'     => $this->session->userdata('iduser'),
                );
    
                $this->db->insert('TBL_DAILY_WEIGHT', $data);
    
                $logs = array(
                    'USR_CRT' => $this->session->userdata('iduser'),
                    'DTM_CRT' => date('Y-m-d H:i:s'),
                    'DESCR'   => json_encode($data), 
                );
                $this->db->insert('TBL_LOG', $logs);
            }
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('penimbangan/lists');
    }

    public function update ($id) {
        $this->form_validation
        ->set_rules('registrasi_no', "No Registrasi", 'trim|required')
        ->set_rules('weight_date', "Tanggal Timbang", 'trim|required')
        ->set_rules('weight', "Berat Timbang", ['trim','decimal','required'])
        ->set_rules('add_vitamin', "Vitamin", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    
        $query2 = SEARCH_CATEGORY.' WHERE '.$this->input->post('weight').' >= RANGE_FIRST AND '.$this->input->post('weight').'<= RANGE_TWO';
        $result = $this->db->query($query2)->result();
        
        try {
            $this->db->select('A.REGISTER_NO, A.WEIGHT, A.ID_DAILY_WEIGHT');
            $this->db->from('TBL_DAILY_WEIGHT A');
            $this->db->where('A.REGISTER_NO', $this->input->post('registrasi_no'));
            $query = $this->db->get();

            $LOG_FARM = array(
                'NO_REGISTRASI' => $this->input->post('registrasi_no'),
                'TYPE'          => TYPE_LOG_DAILY_TIMBANG,
                'FROM'          => $query->row('WEIGHT'),
                'TO'            => $this->input->post('weight'),
                'DTM_OTHER'     => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                'USR_CRT'       => $this->session->userdata('iduser'),
                'DTM_CRT'       => date('Y-m-d H:i:s'),
            );
            $this->db->insert('TBL_LOG_FARM', $LOG_FARM);


            $data = array(
                'REGISTER_NO' => $this->input->post('registrasi_no'),
                'WEIGHT_DATE' => date("Y-m-d", strtotime($this->input->post('weight_date'))),
                'WEIGHT'      => $this->input->post('weight'),
                'ADD_VITAMIN' => $this->input->post('add_vitamin'),
                'TYPE_CATEGORY' => $result[0]->MASTER_CODE,
                'DTM_CRT'     => date('Y-m-d H:i:s'),
                'USR_CRT'     => $this->session->userdata('iduser'),
            );

            $this->db->where('ID_DAILY_WEIGHT', $id);
            $this->db->update('TBL_DAILY_WEIGHT', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);

            
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('penimbangan/lists');
    }

    public function search(){
        $search  = ($this->input->post("table_search"))? $this->input->post("table_search") : NULL;
        $query=DAILY_WEIGHT_SEARCH.' WHERE b.STATUS='.STATUS_ACTIVE.' and '; 
        if ($search !== NULL) {
            $data['penimbangan'] = $this->get_paging($query." a.REGISTER_NO like '%".$search."%'",URL_REGISTRASI);            
        } else {
            $data['penimbangan'] = $this->get_paging($query." a.REGISTER_NO like '% NULL %'",URL_REGISTRASI);            
        }

        $this->data['data'] = $data;
        $this->set_breadcrump('Penimbangan' , 'List Data');
        $this->page = "penimbangan/index";
        $this->layout();
    }

    public function getLogWeight(){

        $this->db->select('a.NO_REGISTRASI, a.FROM, a.TO, a.DTM_OTHER, a.DTM_CRT');
        $this->db->from('TBL_LOG_FARM a');    
        $this->db->where('a.NO_REGISTRASI', $this->input->post('searchTerm'));
        $this->db->where('a.TYPE', TYPE_LOG_DAILY_TIMBANG);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'noregistrasi' => $row->NO_REGISTRASI,
                'from'         => $row->FROM,
                'to'           => $row->TO,
                'dtm_timbang'  => date("d-m-Y H:i:s", strtotime($row->DTM_OTHER)), 
                'dtm_crt'      => date("d-m-Y H:i:s", strtotime($row->DTM_CRT)) 
            ];
                
        }

        echo json_encode($records);
    }

}