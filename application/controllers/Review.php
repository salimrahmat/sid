<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Review extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function stock_ternak() {
        
    }

    public function progres_ternak() {
        $data=[
            'tipe_kandang'  => NULL, 
            'kategori'      => NULL,
            'jenis_ternak'  => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['tipe_kandang'] = $records;

        $this->db->select('a.MASTER_CODE,b.DESCR');
        $this->db->from('TBL_KATEGORI a');
        $this->db->join('GENERAL_SETTING b', 'a.MASTER_CODE = b.MASTER_CODE', 'INNER');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['kategori'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('RANGE_FIRST, RANGE_TWO');
        $this->db->from('TBL_KATEGORI ');
        $this->db->where('MASTER_CODE', $this->input->post('kategori'));
        $query = $this->db->get();

        if ($this->input->post('table_search')===NULL) {
            $query = PROGRESS_FARM;
        } else if ($this->input->post('table_search') !=="" ) {
            $query = PROGRESS_FARM.' WHERE a.ROOM_NUMBER='.$this->input->post('table_search').' AND a.TYPE_CAGE='."'".$this->input->post('tipe_kandang')."'".' AND a.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'".' AND c.WEIGHT BETWEEN '.$query->row('RANGE_FIRST').' AND '.$query->row('RANGE_TWO');  
        } else {
            $query = PROGRESS_FARM.' WHERE a.TYPE_CAGE='."'".$this->input->post('tipe_kandang')."'".' AND a.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'".' AND c.WEIGHT BETWEEN '.$query->row('RANGE_FIRST').' AND '.$query->row('RANGE_TWO');  
        }

        $data['progress_ternak'] = $this->get_paging($query,URL_PROGRESS_TERNAK);
        $this->data['data'] = $data;
        $this->set_breadcrump('Progress Ternak' , 'Report Data');
        $this->page = "review/progress";
        $this->layout();

    }
    
    public function check_ternak() {
        $data=[
            'jenis_ternak' => NULL,
            'tipe_ternak'  => NULL,
            'kategori'     => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FARM');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_ternak'] = $records;

        $this->db->select('a.MASTER_CODE,b.DESCR');
        $this->db->from('TBL_KATEGORI a');
        $this->db->join('GENERAL_SETTING b', 'a.MASTER_CODE = b.MASTER_CODE', 'INNER');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }

        $data['kategori'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_LIVESTOCK');
        $query2 = $this->db->get();

        $records = [];
        foreach ($query2->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_ternak'] = $records;
  
        if ($this->input->post('tipe_ternak') === "" && $this->input->post('jenis_ternak') === "" && $this->input->post('kategori')=== "") {
            $data['kategori_check']='ALL';
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE;
        } else if ($this->input->post('tipe_ternak') !== "" && $this->input->post('jenis_ternak') === "" && $this->input->post('kategori')=== ""){ 
            $data['kategori_check']='ALL';                        
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_LIVESTOCK='."'".$this->input->post('tipe_ternak')."'";
        } else if ($this->input->post('tipe_ternak') === "" && $this->input->post('jenis_ternak') !== "" && $this->input->post('kategori')=== "") {
            $data['kategori_check']='ALL';                        
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'";
        } else if ($this->input->post('tipe_ternak') === "" && $this->input->post('jenis_ternak') === "" && $this->input->post('kategori')!== "") {
            $this->db->select('A.RANGE_FIRST, A.RANGE_TWO','B.DESCR');
            $this->db->from('TBL_KATEGORI A');
            $this->db->join('GENERAL_SETTING B','A.MASTER_CODE = B.MASTER_CODE','INNER');
            $this->db->where('A.MASTER_CODE', $this->input->post('kategori'));
            $query2= $this->db->get();
            $data['kategori_check']=$query2->row('DESCR');
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND B.WEIGHT BETWEEN '.$query2->row('RANGE_FIRST').' AND '.$query2->row('RANGE_TWO');
        } else if ($this->input->post('tipe_ternak') !== "" && $this->input->post('jenis_ternak') !== "" && $this->input->post('kategori')=== "") {
            $data['kategori_check']='ALL';                        
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_LIVESTOCK='."'".$this->input->post('tipe_ternak')."'".'AND A.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'";
        } else if ($this->input->post('tipe_ternak') !== "" && $this->input->post('jenis_ternak') === "" && $this->input->post('kategori') !== "") {
            $this->db->select('A.RANGE_FIRST, A.RANGE_TWO','B.DESCR');
            $this->db->from('TBL_KATEGORI A');
            $this->db->join('GENERAL_SETTING B','A.MASTER_CODE = B.MASTER_CODE','INNER');
            $this->db->where('A.MASTER_CODE', $this->input->post('kategori'));
            $query2= $this->db->get();
            $data['kategori_check']=$query2->row('DESCR');                      
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_LIVESTOCK='."'".$this->input->post('tipe_ternak')."'".'AND B.WEIGHT BETWEEN '.$query2->row('RANGE_FIRST').' AND '.$query2->row('RANGE_TWO');
        } else if ($this->input->post('tipe_ternak') === "" && $this->input->post('jenis_ternak') !== "" && $this->input->post('kategori') !== "") {
            $this->db->select('A.RANGE_FIRST, A.RANGE_TWO','B.DESCR');
            $this->db->from('TBL_KATEGORI A');
            $this->db->join('GENERAL_SETTING B','A.MASTER_CODE = B.MASTER_CODE','INNER');
            $this->db->where('A.MASTER_CODE', $this->input->post('kategori'));
            $query2= $this->db->get();
            $data['kategori_check']=$query2->row('DESCR');                      
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'".'AND B.WEIGHT BETWEEN '.$query2->row('RANGE_FIRST').' AND '.$query2->row('RANGE_TWO');
        } else {
            $data['kategori_check']='ALL';
            $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE;
        }

        // if ($this->input->post('tipe_ternak') !== NULL ){
        //     $this->db->select('A.RANGE_FIRST, A.RANGE_TWO','B.DESCR');
        //     $this->db->from('TBL_KATEGORI A');
        //     $this->db->join('GENERAL_SETTING B','A.MASTER_CODE = B.MASTER_CODE','INNER');
        //     $this->db->where('A.MASTER_CODE', $this->input->post('kategori'));
        //     $query2= $this->db->get();
        //     $data['kategori_check']=$query2->row('DESCR');
        //     $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE.' AND A.TYPE_FARM='."'".$this->input->post('jenis_ternak')."'".' AND A.TYPE_LIVESTOCK='."'".$this->input->post('jenis_ternak')."'".' AND B.WEIGHT BETWEEN '.$query2->row('RANGE_FIRST').' AND '.$query2->row('RANGE_TWO');
        // } 
        // else {
        //     $data['kategori_check']='ALL';
        //     $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE;
        // }

        $data['check_ternak'] = $this->get_paging($query,URL_CHECK_TERNAL_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('Check Ternak' , 'Report Data');
        $this->page = "review/check_ternak";
        $this->layout();
    }
    
    public function chart(){

        $data = [
            'chart' => NULL,
        ];

        if ($this->input->post('searchTerm') === NULL) {
            $query=CHAT_CATEGORY. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY C.DESCR';
        } else if ($this->input->post('searchTerm')==='tipeternak') {
            $query=CHART_TIPE_TERNAK. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY B.DESCR';
            
        }
        
        $result = $this->db->query($query)->result();

        $records = [];
        foreach ($result as $row)
        {
            // $records[] = [
            //     'label' => $row->CATEGORY,
            //     'data' => $row->TOTAL,
            //     'color' =>'#3c8dbc',
            // ];
            $records[] = [
                'value' => $row->TOTAL,
                'color' => $this->rand_color(),
                'highlight' =>$this->rand_color(),
                'label'=>$row->CATEGORY,
            ];
                
        }
        $data['chart'] = $records;
        $this->data['data'] = $data;
        $this->set_breadcrump('Chart Ternak' , 'Chart Data');
        $this->page = "review/chart";
        $this->layout();
    }

    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    public function radiochart(){
        $data = [
            'chart' => NULL,
        ];

        if ($this->input->post('searchTerm') === "category") {
            $query=CHAT_CATEGORY. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY C.DESCR';
        } else if ($this->input->post('searchTerm')==="tipeternak") {
            $query=CHART_TIPE_TERNAK. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY B.DESCR';
        } else if ($this->input->post('searchTerm')==="jenisternak") {
            $query=CHART_JENIS_TERNAK. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY B.DESCR';
        }else if ($this->input->post('searchTerm')==="tipekandang") {
            $query=CHART_TIPE_KANDANG. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY B.DESCR';
        }else if ($this->input->post('searchTerm')==="nomorkandang") {
            $query=NOMOR_KANDANG. ' WHERE A.STATUS='.STATUS_ACTIVE.' GROUP BY A.ROOM_NUMBER ';
        }

        $result = $this->db->query($query)->result();

        $records = [];
        foreach ($result as $row)
        {
            $records[] = [
                'value' => $row->TOTAL,
                'color' => $this->rand_color(),
                'highlight' =>$this->rand_color(),
                'label'=>$row->CATEGORY,
            ];
                
        }
  
        echo json_encode($records);
    }

    public function barchart(){
        $data = [
            'chart' => NULL,
        ];

        if ($this->input->post('searchTerm') === "1") {
            $query='SELECT A.LABEL,A.PERIODE,SUM(A.TOTAL) AS TOTAL, @row_number:=CASE WHEN @periode = A.PERIODE THEN @row_number ELSE @row_number + 1 END AS num,@periode:=A.PERIODE  
                        FROM ( 
                    SELECT CONCAT(C.DESCR,"-",D.DESCR) AS LABEL ,DATE_FORMAT(E.WEIGHT_DATE, "%M-%Y") AS PERIODE, 
                    (REPLACE(B.TO,"-",E.WEIGHT)) - (REPLACE(B.FROM,"-",E.WEIGHT)) AS TOTAL 
                    FROM TBL_FARM A 
                    INNER JOIN TBL_LOG_FARM B ON A.REGISTER_NO = B.NO_REGISTRASI 
                    INNER JOIN GENERAL_SETTING C ON C.MASTER_CODE = A.TYPE_LIVESTOCK 
                    INNER JOIN GENERAL_SETTING D ON D.MASTER_CODE = A.TYPE_FARM 
                    INNER JOIN TBL_DAILY_WEIGHT E ON A.REGISTER_NO = E.REGISTER_NO 
                    WHERE B.TYPE='.TYPE_LOG_DAILY_TIMBANG.' AND A.STATUS='.STATUS_ACTIVE.' AND DATE_FORMAT(E.WEIGHT_DATE,'."'%m/%d/%Y'".')  BETWEEN '."'".$this->input->post('datefrom')."'".' AND '."'".$this->input->post('dateto')."'".'
                    ORDER BY E.WEIGHT_DATE ASC
                    ) A , (SELECT @row_number := 0) r, (SELECT @periode := 0) x GROUP BY A.LABEL,A.PERIODE';
            } else if ($this->input->post('searchTerm') === "2") {
                $query='SELECT A.LABEL,A.PERIODE,A.TOTAL,@row_number:=CASE WHEN @periode = A.PERIODE THEN @row_number  ELSE @row_number + 1  END AS num,@periode:=A.PERIODE 
                        FROM (     
                            SELECT B.DESCR AS LABEL ,DATE_FORMAT(A.FOOD_DATE, "%M-%Y") AS PERIODE, SUM(A.TOTAL_FOOD) AS TOTAL 
                            FROM TBL_FOOD_FARM A 
                            INNER JOIN GENERAL_SETTING B ON A.TYPE_FOOD = B.MASTER_CODE 
                            WHERE DATE_FORMAT(A.FOOD_DATE,'."'%m/%d/%Y'".')  BETWEEN '."'".$this->input->post('datefrom')."'".' AND '."'".$this->input->post('dateto')."'".'
                            GROUP BY B.DESCR,A.FOOD_DATE ORDER BY A.FOOD_DATE
                        ) A , (SELECT @row_number := 0) r, (SELECT @periode := 0) x';
            } else if ($this->input->post('searchTerm') === "3") {
                $query='SELECT A.LABEL,A.PERIODE,A.TOTAL,@row_number:=CASE WHEN @periode = A.PERIODE THEN @row_number  ELSE @row_number + 1  END AS num,@periode:=A.PERIODE 
                        FROM (
                            SELECT CONCAT(C.DESCR,"-",D.DESCR) AS LABEL ,DATE_FORMAT(B.OUTSIDE_DATE, "%M-%Y") AS PERIODE,COUNT(B.REGISTER_NO) AS TOTAL 
                            FROM TBL_FARM A 
                            INNER JOIN TBL_SALE_FARM B ON A.REGISTER_NO = B.REGISTER_NO
                            INNER JOIN GENERAL_SETTING C ON C.MASTER_CODE = A.TYPE_LIVESTOCK 
                            INNER JOIN GENERAL_SETTING D ON D.MASTER_CODE = A.TYPE_FARM
                            WHERE DATE_FORMAT(B.OUTSIDE_DATE,'."'%m/%d/%Y'".')  BETWEEN '."'".$this->input->post('datefrom')."'".' AND '."'".$this->input->post('dateto')."'".' 
                            GROUP BY   C.DESCR,D.DESCR,B.OUTSIDE_DATE
                        ORDER BY B.OUTSIDE_DATE ASC
                        )A , (SELECT @row_number := 0) r, (SELECT @periode := 0) x';
            }
            
        $result = $this->db->query($query)->result();

        $records   = [];
        
        foreach ($result as $row)
        {
            $datatotal = [];
            for ($x = 1; $x <= count($result); $x++) {
                 if ($row->num == $x){
                    $datatotal[]=[$row->TOTAL];
                 } else {
                    $datatotal[]=[0];
                 }
            }
            $records[] = [
                'label'     => $row->LABEL,
                'periode'   => $row->PERIODE,
                'total'     => $row->TOTAL,
                'data'      => $datatotal,
                'backgroundColor' =>$this->rand_color(),
                'borderColor'     =>$this->rand_color()
            ];
        }
        echo json_encode($records);
    }

    public function export_ternak($kategori){
        $query = CHECK_TERNAK.' WHERE A.STATUS='.STATUS_ACTIVE;
        $result = $this->db->query($query)->result();

        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $sheet
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'No Registrasi')
                      ->setCellValue('C1', 'Kategori')
                      ->setCellValue('D1', 'Nomor Kamar')
                      ->setCellValue('E1', 'Berat Badan')
                      ->setCellValue('F1', 'Tanggal Timbang');
        $kolom = 2;
        $nomor = 1;     

        foreach($result as $idx => $record) {

            $sheet
                        ->setCellValue('A' . $kolom, $nomor)
                        ->setCellValue('B' . $kolom, $record->REGISTER_NO)
                        ->setCellValue('C' . $kolom, $kategori)
                        ->setCellValue('D' . $kolom, $record->TIPE_KANDANG)
                        ->setCellValue('E' . $kolom, $record->BERAT_BADAN)
                        ->setCellValue('F' . $kolom, date_reformat($record->TANGGAL_TIMBANG, 'j M Y', '&ndash;'));

            $kolom++;
            $nomor++;

       }

       $writer = new Xlsx($spreadsheet);

       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Review_Ternak.xlsx"');
       header('Cache-Control: max-age=0');

       $writer->save('php://output');
    }

    public function export_progres(){
        $query = PROGRESS_FARM;
        $result = $this->db->query($query)->result();

        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $sheet
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Tipe Kandang')
                      ->setCellValue('C1', 'Nomor Kamar')
                      ->setCellValue('D1', 'No Register')
                      ->setCellValue('E1', 'Tanggal Register')
                      ->setCellValue('F1', 'Berat Masuk')
                      ->setCellValue('G1', 'Tanggal Timbang')
                      ->setCellValue('H1', 'Berat Timbang')
                      ->setCellValue('I1', 'Total Hari')
                      ->setCellValue('J1', 'Total Naik');
        $kolom = 2;
        $nomor = 1;     

        foreach($result as $idx => $record) {
            $sheet
                        ->setCellValue('A' . $kolom, $nomor)
                        ->setCellValue('B' . $kolom, $record->tipe_kandang)
                        ->setCellValue('C' . $kolom, $record->nomor_kamar)
                        ->setCellValue('D' . $kolom, $record->no_registrasi)
                        ->setCellValue('E' . $kolom, date_reformat($record->tanggal_register, 'j M Y', '&ndash;'))
                        ->setCellValue('F' . $kolom, $record->berat_masuk)
                        ->setCellValue('G' . $kolom, date_reformat($record->tanggal_timbang, 'j M Y', '&ndash;'))
                        ->setCellValue('H' . $kolom, $record->berat_timbang)
                        ->setCellValue('I' . $kolom, $record->total_hari)
                        ->setCellValue('J' . $kolom, $record->total_naik)
                        ;

            $kolom++;
            $nomor++;
       }

       $writer = new Xlsx($spreadsheet);

       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Progress_Ternak.xlsx"');
       header('Cache-Control: max-age=0');

       $writer->save('php://output');
    }

}
