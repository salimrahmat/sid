<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_setting extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists(){
        $data['setting'] = $this->get_paging(SETTING_LIST,URL_SETTING_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('List Master' , 'Data Setting Master');
        $this->page = "setting/index";
        $this->layout();
    }

    public function form ($id=NULL) {

        $data = [
            'setting_code' => NULL,
            'flow_data'       => [
               'setting_id'   => NULL, 
               'setting_code' => NULL,
               'mastercode'   => NULL, 
               'descr'        => NULL, 
               'value'        => NULL,
            ]
       ];

        $this->db->select('GENERAL_SETTING_CODE,DESCR');
        $this->db->from('GENERAL_SETTING_TYPE');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'DESCR' => $row->DESCR,
                'GENERAL_SETTING_CODE' => $row->GENERAL_SETTING_CODE
            ];
                
        }
        if ($id === NULL) {
            $data['flow_data'] = [
                'setting_id'   => NULL, 
                'setting_code' => $this->input->post('setting_code'),
                'mastercode'   => $this->input->post('mastercode'), 
                'descr'        => $this->input->post('descr'), 
                'value'        => $this->form_validation->error_array(),
            ]; 
        }
        $data['setting_code'] = $records;
        $this->data['data'] = $data;
        $this->set_breadcrump('Insert Data' , 'Setting Master');
        $this->page = "setting/form";
        $this->layout();
    }
    
    public function save() {
        $this->form_validation
        ->set_rules('setting_code', "Tipe", 'trim|required')
        ->set_rules('mastercode', "Kode Master", 'trim|required|max_length[5]')
        ->set_rules('descr', "Deskripsi", ['trim','required', 'max_length[150]']);
         
        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        try {
            
            $data = array(
                'GENERAL_SETTING_CODE'  => $this->input->post('setting_code'),
                'MASTER_CODE'           => $this->input->post('mastercode'),
                'DESCR'                 => $this->input->post('descr'),
                'IS_ACTIVE'             => 1,
                'USR_CRT'               => $this->session->userdata('iduser'),
                'DTM_CRT'               => date('Y-m-d H:i:s'),
            );


            $this->db->insert('GENERAL_SETTING', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('general_setting/lists');
    }

    public function update($id) {
        try {
            $data = array(
                'IS_ACTIVE'  => 0,
                'DTM_UPD'    => date('Y-m-d H:i:s'),
                'USR_UPD'    => $this->session->userdata('iduser'),
            );

            $this->db->where('GENERAL_SETTING_ID', $id);
            $this->db->update('GENERAL_SETTING', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form($id);
        }
        redirect('general_setting/lists');
    }
}