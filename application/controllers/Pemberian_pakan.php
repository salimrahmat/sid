<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemberian_pakan extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists() {
        $data = [
            'jadwal_pakan' => NULL,
            'jenis_pakan'  => NULL,  
            'tipe_kandang' => NULL
        ];
        $this->db->select('DESCR, MASTER_CODE');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('GENERAL_SETTING_CODE', 'SCHEDULE_FOODING');
            $query = $this->db->get();

            $records = [];
            foreach ($query->result() as $row)
            {
                $records[] = [
                    'descr' => $row->DESCR,
                    'master_code' => $row->MASTER_CODE
                ];
                    
            }
            $data['jadwal_pakan'] = $records;

            $this->db->select('DESCR, MASTER_CODE');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
            $query = $this->db->get();

            $records = [];
            foreach ($query->result() as $row)
            {
                $records[] = [
                    'descr' => $row->DESCR,
                    'master_code' => $row->MASTER_CODE
                ];
                    
            }
            $data['jenis_pakan'] = $records;

            $this->db->select('DESCR, MASTER_CODE');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
            $query = $this->db->get();

            $records = [];
            foreach ($query->result() as $row)
            {
                $records[] = [
                    'descr' => $row->DESCR,
                    'master_code' => $row->MASTER_CODE
                ];
                    
            }
            $data['tipe_kandang'] = $records;
            $this->data['data'] = $data;

            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_LIST,URL_PEMBERIAN_PAKAN);
            $this->data['data'] = $data;
            $this->set_breadcrump('Pemberian Pakan' , 'List Data');
            $this->page = "pemberian_pakan/index";
            $this->layout();
    }
    /**
     * Form Pemberian Pakan
     * @param integer
     */
    public function form($id = NULL){
        $data = [
            'jadwal_pakan' => NULL,
            'jenis_pakan'  => NULL,  
            'tipe_kandang' => NULL,
            'flow_data'        => [
                'id_food_farm'  => NULL, 
                'food_date' => NULL,
                'type_food'   => NULL,
                'tipe_kandang_pakan'  => NULL,
                'total_weight'        => NULL,
                'total_pakan'  => NULL,
                'time_food'        => NULL,
                'value'         => NULL,
             ]
        ]; 
        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'SCHEDULE_FOODING');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jadwal_pakan'] = $records;

        $this->db->select('b.DESCR, b.MASTER_CODE');
        $this->db->from('TBL_ORDER_FOOD a');
        $this->db->join('GENERAL_SETTING b' , 'a.TYPE_FOOD = b.MASTER_CODE', 'INNER');
        $this->db->join('TBL_FOOD_FARM c' , 'a.TYPE_FOOD = c.TYPE_FOOD', 'LEFT');
        $this->db->join('GENERAL_SETTING d' , 'c.SCEDULE_FOOD = d.MASTER_CODE', 'LEFT');
        $this->db->join('TBL_ORDER_HEADER_FOOD e', 'e.ORDER_NO = a.ORDER_NO','INNER');
        $this->db->where('e.STATUS','RCV');
        $this->db->group_by(array("b.DESCR", "b.MASTER_CODE"));
        $this->db->having(('SUM(ORDER_TOTAL) - IFNULL(SUM(TOTAL_FOOD), 0) > 0'));
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_pakan'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_kandang'] = $records;

        if ($id === NULL) {
            
            $data['flow_data'] = [
                'id_food_farm'  => NULL, 
                'food_date'     => $this->input->post('food_date'),
                'type_food'     => $this->input->post('type_food'),
                'tipe_kandang_pakan'  => $this->input->post('tipe_kandang_pakan'),
                'total_weight'        => $this->input->post('total_weight'),
                'total_pakan'         => $this->input->post('total_pakan'),
                'time_food'           => $this->input->post('time_food'),
                'value'               => $this->form_validation->error_array(),
            ];
            $this->data['data'] = $data;
        } else {
            $this->db->select('a.ID_FOOD_FARM, a.SCEDULE_FOOD, a.TYPE_FOOD, a.TYPE_CAGE, a.FOOD_DATE, a.TOTAL_WEIGHT, a.TOTAL_FOOD,a.CREATE_DATE, a.UPDATE_DATE, a.USR_CRT, a.USR_UPD');
            $this->db->from('TBL_FOOD_FARM a');
            $this->db->where('a.ID_FOOD_FARM', $id);
            $query = $this->db->get();

            $data['flow_data'] = [
                'id_food_farm'  => $query->row('ID_FOOD_FARM'),
                'food_date'     => date("d/m/Y", strtotime($query->row('FOOD_DATE'))),
                'type_food'     => $query->row('TYPE_FOOD'),
                'tipe_kandang_pakan'  => $query->row('TYPE_CAGE'),
                'total_weight'        => $query->row('TOTAL_WEIGHT'),
                'total_pakan'         => $query->row('TOTAL_FOOD'),
                'time_food'           => $query->row('SCEDULE_FOOD'),
                'value'               => $this->form_validation->error_array(),
            ];
                    
            $this->data['data'] = $data;
        }
        $this->set_breadcrump('Pemberian Pakan' , 'Input data');
        $this->page = "pemberian_pakan/form";
        $this->layout();
    }

    public function getTotalWeight(){

        $this->db->select('A.TYPE_CAGE ,gs.DESCR ,SUM(B.WEIGHT)AS BERAT_CURRENT');
        $this->db->from('TBL_FARM A ');
        $this->db->join('TBL_DAILY_WEIGHT B', 'A.REGISTER_NO  = B.REGISTER_NO','INNER'); 
        $this->db->join('GENERAL_SETTING gs', 'gs.MASTER_CODE  = A.TYPE_CAGE','INNER'); 
        $this->db->where('A.TYPE_CAGE', $this->input->post('searchTerm'));
        $this->db->where('A.STATUS', STATUS_ACTIVE);
        $this->db->group_by(array('A.TYPE_CAGE', 'gs.DESCR'));
        $query = $this->db->get();


        echo json_encode($query->row('BERAT_CURRENT'));
    }

    public function save() {
        $this->form_validation
        ->set_rules('food_date', "Tanggal Pemberian Pakan", 'trim|required')
        ->set_rules('type_food', "Jenis Pakan", 'trim|required')
        ->set_rules('tipe_kandang_pakan', "Tipe Kandang", 'trim|required')
        ->set_rules('total_weight', "Berat Badan", ['trim','decimal','required'])
        ->set_rules('total_pakan', "Total pakan", ['trim','decimal','required'])
        ->set_rules('time_food', "Jadwal Pemberian Pakan", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }

        try {
            $data = array(
                'SCEDULE_FOOD'  => $this->input->post('time_food'),
                'TYPE_FOOD'     => $this->input->post('type_food'),
                'TYPE_CAGE'     => $this->input->post('tipe_kandang_pakan'),
                'FOOD_DATE'     => date("Y-m-d", strtotime($this->input->post('food_date'))),
                'TOTAL_WEIGHT'  => $this->input->post('total_weight'),
                'TOTAL_FOOD'    => $this->input->post('total_pakan'),
                'CREATE_DATE'   => date('Y-m-d H:i:s'),
                'USR_CRT'       => $this->session->userdata('iduser'),
            );
            $this->db->insert('TBL_FOOD_FARM', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('pemberian_pakan/lists');
    }

    public function update($id) {
        $this->form_validation
        ->set_rules('food_date', "Tanggal Pemberian Pakan", 'trim|required')
        ->set_rules('type_food', "Jenis Pakan", 'trim|required')
        ->set_rules('tipe_kandang_pakan', "Tipe Kandang", 'trim|required')
        ->set_rules('total_weight', "Berat Badan", ['trim','decimal','required'])
        ->set_rules('total_pakan', "Total pakan", ['trim','decimal','required'])
        ->set_rules('time_food', "Jadwal Pemberian Pakan", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form($id);
            return;
        }

        try {
            $data = array(
                'SCEDULE_FOOD'  => $this->input->post('time_food'),
                'TYPE_FOOD'     => $this->input->post('type_food'),
                'TYPE_CAGE'     => $this->input->post('tipe_kandang_pakan'),
                'FOOD_DATE'     => date("Y-m-d", strtotime($this->input->post('food_date'))),
                'TOTAL_WEIGHT'  => $this->input->post('total_weight'),
                'TOTAL_FOOD'    => $this->input->post('total_pakan'),
                'UPDATE_DATE'   => date('Y-m-d H:i:s'),
                'USR_UPD'       => $this->session->userdata('iduser'),
            );

            $this->db->where('ID_FOOD_FARM', $id);
            $this->db->update('TBL_FOOD_FARM', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form($id);
        }
        redirect('pemberian_pakan/lists');
    }

    public function search(){
        $data = [
            'pemberian_pakan' => NULL,
            'jadwal_pakan'    => NULL,
            'jenis_pakan'     => NULL,  
            'tipe_kandang'    => NULL
        ];
        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'SCHEDULE_FOODING');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jadwal_pakan'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_pakan'] = $records;

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_CAGE');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_kandang'] = $records;
        $this->data['data'] = $data;

        // if ($this->input->post("jadwal_pakan")!== NULL OR $this->input->post("jadwal_pakan")!== '-' ) {
        //     $search1 = $this->input->post("jadwal_pakan");
        // }
        
        // if ($this->input->post("jenis_pakan")!== NULL OR $this->input->post("jenis_pakan")!== '-' ) {
        //     $search2 = $this->input->post("jenis_pakan");
        // }

        // if ($this->input->post("tipe_kandang")!== NULL OR $this->input->post("tipe_kandang")!== '-' ) {
        //     $search3 = $this->input->post("tipe_kandang");
        // }
        
        $search1 = ($this->input->post("jadwal_pakan"))? $this->input->post("jadwal_pakan") : NULL;
        $search2 = ($this->input->post("jenis_pakan"))? $this->input->post("jenis_pakan") : NULL;
        $search3 = ($this->input->post("tipe_kandang"))? $this->input->post("tipe_kandang") : NULL;

        if ($search1 !== NULL) {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.SCEDULE_FOOD like '%".$search1."%'",URL_PEMBERIAN_PAKAN);            
        } else {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.SCEDULE_FOOD like '% NULL %'",URL_PEMBERIAN_PAKAN);            
        }

        if ($search2 !== NULL) {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.TYPE_FOOD like '%".$search2."%'",URL_PEMBERIAN_PAKAN);            
        } else {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.TYPE_FOOD like '% NULL %'",URL_PEMBERIAN_PAKAN);            
        }

        if ($search3 !== NULL) {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.TYPE_CAGE like '%".$search3."%'",URL_PEMBERIAN_PAKAN);            
        } else {
            $data['pemberian_pakan'] = $this->get_paging(PEMBERIAN_PAKAN_SEACRH." a.TYPE_CAGE like '% NULL %'",URL_PEMBERIAN_PAKAN);            
        }

        $this->data['data'] = $data;
        $this->set_breadcrump('Pemberian Pakan' , 'List Data');
        $this->page = "pemberian_pakan/index";
        $this->layout();

    }
}
