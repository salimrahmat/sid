<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists() {
        $data=[
            'user_list'  => NULL,
            'akses_user' => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'USER_ACCESS');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['akses_user'] = $records;

        $query=USER_LIST.' WHERE B.IS_ACTIVE = 1';
        $data['user_list'] = $this->get_paging($query,URL_USER_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('User' , 'List Data');
        $this->page = "user/index";
        $this->layout();

    }

    public function form($id = NULL) {
        $data = [
            'akses_user' => NULL,
            'flow_data'  => [
               'user_id'    => NULL, 
               'user_name'  => NULL,
               'password'   => NULL,
               'access'     => NULL,
               'is_active'  => NULL,
               'value'      => NULL,
            ]
       ];

       $this->db->select('DESCR, MASTER_CODE');
       $this->db->from('GENERAL_SETTING');
       $this->db->where('GENERAL_SETTING_CODE', 'USER_ACCESS');
       $query = $this->db->get();

       $records = [];
       foreach ($query->result() as $row)
       {
           $records[] = [
               'descr' => $row->DESCR,
               'master_code' => $row->MASTER_CODE
           ];
               
       }
       $data['akses_user'] = $records;

       if ($id === NULL) {
            $data['flow_data'] = [
                'user_id'    => NULL, 
                'user_name'  => $this->input->post('user_name'),
                'password'   => $this->input->post('password'),
                'access'     => $this->input->post('access'),
                'is_active'  => $this->input->post('is_active'),
                'value'      => $this->form_validation->error_array(),
            ];
        } else {
            $this->db->select('A.*, B.DESCR');
            $this->db->from('TBL_USER A');
            $this->db->join('GENERAL_SETTING B', 'A.USER_ACCESS = B.MASTER_CODE', 'INNER');
            $this->db->where('A.ID_USER', $id);
            $query = $this->db->get();

            $data['flow_data'] = [
                'user_id'    => $query->row('ID_USER'), 
                'user_name'  => $query->row('USERNAME'),
                'password'   => $query->row('PASSWORD'),
                'access'     => $query->row('USER_ACCESS'),
                'is_active'  => $query->row('IS_ACTIVE'),
                'value'      => $this->form_validation->error_array(),
            ];

        }

        $this->data['data'] = $data;
        $this->set_breadcrump('User' , 'Input user');
        $this->page = "user/form";
        $this->layout();

    }

    public function save() {
        $this->form_validation
        ->set_rules('user_name', "Username", 'trim|required|valid_email')
        ->set_rules('password', "Password", 'trim|required')
        ->set_rules('access', "Akses", 'trim|required')
        ->set_rules('is_active', "Status", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        } 

        try {
           
            $data = array(
                'USERNAME'      => $this->input->post('user_name'),
                'PASSWORD'      => md5($this->input->post('password')),
                'USER_ACCESS'   => $this->input->post('access'),
                'IS_ACTIVE'     => $this->input->post('is_active'),
                'DTM_CRT'       => date('Y-m-d H:i:s'),
            );

            $this->db->insert('TBL_USER', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('user/lists');
    }

    public function update($id){
        $this->form_validation
        ->set_rules('user_name', "Username", 'trim|required|valid_email')
        ->set_rules('password', "Password", 'trim|required')
        ->set_rules('access', "Akses", 'trim|required')
        ->set_rules('is_active', "Status", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }

        try{
             
            $data = array(
                'USERNAME'      => $this->input->post('user_name'),
                'PASSWORD'      => md5($this->input->post('password')),
                'USER_ACCESS'   => $this->input->post('access'),
                'IS_ACTIVE'     => $this->input->post('is_active'),
            );

            $this->db->where('ID_USER', $id);
            $this->db->update('TBL_USER', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        redirect('user/lists');
    }

}