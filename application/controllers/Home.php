<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    /**
     * Home Aplication.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function index(){

        $data = [
            'order'         => [],
            'order_cancel'  => [],
            'member'        => [],
            'chart'         => [],
        ];

        $this->db->select('count(*) AS total');
        $this->db->from('TBL_SALE_HEADER');
        $this->db->where('CONFIRM_SALE', 'Y');
        $query = $this->db->get();

        $data['order'] = [
            'total' => $query->row('total')
        ];

        $this->db->select('count(*) AS total');
        $this->db->from('TBL_SALE_HEADER');
        $this->db->where('CONFIRM_SALE', 'C');
        $query = $this->db->get();

        $data['order_cancel'] = [
            'total' => $query->row('total')
        ];

        $this->db->select('count(*) AS total');
        $this->db->from('TBL_MEMBER');
        $query = $this->db->get();

        $data['member'] = [
            'total' => $query->row('total')
        ];

        $query='SELECT A.TOTAL,A.DESCR,A.PERIODE,@row_number:=CASE WHEN @periode = A.PERIODE THEN @row_number ELSE @row_number + 1 END AS num,@periode:=A.PERIODE
        FROM (
        SELECT COUNT(*) TOTAL,B.DESCR,DATE_FORMAT(A.OUTSIDE_DATE, "%M") AS PERIODE
          FROM TBL_SALE_FARM A 
               INNER JOIN GENERAL_SETTING B ON A.TYPE_SALE = B.MASTER_CODE
          WHERE A.CONFIRM_SALE='."'Y'".' AND 
                A.OUTSIDE_DATE BETWEEN DATE_FORMAT(NOW() ,'."'%Y-01-01'".') AND  DATE_FORMAT(NOW() ,'."'%Y-%m-%d'".')
        GROUP BY B.DESCR,A.OUTSIDE_DATE
        )A, (SELECT @row_number := 0) r, (SELECT @periode := 0) x GROUP BY A.DESCR,A.PERIODE,A.TOTAL';

        $result = $this->db->query($query)->result();

        $records   = [];        
        foreach ($result as $row){
            $color = $this->rand_color();
            $datatotal = [];
            for ($x = 1; $x <= count($result); $x++) {
                 if ($row->num == $x){
                    $datatotal[]=[$row->TOTAL];
                 } else {
                    $datatotal[]=[0];
                 }
            }
            $records[] = [
                'label'     => $row->DESCR,
                'periode'   => $row->PERIODE,
                'total'     => $row->TOTAL,
                'data'      => $datatotal,
                'pointColor' =>$color,
                'fillColor'  =>$color
            ];
        }
        $data['chart'] = $records;
        $this->data['data'] = $data;
        $this->set_breadcrump('Dashboard' , 'Control panel');
        $this->page="index";
        $this->layout();
    }

    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    function chart(){
        $query='SELECT A.TOTAL,A.DESCR,A.PERIODE,@row_number:=CASE WHEN @periode = A.PERIODE THEN @row_number ELSE @row_number + 1 END AS num,@periode:=A.PERIODE
        FROM (
        SELECT COUNT(*) TOTAL,B.DESCR,DATE_FORMAT(A.OUTSIDE_DATE, "%M") AS PERIODE
          FROM TBL_SALE_FARM A 
               INNER JOIN GENERAL_SETTING B ON A.TYPE_SALE = B.MASTER_CODE
          WHERE A.CONFIRM_SALE='."'Y'".' AND 
                A.OUTSIDE_DATE BETWEEN DATE_FORMAT(NOW() ,'."'%Y-01-01'".') AND  DATE_FORMAT(NOW() ,'."'%Y-%m-%d'".')
        GROUP BY B.DESCR,A.OUTSIDE_DATE
        )A, (SELECT @row_number := 0) r, (SELECT @periode := 0) x GROUP BY A.DESCR,A.PERIODE,A.TOTAL';

        $result = $this->db->query($query)->result();

        $records   = [];        
        foreach ($result as $row){
            $color = $this->rand_color();
            $datatotal = [];
            for ($x = 1; $x <= count($result); $x++) {
                 if ($row->num == $x){
                    $datatotal[]=[$row->TOTAL];
                 } else {
                    $datatotal[]=[0];
                 }
            }
            $records[] = [
                'label'     => $row->DESCR,
                'periode'   => $row->PERIODE,
                'total'     => $row->TOTAL,
                'data'      => $datatotal,
                'pointColor' =>$color,
                'fillColor'  =>$color
            ];
        }

        var_dump($records);
    }

    public function moreOrder(){
        $data['list_yes'] = $this->get_paging(ORDER_LIST_YES,URL_ORDER_YES);
        $this->data['data'] = $data;
        $this->set_breadcrump('Order ' , 'List Data');
        $this->page = "home/orders";
        $this->layout();
    }

    public function Order_Cancel(){
        $data['list_cancel'] = $this->get_paging(ORDER_LIST_CANCEL,URL_ORDER_CANCEL);
        $this->data['data'] = $data;
        $this->set_breadcrump('Order Cancel' , 'List Data');
        $this->page = "home/cancel";
        $this->layout();
    }


    public function detail($order_no){
        $data=[
            'header'=>[],
            'details'=>[],
        ];

        $this->db->select('A.TAX, A.CONFIRM_SALE, DATE_FORMAT(A.OUT_DATE, "%d-%m-%Y") AS OUT_DATE, B.MEMBER_NAME, B.MEMBER_ALAMAT, B.MEMBER_HP');
        $this->db->from('TBL_SALE_HEADER A');
        $this->db->join('TBL_MEMBER B','B.MEMBER_ID = A.MEMBER_ID','INNER');
        $this->db->where('A.ORDER_NO' , $order_no);
        $query = $this->db->get();
        $data['header'] = [
            'confirm'       =>$query->row('CONFIRM_SALE'),
            'outdate'       =>$query->row('OUT_DATE'),
            'member_name'   =>$query->row('MEMBER_NAME'),
            'member_alamat' =>$query->row('MEMBER_ALAMAT'),
            'member_hp'     =>$query->row('MEMBER_HP'),
            'tax'           =>$query->row('TAX'),
        ];

        $this->db->select('D.REGISTER_NO, DATE_FORMAT(D.REGISTER_DATE, "%d-%m-%Y") AS REGISTER_DATE, E.DESCR AS TYPE_TERNAK,
                          F.DESCR AS JENIS_TERNAK,A.CONFIRM_SALE ,A.TAX, B.REGISTER_NO,C.DESCR AS JENIS_JUAL, 
                          B.WEIGHT_CAGE, B.WEIGHT_CUSTOMER, B.TOTAL_JOGROG, B.TOTAL_NON_JOGROG,B.DISCOUNT');
        $this->db->from('TBL_SALE_HEADER A');
        $this->db->join('TBL_SALE_FARM B','B.ORDER_NO = A.ORDER_NO','INNER');
        $this->db->join('GENERAL_SETTING C','C.MASTER_CODE = B.TYPE_SALE','INNER');
        $this->db->join('TBL_FARM D','D.REGISTER_NO = B.REGISTER_NO','INNER');
        $this->db->join('GENERAL_SETTING E','E.MASTER_CODE = D.TYPE_LIVESTOCK','INNER');
        $this->db->join('GENERAL_SETTING F','F.MASTER_CODE = D.TYPE_FARM','INNER');
        $this->db->where('A.ORDER_NO' , $order_no);
        $query = $this->db->get();
        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'registerno'    => $row->REGISTER_NO,
                'registerdate'  => $row->REGISTER_DATE,
                'type_ternak'   => $row->TYPE_TERNAK,
                'jenis_ternak'  => $row->JENIS_TERNAK,
                'tax'           => $row->TAX,
                'jenis'         => $row->JENIS_JUAL,
                'berat'         => $row->WEIGHT_CAGE,
                'berat_konsumen'=> $row->WEIGHT_CUSTOMER,
                'jogrog'        => $row->TOTAL_JOGROG,
                'non_jogrog'    => $row->TOTAL_NON_JOGROG,
                'discount'      => $row->DISCOUNT,
            ];
        }

        $data['details'] = $records;

        $this->data['data'] = $data;
        $this->set_breadcrump('Detail' , 'Detail Order');
        $this->page = "home/detail";
        $this->layout();
    }
}

