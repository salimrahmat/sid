<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_pakan extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    /**
     * 
     */
    public function lists() {
        $data=[
            'order_pakan' => NULL,
            'tipe_pakan'  => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_pakan']  = $records;
        $data['order_pakan'] = $this->get_paging(ORDER_PAKAN_LIST,URL_ORDER_PAKAN);
        $this->data['data']  = $data;
        $this->set_breadcrump('Order Pakan' , 'List Data');
        $this->page = "order_pakan/index";
        $this->layout();
    }

    public function listPo() {
        $data=[
            'order_pakan_po' => NULL,
        ];

        $data['order_pakan_po'] = $this->get_paging(ORDER_PAKAN_PO_LIST,URL_PAKAN_PO_LIST);
        $this->data['data']  = $data;
        $this->set_breadcrump('Print PO' , 'List Data Vendor');
        $this->page = "order_pakan/list_po";
        $this->layout();
    }
    /**
     * Form Order Pakan
     * @param integer
     */
    public function form($id = NULL){
        $data = [
             'tipe_pakan' => NULL,
             'price_food' => NULL,
             'id_vendor'  => NULL,   
             'flow_data'        => [
                'id_stock_food' => NULL, 
                'order_date'    => NULL,
                'arrival_date'  => NULL,
                'id_vendor'     => NULL,
                'alamat_vendor' => NULL,
                'phone_vendor'  => NULL,
                'pic_vendor'    => NULL,
                'tax'           => NULL,
                'tax_value'     => NULL,
                'value'         => NULL,
             ],
             'order' => [],
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
        $this->db->where('IS_ACTIVE', 1);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_pakan'] = $records;


        $this->db->select('ID_VENDOR, VENDOR_NAME');
        $this->db->from('TBL_VENDOR');
        $this->db->where('IS_ACTIVE', 1);
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'descr' => $row->VENDOR_NAME,
                'master_code' => $row->ID_VENDOR
            ];
                
        }
        $data['id_vendor'] = $records;
        

        if ($id === NULL) {
            $data['flow_data'] = [
                   'id_stock_food' => NULL, 
                   'order_date'    => $this->input->post('order_date'),
                   'arrival_date'  => $this->input->post('arrival_date'),
                   'id_vendor'     => $this->input->post('id_vendor'),
                   'tax'           => $this->input->post('tax'),
                   'tax_value'     => $this->input->post('tax_value'),
                   'alamat_vendor' => NULL,
                   'phone_vendor'  => NULL,
                   'pic_vendor'    => NULL,
                   'value'         => $this->form_validation->error_array(),
            ];

            if ($this->input->post('jenis_pakan') !== NULL) {
                $examCount = count($this->input->post('jenis_pakan'));
                for ($examIdx = 0; $examIdx < $examCount; $examIdx++) {
                     $orderall = [
                          'tipe_pakan'    => $this->input->post('jenis_pakan')[$examIdx ],
                          'price_food'    => $this->input->post('price_food')[$examIdx ],
                          'total_order'   => $this->input->post('total_order')[$examIdx ],
                          'total'         => $this->input->post('total')[$examIdx ],
                          'biaya_lain'    => $this->input->post('biaya_lain')[$examIdx ],
                     ];
                    $data['order'][] = $orderall;
                };
            }


            $this->data['data'] = $data;
        } else {
            $this->db->select('A.ID_STOCK_FOOD, A.TYPE_FOOD, A.ORDER_DATE, A.ORDER_TOTAL, A.ARRIVAL_DATE, A.CREATE_DATE, A.UPDATE_DATE, A.USR_CRT, A.USR_UPD, A.PRICE_FOOD, A.PRICE_KG, A.TOTAL, A.BIAYA_LAIN, A.ID_VENDOR, B.VENDOR_ADDRESS, B.VENDOR_PHONE, B.VENDOR_PIC, A.TAX, A.TAX_VALUE, C.DESCR AS NAMA_PAKAN');
            $this->db->from('TBL_ORDER_FOOD A');
            $this->db->join('TBL_VENDOR B','A.ID_VENDOR = B.ID_VENDOR','INNER');
            $this->db->join('GENERAL_SETTING C','C.MASTER_CODE = A.TYPE_FOOD','INNER');
            $this->db->where('A.ID_STOCK_FOOD', $id);
            $query = $this->db->get();

            $data['flow_data'] = [
                'id_stock_food' => $query->row('ID_STOCK_FOOD'), 
                'order_date'    => date("d/m/Y", strtotime($query->row('ORDER_DATE'))),
                'arrival_date'  => date("d/m/Y", strtotime($query->row('ARRIVAL_DATE'))),
                'value'         => $this->form_validation->error_array(),
                'tax'           => $query->row('TAX'),
                'tax_value'     => $query->row('TAX_VALUE'),
                'alamat_vendor' => $query->row('VENDOR_ADDRESS'),
                'phone_vendor'  => $query->row('VENDOR_PHONE'),
                'pic_vendor'    => $query->row('VENDOR_PIC'),
                'id_vendor'     => $query->row('ID_VENDOR'),

            ];   
            $orderall = [
                'tipe_pakan'    => $query->row('TYPE_FOOD'),
                'price_food'    => $query->row('PRICE_FOOD'),
                'total_order'   => $query->row('ORDER_TOTAL'),
                'total'         => $query->row('TOTAL'),
                'biaya_lain'    => $query->row('BIAYA_LAIN'),
                'nama_pakan'    => $query->row('NAMA_PAKAN'),
            ];
            $data['order'][] = $orderall;
            $this->data['data'] = $data;
        }

        $this->set_breadcrump('Order Pakan' , 'Input data');
        $this->page = "order_pakan/form";
        $this->layout();

    }

    public function save() {
        $this->form_validation
        ->set_rules('jenis_pakan[]', "Jenis Pakan", 'trim|required')
        ->set_rules('order_date', "Tanggal Order", 'trim|required')
        ->set_rules('total_order[]', "Jumlah Order", 'trim|required')
        ->set_rules('arrival_date', "Tanggal Kedatangan", 'trim|required')
        ->set_rules('price_food[]', "Harga Perkilo", 'trim|required')
        ->set_rules('total[]', "Total", 'trim|required')
        ->set_rules('biaya_lain[]', "Biaya Lain", 'trim|required')
        ->set_rules('id_vendor', "Vendor Name", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }   

        try {
            $header = array(
                'STATUS'    => 'ODR',
                'INVOICE'   => 'NULL', 
                'USR_CRT'   => $this->session->userdata('iduser'),
                'DTM_CRT'   => date('Y-m-d H:i:s')
            );
            $this->db->insert('TBL_ORDER_HEADER_FOOD', $header);

            $this->db->select('ORDER_NO');
            $this->db->from('TBL_ORDER_HEADER_FOOD');
            $this->db->order_by("ORDER_NO", "DESC");
            $this->db->limit(1);
            $query = $this->db->get();
            
            for ($sessionIdx=0; $sessionIdx<count($this->input->post('jenis_pakan')); $sessionIdx++) {
                $data = array(
                    'TYPE_FOOD'   => $this->input->post('jenis_pakan')[$sessionIdx],
                    'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date'))),
                    'ORDER_TOTAL' => $this->input->post('total_order')[$sessionIdx],
                    'ARRIVAL_DATE'=> date("Y-m-d", strtotime($this->input->post('arrival_date'))),
                    'CREATE_DATE' => date('Y-m-d H:i:s'),
                    'USR_CRT'     => $this->session->userdata('iduser'),
                    'TOTAL'       => $this->input->post('total')[$sessionIdx],
                    'PRICE_KG'    => $this->input->post('price_food')[$sessionIdx],
                    'BIAYA_LAIN'  => $this->input->post('biaya_lain')[$sessionIdx],
                    'PRICE_FOOD'  => $this->input->post('price_food')[$sessionIdx],
                    'ID_VENDOR'   => $this->input->post('id_vendor'),
                    'TAX'         => $this->input->post('tax'),
                    'TAX_VALUE'   => $this->input->post('tax_value'), 
                    'STATUS'      => 'ODR',
                    'PO_NUMBER'   => NULL,
                    'ORDER_NO'    => $query->row('ORDER_NO'),  
                );
                $this->db->insert('TBL_ORDER_FOOD', $data);
            }

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('order_pakan/lists');
    }

    public function update($id) {
        $this->form_validation
        ->set_rules('jenis_pakan[]', "Jenis Pakan", 'trim|required')
        ->set_rules('order_date', "Tanggal Order", 'trim|required')
        ->set_rules('total_order[]', "Jumlah Order", 'trim|required')
        ->set_rules('arrival_date', "Tanggal Kedatangan", 'trim|required')
        ->set_rules('price_food[]', "Harga Perkilo", 'trim|required')
        ->set_rules('total[]', "Total", 'trim|required')
        ->set_rules('biaya_lain[]', "Biaya Lain", 'trim|required')
        ->set_rules('id_vendor', "Vendor Name", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }   

        try {
            for ($sessionIdx=0; $sessionIdx<count($this->input->post('jenis_pakan')); $sessionIdx++) {
                $data = array(
                    'TYPE_FOOD'   => $this->input->post('jenis_pakan')[$sessionIdx],
                    'ORDER_DATE'  => date("Y-m-d", strtotime($this->input->post('order_date'))),
                    'ORDER_TOTAL' => $this->input->post('total_order')[$sessionIdx],
                    'ARRIVAL_DATE'=> date("Y-m-d", strtotime($this->input->post('arrival_date'))),
                    'CREATE_DATE' => date('Y-m-d H:i:s'),
                    'USR_CRT'     => $this->session->userdata('iduser'),
                    'TOTAL'       => $this->input->post('total')[$sessionIdx],
                    'PRICE_KG'    => $this->input->post('price_food')[$sessionIdx],
                    'BIAYA_LAIN'  => $this->input->post('biaya_lain')[$sessionIdx],
                    'PRICE_FOOD'  => $this->input->post('price_food')[$sessionIdx],
                    'ID_VENDOR'   => $this->input->post('id_vendor'),
                    'TAX'         => $this->input->post('tax'),
                    'TAX_VALUE'   => $this->input->post('tax_value'), 
                );
                $this->db->where('ID_STOCK_FOOD', $id);
                $this->db->update('TBL_ORDER_FOOD', $data);
            }


            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('order_pakan/lists');
    }

    public function search(){
        $data=[
            'order_pakan' => NULL,
            'tipe_pakan' => NULL, 
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_pakan'] = $records;



        $search  = ($this->input->post("tipe_pakan"))? $this->input->post("tipe_pakan") : NULL;
        
        if ($search !== NULL) {
            $data['order_pakan'] = $this->get_paging(ORDER_PAKAN_SEARCH." a.TYPE_FOOD like '%".$search."%'",URL_REGISTRASI);            
        } else {
            $data['order_pakan'] = $this->get_paging(ORDER_PAKAN_SEARCH." a.TYPE_FOOD like '% NULL %'",URL_REGISTRASI);            
        }

        $this->data['data'] = $data;
        $this->set_breadcrump('Order Pakan' , 'List Data');
        $this->page = "order_pakan/index";
        $this->layout();
    }

    public function stock() {
        $data=[
            'stock_pakan' => NULL,
        ];

        $data['stock_pakan'] = $this->get_paging(STOCK_FOOD,URL_ORDER_PAKAN);
        $this->data['data']  = $data;
        $this->set_breadcrump('Stock Pakan' , 'List Data');
        $this->page = "order_pakan/stock";
        $this->layout();
    }

    public function detailVendor(){
        $searchTerm = $this->input->post('searchTerm');


        $this->db->select('VENDOR_ADDRESS,VENDOR_PHONE,VENDOR_PIC');
        $this->db->from('TBL_VENDOR');
        $this->db->where('ID_VENDOR', $searchTerm);
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'address'   => $row->VENDOR_ADDRESS,
                'phone'     => $row->VENDOR_PHONE,
                'pic'       => $row->VENDOR_PIC,
            ];
                
        }
        echo json_encode($records);
    }

    public function pdf_po($id_vendor, $order_date,$order_no){

        $data=[
            'header' => NULL,
            'detail' => NULL,   
        ];

        $this->db->select('*');
        $this->db->from('TBL_ORDER_HEADER_FOOD');
        $this->db->where('ORDER_NO',$order_no);
        $this->db->where('INVOICE','NULL');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = array(
                'INVOICE'   => $this->getPoNumber(), 
                'STATUS'    => 'PPO'
            );
            $this->db->where('ORDER_NO', $order_no);
            $this->db->update('TBL_ORDER_HEADER_FOOD', $data);

        } 

        $this->db->select('VENDOR_NAME,VENDOR_ADDRESS,VENDOR_PHONE,VENDOR_PIC');
        $this->db->from('TBL_VENDOR');
        $this->db->where('ID_VENDOR', $id_vendor);
        $query1 = $this->db->get();

        $this->db->select('INVOICE');
        $this->db->from('TBL_ORDER_HEADER_FOOD');
        $this->db->where('ORDER_NO', $order_no);
        $query2 = $this->db->get();

        $data['header'] = [
            'vendor_name' =>$query1->row('VENDOR_NAME'),
            'vendor_address' =>$query1->row('VENDOR_ADDRESS'),
            'vendor_phone' =>$query1->row('VENDOR_PHONE'),
            'vendor_pic'=>$query1->row('VENDOR_PIC'),
            'invoice'   =>$query2->row('INVOICE'),
        ];
        // $this->load->library('Pdf');
        $this->db->select('C.INVOICE, A.ORDER_TOTAL AS QTY, B.DESCR AS TIPE_PAKAN, A.PRICE_FOOD AS HARGA_KG, A.TOTAL AS TOTAL, A.BIAYA_LAIN, A.TAX_VALUE, A.TAX');
        $this->db->from('TBL_ORDER_FOOD A');
        $this->db->join('GENERAL_SETTING B','A.TYPE_FOOD = B.MASTER_CODE','INNER');
        $this->db->join('TBL_ORDER_HEADER_FOOD C', 'A.ORDER_NO = C.ORDER_NO','INNER');
        $this->db->where('A.ID_VENDOR', $id_vendor);
        $this->db->where('A.ORDER_DATE', $order_date);
        $this->db->where('C.ORDER_NO', $order_no);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'qty'       => $row->QTY,
                'pakan'     => $row->TIPE_PAKAN,
                'hargakg'   => $row->HARGA_KG,
                'total'     => $row->TOTAL,
                'biaya'     => $row->BIAYA_LAIN,
                'tax'       => $row->TAX,
                'tax_value'  => $row->TAX_VALUE,
            ];
                
        }
        $data['detail']  = $records;

        $html = $this->load->view('order_pakan/pdf_po',$data);
        // $this->pdf->createPDF($html, 'mypdf', false);

    }

    public function getSelectTax() {
        
        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TAX');
        $this->db->where('IS_ACTIVE', 1);
        $query = $this->db->get();

        echo json_encode(floatval( $query->row('DESCR') ));
    }

    public function receive() {

        $result=[
            'status'=>FALSE,
        ];

        $data = array(
            'STATUS' => 'RCV',
            'ACTUAL_DATE' => date("Y-m-d", strtotime($this->input->post('actual_date')))    
        );
        $this->db->where('ORDER_NO', $this->input->post('order_no'));
        $this->db->update('TBL_ORDER_HEADER_FOOD', $data);
        
        // redirect('order_pakan/lists');

        echo json_encode($result['status']=TRUE);
    }

    public function detailReceive(){
        $data =[
            'header'=>[],
            'detail'=>[],
        ];

        $this->db->select('tv.*,gs.DESCR,tof.ORDER_NO');
        $this->db->from('TBL_ORDER_FOOD tof');
        $this->db->join('TBL_VENDOR tv' ,'tof.ID_VENDOR = tv.ID_VENDOR','INNER');
        $this->db->join('TBL_ORDER_HEADER_FOOD tohf' ,'tohf.ORDER_NO  = tof.ORDER_NO','INNER');
        $this->db->join('GENERAL_SETTING gs' ,'gs.MASTER_CODE = tohf.STATUS','INNER');
        $this->db->where('tof.ORDER_NO',$this->input->post('order_no'));
        $this->db->limit('1');
        $query = $this->db->get();
        
        $data['header'] = [
            'vendor_name'  => [
                    'name'  => 'Nama Vendor',
                    'value' =>$query->row('VENDOR_NAME'),
            ],
            'vendor_address' =>[
                    'name'   =>'Alamat',
                    'value'  =>$query->row('VENDOR_ADDRESS'),  
            ],
            'vendor_phone' =>[
                    'name'  =>'Phone',
                    'value' =>$query->row('VENDOR_PHONE'),
            ],
            'vendor_pic' =>[
                    'name'  =>'PIC',
                    'value' =>$query->row('VENDOR_PIC'),
            ],
            'sts' =>[
                    'name' =>'Status',
                    'value'=>$query->row('DESCR'),
            ],
            'order' =>[
                'name' =>'No Order',
                'value'=>$query->row('ORDER_NO'),
            ]
        ];


        $this->db->select('gs.DESCR, tof.ORDER_TOTAL as QTY, tof.PRICE_KG, tof.TOTAL, tof.BIAYA_LAIN ,tof.TAX_VALUE'); 	
        $this->db->from('TBL_ORDER_FOOD tof'); 
        $this->db->join('GENERAL_SETTING gs', 'tof.TYPE_FOOD = gs.MASTER_CODE', 'INNER'); 
        $this->db->where('tof.ORDER_NO',$this->input->post('order_no'));
        $query = $this->db->get();
        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'qty'       => $row->QTY,
                'pakan'     => $row->DESCR,
                'hargakg'   => $row->PRICE_KG,
                'total'     => $row->TOTAL,
                'biaya'     => $row->BIAYA_LAIN,
                'tax_value'  => $row->TAX_VALUE,
            ];
                
        }
        $data['detail']=$records;

        echo json_encode($data);
    }
}
