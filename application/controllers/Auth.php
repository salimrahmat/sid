<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * login auth
     */
    public function auth_login(){
        $data= [
            'error' => NULL,
        ];
        
        $this->form_validation
            ->set_rules('email', "Email", 'trim|required|valid_email|max_length[255]')
            ->set_rules('password', "Password", 'trim|required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $data['error'] = [
                'email'     => $this->input->post('email') ,
                'password'  => $this->input->post('password'),
                'value'     => $this->form_validation->error_array()
            ];

            $this->load->view('login',$data);
            return;
        }    
        
        $this->db->select('A.USERNAME, A.USER_ACCESS, B.DESCR, A.ID_USER');
        $this->db->from('TBL_USER A');
        $this->db->join('GENERAL_SETTING B', 'B.MASTER_CODE = A.USER_ACCESS' ,'INNER');
        $this->db->where('A.IS_ACTIVE', 1);
        $this->db->where('A.USERNAME', $this->input->post('email'));
        $this->db->where('A.PASSWORD', md5($this->input->post('password')));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $this->session_login($query->row('USERNAME'), $query->row('USER_ACCESS'), $query->row('DESCR'), $query->row('ID_USER'));
            redirect('Home');  
        } else {
            $data['error'] = [
                'email'     => $this->input->post('email') ,
                'password'  => $this->input->post('password'),
                'value'     => array('error'=>'Username atau password salah')
            ];

            $this->load->view('login',$data);
        }
            

    }

    public function validate() {
        $data= [
            'error' => [
                'email'     => NULL,
                'value'     => NULL,
            ],
        ];
        $this->form_validation
            ->set_rules('email', "Email", 'trim|required|valid_email|max_length[255]');
        if ($this->form_validation->run() === FALSE)
        {
            $data['error'] = [
                'email'     => $this->input->post('email') ,
                'value'     => $this->form_validation->error_array()
            ];

            $this->load->view('validate',$data);
            return;
        }        
        if ($this->input->post('email') > 0) {
            $this->db->select('A.USERNAME, A.USER_ACCESS, B.DESCR, A.ID_USER');
            $this->db->from('TBL_USER A');
            $this->db->join('GENERAL_SETTING B', 'B.MASTER_CODE = A.USER_ACCESS' ,'INNER');
            $this->db->where('A.IS_ACTIVE', 1);
            $this->db->where('A.USERNAME', $this->input->post('email'));
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $this->session->set_flashdata('user_email', $this->input->post('email'));
                redirect('auth/forgotPassword');
            } else {
                $data['error'] = [
                    'email'     => $this->input->post('email') ,
                    'value'     => array('error'=>'Email tidak terdaftar')
                ];
            }
        }
        $this->load->view('validate',$data);
    }

    public function forgotPassword(){
        $data= [
            'email' => $this->session->flashdata('user_email'),
            'error' => [
                'value' => NULL,
            ],
        ];

        $this->load->view('forgot',$data);
    }

    public function reset(){
        $data['error'] = [
			'email'     => NULL ,
			'password'  => NULL ,
			'value'     => NULL
		];
    
        try {

            $data = array(
                'password' => md5($this->input->post('password')),
            );

            $this->db->where('USERNAME', $this->session->flashdata('user_email'));
            $this->db->update('TBL_USER', $data);
            redirect('auth/auth_login');
        }catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->reset();
        }
		
    }
    /**
     * Logout Session
     */
    public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

}