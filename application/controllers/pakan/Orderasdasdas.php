<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    /**
     * 
     */
    public function lists() {
        $data=[
            'order_pakan' => NULL,
            'tipe_pakan'  => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['tipe_pakan']  = $records;
        $data['order_pakan'] = $this->get_paging(ORDER_PAKAN_LIST,URL_ORDER_PAKAN);
        var_dump($data['order_pakan']);die();
        $this->data['data']  = $data;
        $this->set_breadcrump('List Pakan Order' , 'List Data');
        $this->page = "pakan/order/index";
        $this->layout();
    }
    /**
     * Form Order Pakan
     * @param integer
     */
    public function form($id = NULL){
        $data = [
             'tipe_pakan' => NULL,
        ];
        if ($id === NULL) {
            $this->db->select('DESCR, MASTER_CODE');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
            $query = $this->db->get();
 
            $records = [];
            foreach ($query->result() as $row)
            {
                $records[] = [
                    'descr' => $row->DESCR,
                    'master_code' => $row->MASTER_CODE
                ];
                    
            }
            $data['tipe_pakan'] = $records;
            $this->data['data'] = $data;
        }
        $this->set_breadcrump('Order Pakan' , 'Input data');
        $this->page = "pakan/order/form";
        $this->layout();

    }

}