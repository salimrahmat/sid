<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    /**
     * Form Input
     * @param integer
     */
    public function form($id = NULL){
        $data = [
            'tipe_pakan'  => NULL,
            'proses'      => NULL,  
            'flow_data'   => [
                'jenis_pakan' => NULL,
                'proses'      => NULL,
                'proses_date' => NULL,
                'amount'      => NULL,  
                'value'       => NULL
            ]
        ];
       if ($id === NULL) {
           $this->db->select('DESCR, MASTER_CODE');
           $this->db->from('GENERAL_SETTING');
           $this->db->where('GENERAL_SETTING_CODE', 'TYPE_FOOD');
           $query = $this->db->get();

           $records = [];
           foreach ($query->result() as $row)
           {
               $records[] = [
                   'descr' => $row->DESCR,
                   'master_code' => $row->MASTER_CODE
               ];
                   
           }
           $data['tipe_pakan'] = $records;

           $this->db->select('DESCR, MASTER_CODE');
           $this->db->from('GENERAL_SETTING');
           $this->db->where('GENERAL_SETTING_CODE', 'PROSES_FOOD');
           $query1 = $this->db->get();

           $records = [];
           foreach ($query1->result() as $row)
           {
               $records[] = [
                   'descr' => $row->DESCR,
                   'master_code' => $row->MASTER_CODE
               ];
                   
           }
           $data['proses'] = $records;

           $data['flow_data'] = [
                'jenis_pakan' => $this->input->post('jenis_pakan') ,
                'proses_pakan'=> $this->input->post('proses'),
                'proses_date' => $this->input->post('proses_date'),
                'amount'      => $this->input->post('amount'),  
                'value'       => $this->form_validation->error_array()                                  
            ];

           $this->data['data'] = $data;
       }
       $this->set_breadcrump('Stock Pakan' , 'Input data');
       $this->page = "pakan/stock/form";
       $this->layout();
    }

    public function save() {

        $data = [
            'tipe_pakan'  => NULL,
            'proses'      => NULL,  
            'error'       => [
                'jenis_pakan' => NULL ,
                'proses'      => NULL,
                'proses_date' => NULL,
                'amount'      => NULL,  
                'value'       => NULL
            ]
        ];
        
        $this->form_validation
        ->set_rules('jenis_pakan', "Email", 'trim|required|max_length[50]')
        ->set_rules('proses', "Proses", 'trim|required|max_length[50]')
        ->set_rules('proses_date', "Tanggal Proses", 'trim|required')
        ->set_rules('amount', "Jumlah", 'required');

        if ($this->form_validation->run() === FALSE)
        { 
            $this->form();
            return;
        }   



    }
}