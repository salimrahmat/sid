<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege extends MY_Controller {
    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    /**
     * @throws APIErrorException
     */
    public function lists() {
        $data=[
            'access_user' => NULL, 
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'USER_ACCESS');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['access_user'] = $records;
        $query=USER_ACCESS_LIST.' ORDER BY A.ID_USER_ACCESS ASC';
        $data['detail'] = $this->get_paging($query,URL_PRIVILAGE);
        $this->data['data'] = $data;
        $this->set_breadcrump('Privilage User' , 'List Data');
        $this->page = "privilege/index";
        $this->layout();
    }

    public function search(){
        $data=[
            'access_user' => NULL, 
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'USER_ACCESS');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['access_user'] = $records;
        $search  = ($this->input->post("table_search"))? $this->input->post("table_search") : NULL;
        $search1 = ($this->input->post("access_user"))? $this->input->post("access_user") : NULL;

        if ($search !== NULL) {
            $data['detail'] = $this->get_paging(USER_ACCESS_LIST." WHERE A.MENU_NAME like '%".$search."%'",URL_PRIVILAGE);              
        }

        if ($search1 !== NULL) {
            $data['detail'] = $this->get_paging(USER_ACCESS_LIST." WHERE A.ID_USER_ACCESS = '".$search1."'",URL_PRIVILAGE);  
        }   

        $this->data['data'] = $data;     
        $this->set_breadcrump('Privilage User' , 'List Data');
        $this->page = "privilege/index";
        $this->layout();
    }

    public function form($id){
        $this->db->select('B.DESCR,A.MENU_NAME,A.MENU_LIST,A.MENU_ADD,A.MENU_EDIT,A.MENU_DELETE,A.MENU_DETAIL');
        $this->db->from('TBL_ACCESS_MENU A');
        $this->db->join('GENERAL_SETTING B','A.ID_USER_ACCESS = B.MASTER_CODE','INNER');
        $this->db->where('A.ID_ACCESS_MENU',$id);
        $query = $this->db->get();
        $data['detail'] = [
            'id'    => $id,
            'descr' => $query->row('DESCR'),
            'nama'  => $query->row('MENU_NAME'),
            'list'  => $query->row('MENU_LIST'),
            'add'   => $query->row('MENU_ADD'),  
            'edit'    => $query->row('MENU_EDIT'),  
            'delete'  => $query->row('MENU_DELETE'),  
            'detail'  => $query->row('MENU_DETAIL'),  
        ];
        $this->data['data'] = $data;
        $this->set_breadcrump('Privilage' , 'Edit data');
        $this->page = "privilege/form";
        $this->layout();
    }

    public function update($id){
        try {
            $data = array(
                'MENU_LIST'   => $this->input->post('list'),
                'MENU_ADD'    => $this->input->post('add'),
                'MENU_EDIT'   => $this->input->post('edit'),
                'MENU_DELETE' => $this->input->post('delete'),
                'MENU_DETAIL' => $this->input->post('detail'),
            );
            $this->db->where('ID_ACCESS_MENU', $id);
            $this->db->update('TBL_ACCESS_MENU', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
            
        } catch (Exception $e) {
            log_message('error ',$e->getMessage());
        }
        redirect('privilege/lists'); 
    }
}