<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_domba extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists(){
        $query=PENJUALAN_LIST.' WHERE A.STATUS ='.STATUS_ACTIVE.' ORDER BY A.REGISTER_NO DESC';
        $data['penjualan_list'] = $this->get_paging($query,URL_PENJUALAN_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('Katalog' , 'List Data Farm');
        $this->page = "penjualan_domba/index";
        $this->layout();
    }

    public function search(){
        
        $search=$this->input->post('table_search');
        $query=PENJUALAN_LIST.' WHERE A.STATUS ='.STATUS_ACTIVE;
        $data['penjualan_list'] = $this->get_paging($query." AND A.REGISTER_NO like '%".$search."%'",URL_PENJUALAN_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('Katalog' , 'List Data Farm');
        $this->page = "penjualan_domba/index";
        $this->layout();
    }

    public function detail($regNo){
        $data = [
            'detail'        => NULL,
            'jenis_jual'    => NULL,
        ];

        $this->db->select('DESCR, MASTER_CODE');
        $this->db->from('GENERAL_SETTING');
        $this->db->where('GENERAL_SETTING_CODE', 'TYPE_SALE');
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'descr' => $row->DESCR,
                'master_code' => $row->MASTER_CODE
            ];
                
        }
        $data['jenis_jual'] = $records;
    
        $this->db->select('A.REGISTER_NO,A.REGISTER_DATE,C.DESCR AS TYPE_TERNAK,B.DESCR AS JENIS_TERNAK,A.WEIGHT_IN,D.DESCR AS TIPE_KANDANG,A.ROOM_NUMBER');
        $this->db->from('TBL_FARM A');
        $this->db->join('GENERAL_SETTING B','A.TYPE_FARM = B.MASTER_CODE','INNER');
        $this->db->join('GENERAL_SETTING C','A.TYPE_LIVESTOCK = C.MASTER_CODE','INNER');
        $this->db->join('GENERAL_SETTING D','A.TYPE_CAGE = D.MASTER_CODE','INNER');
        $this->db->where("A.STATUS =".STATUS_ACTIVE);
        $this->db->where("A.REGISTER_NO ='".$regNo."'");
        $query = $this->db->get();

        $data['detail'] = [
            'register_no'   =>$query->row('REGISTER_NO'),
            'register_date' =>date("d-m-Y", strtotime($query->row('REGISTER_DATE'))),
            'tipe_ternak'   =>$query->row('TYPE_TERNAK'),
            'jenis_ternak'  =>$query->row('JENIS_TERNAK'),
            'berat_masuk'   =>$query->row('WEIGHT_IN'),
            'tipe_kandang'  =>$query->row('TIPE_KANDANG'),
            'nomor_kamar'   =>$query->row('ROOM_NUMBER'),
        ];
        $this->data['data'] = $data;
        $this->set_breadcrump('Detail' , 'Detail Farm');
        $this->page = "penjualan_domba/detail";
        $this->layout();

    }

    public function cart(){
        $data = [
            'items'  =>[],
            'member' =>[],
        ];

        $this->db->select('MEMBER_ID, MEMBER_NAME');
        $this->db->from('TBL_MEMBER');;
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'member_id'     => $row->MEMBER_ID,
                'member_name'   => $row->MEMBER_NAME
            ];
                
        }
        $data['member'] = $records;
        if ($this->input->post('register_no') !== NULL) {
            $this->db->select('DESCR');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('MASTER_CODE', $this->input->post('jenis_jual'));
            $query = $this->db->get();

            $items = array(
                'register_no'       => $this->input->post('register_no'), 
                'tanggal_register'  => $this->input->post('register_date'),
                'tipe_ternak'       => $this->input->post('tipe_ternak'), 
                'jenis_ternak'      => $this->input->post('jenis_ternak'),
                'berat_masuk'       => $this->input->post('berat_masuk'), 
                'berat_customer'    => $this->input->post('weight_cus'), 
                'jenis_jual'        => $this->input->post('jenis_jual'), 
                'jenis_jual_desc'   => $query->row('DESCR'),
                'price_kg'          => $this->input->post('price_kg'), 
                'jogrog'            => $this->input->post('jogrog'),
                'quantity'          => 1,
                'discount'          => $this->input->post('discount'),
            );
            if(!$this->session->has_userdata('cart')) {
                $cart = array($items);
                $this->session->set_userdata('cart', serialize($cart));
            } 
            else {
                $index = $this->exists($this->input->post('register_no'));
                $cart = array_values(unserialize($this->session->userdata('cart')));
                if($index == -1) {
                    array_push($cart, $items);
                    $this->session->set_userdata('cart', serialize($cart));
                } else {
                    $cart[$index]['quantity']++;
                    $this->session->set_userdata('cart', serialize($cart));
                }
            }
        } 

        if ($this->session->userdata('cart') !=NULL) {
            $data['items'] = array_values(unserialize($this->session->userdata('cart')));
        } else {
            $data['items'] = [];
        }
        
        $this->data['data'] = $data;
        $this->set_breadcrump('Cart' , 'Keranjang Belanja Farm');
        $this->page = "penjualan_domba/cart";
        $this->layout();
    }

    private function exists($id)
    {
        $cart = array_values(unserialize($this->session->userdata('cart')));
        for ($i = 0; $i < count($cart); $i ++) {
            if ($cart[$i]['register_no'] == $id) {
                return $i;
            }
        }
        return -1;
    }

    public function remove($id)
    {
        $index = $this->exists($id);
        $cart = array_values(unserialize($this->session->userdata('cart')));
        unset($cart[$index]);
        $this->session->set_userdata('cart', serialize($cart));

        redirect('penjualan_domba/cart');
    }

    public function getSelectMember(){
        $searchTerm = $this->input->post('searchTerm');

        $this->db->select('MEMBER_NAME,MEMBER_ALAMAT,MEMBER_HP');
        $this->db->from('TBL_MEMBER');
        $this->db->where('MEMBER_ID', $searchTerm);
        $query1 = $this->db->get();

        $records = [];
        foreach ($query1->result() as $row)
        {
            $records[] = [
                'MEMBER_ALAMAT' => $row->MEMBER_ALAMAT,
                'MEMBER_HP'     => $row->MEMBER_HP,
            ];
                
        }
        echo json_encode($records);
    }

    public function confrim_order(){
        $data = [
            'member' =>[],
            'items'  =>[],
        ];

        $this->db->select('B.MEMBER_NAME,B.MEMBER_ALAMAT,B.MEMBER_HP');
        $this->db->from('TBL_MEMBER B');
        $this->db->where('B.MEMBER_ID', $this->input->post('member'));
        $query = $this->db->get();

        $data['member'] = [
            'nama' => $query->row('MEMBER_NAME'),
            'alamat' => $query->row('MEMBER_ALAMAT'),
            'hp' => $query->row('MEMBER_HP'),
            'tanggal_keluar' => date("d-m-Y", strtotime($this->input->post('outside_date'))),
            'id_member' => $this->input->post('member'),
            'tax' => $this->input->post('tax'),
        ];
        $data['items'] =[];
        foreach (array_values(unserialize($this->session->userdata('cart'))) as $idx => $record) {
            $this->db->select('DESCR');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('MASTER_CODE' , $record['jenis_jual']);
            $query = $this->db->get();

            $this->db->select('B.DESCR AS FARM,C.DESCR AS JENIS');  
	        $this->db->from('TBL_FARM A');
            $this->db->join('GENERAL_SETTING B' , 'A.TYPE_FARM = B.MASTER_CODE' ,'INNER');
            $this->db->join('GENERAL_SETTING C' , 'A.TYPE_LIVESTOCK = C.MASTER_CODE' ,'INNER');
            $this->db->where('A.REGISTER_NO', $record['register_no']);
            $query1 = $this->db->get();

            $item = array(
                'REGISTER_NO'       => $record['register_no'],
                'TYPE_SALE'         => $record['jenis_jual'],
                'TYPE_SALE_DESC'    => $query->row('DESCR'),
                'WEIGHT_CAGE'       => $record['berat_masuk'],
                'WEIGHT_CUSTOMER'   => $record['berat_customer'],
                'TOTAL_JOGROG'      => $record['jogrog'],
                'TOTAL_NON_JOGROG'  => $record['price_kg'],
                'FARM'              => $query1->row('FARM'),
                'JENIS'             => $query1->row('JENIS'),
                'QUANTITY'          => $record['quantity'],
                'DISCOUNT'          => $record['discount'],
            );
            $data['items'][] = $item;
        }
        $this->data['data'] = $data;
        $this->page = "penjualan_domba/confirm";
        $this->layout();
    }
    
    public function checkout(){
        $orderNo = $this->getOrderNo();
        $data = array(
            'ORDER_NO'      => $orderNo,
            'MEMBER_ID'     => $this->input->post('member'),
            'OUT_DATE'      => date("Y-m-d", strtotime($this->input->post('outside_date'))),
            'DTM_CRT'       => date('Y-m-d H:i:s'),
            'INVOICE_NO'    => 'NULL',
            'CONFIRM_SALE'  => 'N',    
        );

        $this->db->insert('TBL_SALE_HEADER', $data);

        $logs = array(
            'USR_CRT' => $this->session->userdata('iduser'),
            'DTM_CRT' => date('Y-m-d H:i:s'),
            'DESCR'   => json_encode($data), 
        );
        $this->db->insert('TBL_LOG', $logs);
        
        foreach (array_values(unserialize($this->session->userdata('cart'))) as $idx => $record) {
                    $data = array(
                        'REGISTER_NO'       => $record['register_no'],
                        'TYPE_SALE'         => $record['jenis_jual'],
                        'OUTSIDE_DATE'      => date("Y-m-d", strtotime($this->input->post('outside_date'))),
                        'WEIGHT_CAGE'       => $record['berat_masuk'],
                        'WEIGHT_CUSTOMER'   => $record['berat_customer'],
                        'CONFIRM_SALE'      => 'N',
                        'MEMBER_ID'         => $this->input->post('member'),
                        'ORDER_NO'          => $orderNo,
                        'CREATE_DATE'       => date('Y-m-d H:i:s'),
                        'USR_CRT'           => $this->session->userdata('iduser'),
                        'TOTAL_JOGROG'      => $record['jogrog'],
                        'TOTAL_NON_JOGROG'  => $record['price_kg']
                    );
                  
                    $this->db->insert('TBL_SALE_FARM', $data);

                    $logs = array(
                        'USR_CRT' => $this->session->userdata('iduser'),
                        'DTM_CRT' => date('Y-m-d H:i:s'),
                        'DESCR'   => json_encode($data), 
                    );
                    $this->db->insert('TBL_LOG', $logs);
        }
        

        redirect('penjualan_domba/lists_jual');
        // SELECT RUN_NUMBER + 1 FROM TBL_SEQUENCE_NUMBER WHERE MASTER_CODE='FM'

        // $data = [
        //     'member'=> [],
        //     'items' => [],
        // ] ;
        // $this->db->select('MEMBER_NAME,MEMBER_ALAMAT,MEMBER_HP');
        // $this->db->from('TBL_MEMBER');
        // $this->db->where('MEMBER_ID', $this->input->post('member'));
        // $query = $this->db->get();
    
        // $data['member'] = [
        //       'nama_member'     =>  $query->row('MEMBER_NAME'),
        //       'alamat_member'   =>  $query->row('MEMBER_ALAMAT'),
        //       'hp_member'       =>  $query->row('MEMBER_HP'),
        //       'tanggal_keluar'  =>  date("d-M-Y", strtotime($this->input->post('outside_date'))),
        // ];

        // $data['items'] = array_values(unserialize($this->session->userdata('cart')));

        // var_dump($data['items']);

    }

    public function lists_jual(){
        $query=PENJUALAN_LIST_JUAL;
        $data['penjualan_list_jual'] = $this->get_paging($query,URL_PENJUALAN_LIST_JUAL);
        $this->data['data'] = $data;
        $this->set_breadcrump('Data Pembelian' , 'List Data');
        $this->page = "penjualan_domba/list_pembelian";
        $this->layout();
    }
    
    public function create_invoice($memberId,$tanggal_keluar,$tax){
        $orderNo = $this->getOrderNo();
        $data = array(
            'ORDER_NO'      => $orderNo,
            'MEMBER_ID'     => $memberId,
            'OUT_DATE'      => date("Y-m-d", strtotime($tanggal_keluar)),
            'DTM_CRT'       => date('Y-m-d H:i:s'),
            'INVOICE_NO'    => 'NULL',
            'CONFIRM_SALE'  => 'Y',    
            'TAX'           => $tax
        );

        $this->db->insert('TBL_SALE_HEADER', $data);

        $logs = array(
            'USR_CRT' => $this->session->userdata('iduser'),
            'DTM_CRT' => date('Y-m-d H:i:s'),
            'DESCR'   => json_encode($data), 
        );
        $this->db->insert('TBL_LOG', $logs);
        
        foreach (array_values(unserialize($this->session->userdata('cart'))) as $idx => $record) {
            $data = array(
                'REGISTER_NO'       => $record['register_no'],
                'TYPE_SALE'         => $record['jenis_jual'],
                'OUTSIDE_DATE'      => date("Y-m-d", strtotime($tanggal_keluar)),
                'WEIGHT_CAGE'       => $record['berat_masuk'],
                'WEIGHT_CUSTOMER'   => $record['berat_customer'],
                'CONFIRM_SALE'      => 'Y',
                'MEMBER_ID'         => $memberId,
                'ORDER_NO'          => $orderNo,
                'CREATE_DATE'       => date('Y-m-d H:i:s'),
                'USR_CRT'           => $this->session->userdata('iduser'),
                'TOTAL_JOGROG'      => $record['jogrog'],
                'TOTAL_NON_JOGROG'  => $record['price_kg'],
                'DISCOUNT'          => $record['discount'],
                'QTY'               => $record['quantity'],
            );
          
            $this->db->insert('TBL_SALE_FARM', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
            $data = array(
                 'STATUS'       => STATUS_SOLD,
                 'UPDATE_DATE'  => date('Y-m-d H:i:s'),
                 'USR_UPD'      => $this->session->userdata('iduser'),
             );
 
             $this->db->where('REGISTER_NO', $record['register_no']);
             $this->db->update('TBL_FARM', $data);
        }

        redirect('penjualan_domba/invoice_detail/'.$orderNo);
    }

    public function invoice_detail($orderNo){
        $this->db->select('INVOICE_NO');
        $this->db->from('TBL_SALE_HEADER');
        $this->db->where('ORDER_NO', $orderNo);
        $query = $this->db->get();
        $invoice=$this->generate_invoice();
        if ($query->row('INVOICE_NO') === 'NULL' ) {
            $this->db->set('INVOICE_NO', $invoice);
            $this->db->set('CONFIRM_SALE', 'Y');
            $this->db->where('ORDER_NO', $orderNo);
            $this->db->update('TBL_SALE_HEADER');

            $this->db->set('INVOICE_NO', $invoice);
            $this->db->set('CONFIRM_SALE', 'Y');
            $this->db->where('ORDER_NO', $orderNo);
            $this->db->update('TBL_SALE_FARM');

        }

        $this->db->select('B.MEMBER_NAME,B.MEMBER_ALAMAT,B.MEMBER_HP,A.OUT_DATE,A.INVOICE_NO,A.TAX');
        $this->db->from('TBL_SALE_HEADER A');
        $this->db->join('TBL_MEMBER B', 'A.MEMBER_ID = A.MEMBER_ID','INNER');
        $this->db->where('A.ORDER_NO', $orderNo);
        $query = $this->db->get();

        $data['member'] = [
              'nama_member'     =>  $query->row('MEMBER_NAME'),
              'alamat_member'   =>  $query->row('MEMBER_ALAMAT'),
              'hp_member'       =>  $query->row('MEMBER_HP'),
              'tanggal_keluar'  =>  date("d-M-Y", strtotime($query->row('OUT_DATE'))),
              'invoice_no'      =>  $query->row('INVOICE_NO'),
              'tax'             =>  $query->row('TAX'),
        ];

        $this->db->select('A.*,B.DESCR, D.DESCR AS JENIS_TERNAK, E.DESCR AS TYPE_TERNAK, F.DESCR AS TYPESALEDESCR');
        $this->db->from('TBL_SALE_FARM A');
        $this->db->join('GENERAL_SETTING B', 'A.TYPE_SALE = B.MASTER_CODE','INNER');
        $this->db->join('TBL_FARM C', 'A.REGISTER_NO = C.REGISTER_NO','INNER');
        $this->db->join('GENERAL_SETTING D', 'D.MASTER_CODE = C.TYPE_FARM','INNER');
        $this->db->join('GENERAL_SETTING E', 'E.MASTER_CODE = C.TYPE_LIVESTOCK','INNER');
        $this->db->join('GENERAL_SETTING F', 'F.MASTER_CODE = A.TYPE_SALE','INNER');
        $this->db->where('A.ORDER_NO', $orderNo);
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'REGISTER_NO'       => $row->REGISTER_NO,
                'TYPE_SALE'         => $row->TYPE_SALE,
                'OUTSIDE_DATE'      => $row->OUTSIDE_DATE,
                'WEIGHT_CAGE'       => $row->WEIGHT_CAGE,
                'WEIGHT_CUSTOMER'   => $row->WEIGHT_CUSTOMER,
                'CONFIRM_SALE'      => $row->CONFIRM_SALE,
                'ORDER_NO'          => $row->ORDER_NO,
                'TOTAL_JOGROG'      => $row->TOTAL_JOGROG,
                'TOTAL_NON_JOGROG'  => $row->TOTAL_NON_JOGROG,
                'JENIS_TERNAK'      => $row->JENIS_TERNAK,
                'TYPE_TERNAK'       => $row->TYPE_TERNAK,
                'QTY'               => $row->QTY,
                'DISCOUNT'          => $row->DISCOUNT,
                'TYPESALEDESC'      => $row->TYPESALEDESCR,
            ];
                
        }
        $data['items'] = $records;
          foreach (array_values(unserialize($this->session->userdata('cart'))) as $idx => $record) {
            $index = $this->exists($record['register_no']);
            $cart = array_values(unserialize($this->session->userdata('cart')));
            unset($cart[$index]);
            $this->session->set_userdata('cart', serialize($cart));            
        }
        $this->load->view('penjualan_domba/pdf_inv',$data);
    }

    private function generate_invoice(){
        $number ="";
        $year   = '%y';
        $month  = '%m';
        $time   = time();  
        if (date("d",(now())) === "01") {
            
            $config['id']     = 0;
            $config['awalan'] = TYPE_INVOICE.mdate($month, $time);
            $config['digit']  = 5;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', 1);
            $this->db->where('MASTER_CODE', TYPE_INVOICE);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        } else {
            $this->db->select('RUN_NUMBER');
            $this->db->from('TBL_SEQUENCE_NUMBER');
            $this->db->where('MASTER_CODE', 'INV');
            $query = $this->db->get();

            $config['id']     = $query->row('RUN_NUMBER');
            $config['awalan'] = TYPE_INVOICE.mdate($month, $time);
            $config['digit']  = 5;
            $this->auto_number->config($config);
            $number = $this->auto_number->generate_id();

            $this->db->set('RUN_NUMBER', $query->row('RUN_NUMBER')+1);
            $this->db->where('MASTER_CODE', TYPE_INVOICE);
            $this->db->update('TBL_SEQUENCE_NUMBER');
        }

        return $number;
    }

    public function print($orderNo){
        $this->db->select('B.MEMBER_NAME,B.MEMBER_ALAMAT,B.MEMBER_HP,A.OUT_DATE,A.INVOICE_NO,A.TAX');
        $this->db->from('TBL_SALE_HEADER A');
        $this->db->join('TBL_MEMBER B', 'A.MEMBER_ID = A.MEMBER_ID','INNER');
        $this->db->where('A.INVOICE_NO', $orderNo);
        $query = $this->db->get();

        $data['member'] = [
            'nama_member'     =>  $query->row('MEMBER_NAME'),
            'alamat_member'   =>  $query->row('MEMBER_ALAMAT'),
            'hp_member'       =>  $query->row('MEMBER_HP'),
            'tanggal_keluar'  =>  date("d-M-Y", strtotime($query->row('OUT_DATE'))),
            'invoice_no'      =>  $query->row('INVOICE_NO'),
            'tax'             =>  $query->row('TAX'),
      ];

      $this->db->select('A.*,B.DESCR, D.DESCR AS JENIS_TERNAK, E.DESCR AS TYPE_TERNAK, F.DESCR AS TYPESALEDESCR');
      $this->db->from('TBL_SALE_FARM A');
      $this->db->join('GENERAL_SETTING B', 'A.TYPE_SALE = B.MASTER_CODE','INNER');
      $this->db->join('TBL_FARM C', 'A.REGISTER_NO = C.REGISTER_NO','INNER');
      $this->db->join('GENERAL_SETTING D', 'D.MASTER_CODE = C.TYPE_FARM','INNER');
      $this->db->join('GENERAL_SETTING E', 'E.MASTER_CODE = C.TYPE_LIVESTOCK','INNER');
      $this->db->join('GENERAL_SETTING F', 'F.MASTER_CODE = A.TYPE_SALE','INNER');
      $this->db->where('A.INVOICE_NO', $orderNo);
      $query = $this->db->get();

      $records = [];
      foreach ($query->result() as $row)
      {
          $records[] = [
              'REGISTER_NO'       => $row->REGISTER_NO,
              'TYPE_SALE'         => $row->TYPE_SALE,
              'OUTSIDE_DATE'      => $row->OUTSIDE_DATE,
              'WEIGHT_CAGE'       => $row->WEIGHT_CAGE,
              'WEIGHT_CUSTOMER'   => $row->WEIGHT_CUSTOMER,
              'CONFIRM_SALE'      => $row->CONFIRM_SALE,
              'ORDER_NO'          => $row->ORDER_NO,
              'TOTAL_JOGROG'      => $row->TOTAL_JOGROG,
              'TOTAL_NON_JOGROG'  => $row->TOTAL_NON_JOGROG,
              'JENIS_TERNAK'      => $row->JENIS_TERNAK,
              'TYPE_TERNAK'       => $row->TYPE_TERNAK,
              'QTY'               => $row->QTY,
              'DISCOUNT'          => $row->DISCOUNT,
              'TYPESALEDESC'      => $row->TYPESALEDESCR,
          ];
              
      }
      $data['items'] = $records;
        $this->load->view('penjualan_domba/print',$data);
    }

    public function cancel($invoice) {

        $data = array(
            'INVOICE_NO'   => $invoice,
            'DESCRIPTION'  => $this->input->post('alasan'), 
            'DTM_CRT'      => date('Y-m-d H:i:s'),
            'ID_USER'      => $this->session->userdata('iduser'),
        );

        $this->db->insert('TBL_SALE_CANCEL', $data);

        $this->db->select('ID_SALE_FARM,REGISTER_NO,INVOICE_NO');
        $this->db->from('TBL_SALE_FARM');
        $this->db->where('INVOICE_NO', $invoice);
        $query = $this->db->get();

        foreach ($query->result() as $row){

            $data = array(
                'CONFIRM_SALE' => 'C',
                'UPDATE_DATE'  => date('Y-m-d H:i:s'),
                'USR_UPD'      => $this->session->userdata('iduser'),
            );

            $this->db->where('ID_SALE_FARM', $row->ID_SALE_FARM);
            $this->db->update('TBL_SALE_FARM', $data);

            $data = array(
                'CONFIRM_SALE' => 'C',
            );
            $this->db->where('INVOICE_NO', $invoice);
            $this->db->update('TBL_SALE_HEADER', $data);
            
            $data = array(
                'STATUS'       => STATUS_ACTIVE,
                'UPDATE_DATE'  => date('Y-m-d H:i:s'),
                'USR_UPD'      => $this->session->userdata('iduser'),
            );

            $this->db->where('REGISTER_NO', $row->REGISTER_NO);
            $this->db->update('TBL_FARM', $data);

        }

        redirect('penjualan_domba/lists_jual');
    }

    public function lists_penjualan(){
        $this->set_breadcrump('Penjualan' , 'List Data');
        $this->page = "penjualan_domba/list_penjualan";
        $this->layout();
    }

    public function lists_penjualan_cancel(){
        $this->set_breadcrump('Penjualan Cancel' , 'List Data');
        $this->page = "penjualan_domba/list_penjualan_cancel";
        $this->layout();
    }
    /**
     * Form Pemberian Pakan
     * @param integer
     */
    // public function form($id = NULL){
    //     $data = [
    //             'jenis_jual' => NULL,
    //             'flow_data'        => [
    //                 'registrasi_no' => NULL, 
    //                 'outside_date'  => NULL,
    //                 'weight_out'    => NULL,
    //                 'weight_cus'    => NULL,
    //                 'jenis_jual'    => NULL,
    //                 'member_id'     => NULL,
    //                 'member_nama'   => NULL,
    //                 'value'         => NULL,
    //              ] 
    //     ];
    //     if ($id === NULL) {
    //         $this->db->select('DESCR, MASTER_CODE');
    //         $this->db->from('GENERAL_SETTING');
    //         $this->db->where('GENERAL_SETTING_CODE', 'TYPE_SALE');
    //         $query = $this->db->get();

    //         $records = [];
    //         foreach ($query->result() as $row)
    //         {
    //             $records[] = [
    //                 'descr' => $row->DESCR,
    //                 'master_code' => $row->MASTER_CODE
    //             ];
                    
    //         }
    //         $data['jenis_jual'] = $records;

    //         $data['flow_data'] = [
    //             'registrasi_no' => $this->input->post('registrasi_no'), 
    //             'outside_date'  => $this->input->post('outside_date'),
    //             'weight_out'    => $this->input->post('weight_out'),
    //             'weight_cus'    => $this->input->post('weight_cus'),
    //             'jenis_jual'    => $this->input->post('jenis_jual'),
    //             'member_id'     => $this->input->post('member_id'),
    //             'member_name'   => NULL,
    //             'value'         => $this->form_validation->error_array(),
    //         ];   

    //         $this->data['data'] = $data;
    //     }
    //     $this->set_breadcrump('Penjualan Domba' , 'Input data');
    //     $this->page = "penjualan_domba/form";
    //     $this->layout();
    // }

    // public function getListRegisterno(){
    //     $this->db->select('a.REGISTER_NO,a.REGISTER_DATE,a.ROOM_NUMBER,e.WEIGHT AS WEIGHT_IN,b.DESCR AS TIPE_TERNAK,c.DESCR AS JENIS_TERNAK,d.DESCR AS TIPE_KANDANG,a.ID_FARM');
    //     $this->db->from('TBL_FARM a');
    //     $this->db->join('GENERAL_SETTING b','a.TYPE_LIVESTOCK = b.MASTER_CODE','INNER');
    //     $this->db->join('GENERAL_SETTING c','a.TYPE_FARM = c.MASTER_CODE','INNER');
    //     $this->db->join('GENERAL_SETTING d','a.TYPE_CAGE = d.MASTER_CODE','INNER');
    //     $this->db->join('TBL_DAILY_WEIGHT e','a.REGISTER_NO = e.REGISTER_NO','INNER');
    //     $this->db->where("a.REGISTER_NO like '%".$this->input->post('searchTerm')."%'  ");
    //     $this->db->where("a.STATUS = 80");
    //     $query = $this->db->get();

    //     $records = [];
    //     foreach ($query->result() as $row)
    //     {
    //         $records[] = [
    //             'REGISTER_NO' => $row->REGISTER_NO,
    //             'ROOM_NUMBER' => $row->ROOM_NUMBER,
    //             'WEIGHT_IN'   => $row->WEIGHT_IN,
    //             'TIPE_TERNAK' => $row->TIPE_TERNAK,
    //             'JENIS_TERNAK'=> $row->JENIS_TERNAK,
    //             'TIPE_KANDANG'=> $row->TIPE_KANDANG,
    //             'ID_FARM'     => $row->ID_FARM,
    //             'REGISTER_DATE'  => date("d-m-Y", strtotime($row->REGISTER_DATE))
    //         ];
                
    //     }

    //     echo json_encode($records);
    // }

    // public function save() {
    //     $this->form_validation
    //     ->set_rules('registrasi_no', "No Registrasi", 'trim|required')
    //     ->set_rules('outside_date', "Tanggal Keluar", 'trim|required')
    //     ->set_rules('weight_out', "Berat Keluar Kandang", 'trim|required')
    //     ->set_rules('weight_cus', "Berat Badan Di Customer", 'trim|required')
    //     ->set_rules('jenis_jual', "Jenis Jual", 'trim|required');

    //     if ($this->form_validation->run() === FALSE)
    //     {
    //         $this->form();
    //         return;
    //     }    

    //     try {
    //         $data = array(
    //             'REGISTER_NO'       => $this->input->post('registrasi_no'),
    //             'TYPE_SALE'         => $this->input->post('jenis_jual'),
    //             'OUTSIDE_DATE'      => date("Y-m-d", strtotime($this->input->post('outside_date'))),
    //             'WEIGHT_CAGE'       => $this->input->post('weight_out'),
    //             'WEIGHT_CUSTOMER'   => $this->input->post('weight_cus'),
    //             'CREATE_DATE'       => date('Y-m-d H:i:s'),
    //             'USR_CRT'           => $this->session->userdata('iduser'),
    //         );
    //         $this->db->insert('TBL_SALE_FARM', $data);

    //         $dataFarm = array(
    //             'STATUS'   => STATUS_SOLD,
    //         );

    //         $this->db->where('REGISTER_NO', $this->input->post('registrasi_no'));
    //         $this->db->update('TBL_FARM', $dataFarm);

    //         $logs = array(
    //             'USR_CRT' => $this->session->userdata('iduser'),
    //             'DTM_CRT' => date('Y-m-d H:i:s'),
    //             'DESCR'   => json_encode($data), 
    //         );
    //         $this->db->insert('TBL_LOG', $logs);
    //     } catch (Exception $e) {
    //         // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
    //         log_message('error ',$e->getMessage());
    //         $this->form();
    //     }
    //     redirect('penjualan_domba/form');
    // }

    // public function lists(){
    //     $data=[
    //         'penjualan' => NULL,
    //     ];
        
    //     $stardate = date("Y-m-d", strtotime($this->input->post('outside_date1')));
    //     $enddate = date("Y-m-d", strtotime($this->input->post('outside_date2')));

    //     if($stardate==='1970-01-01' && $enddate==='1970-01-01'){
    //         $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE between '."'".$stardate."'".' and '."'".$enddate."'";
    //     } else if ($stardate==='1970-01-01') {
    //         $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE = '."'".$stardate."'";
    //     } else if ($enddate==='1970-01-01') {
    //         $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE = '."'".$enddate."'";
    //     } else {
    //         $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE between '."'".$stardate."'".' and '."'".$enddate."'";
    //     }

    //     $data['penjualan'] = $this->get_paging($query,URL_SALE_FARM);
    //     $this->data['data'] = $data;
    //     $this->set_breadcrump('List Penjualan' , 'List data');
    //     $this->page = "penjualan_domba/list";
    //     $this->layout();
    // }

    // public function getSelectMember() {
    //     $searchTerm = $this->input->post('searchTerm');

    //     $response   = $this->getMember($searchTerm);
    //     echo json_encode($response);
    // }
}

