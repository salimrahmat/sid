<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists(){
        $data['member'] = $this->get_paging(MEMBER_LIST,URL_MEMBER_LIST);
        $this->data['data'] = $data;
        $this->set_breadcrump('List Member' , 'Data Member');
        $this->page = "member/index";
        $this->layout();
    }

    public function form($id=NULL){
        $data = [ 
            'flow_data'    => [
               'member_id'      => NULL, 
               'member_name'    => NULL,
               'member_email'   => NULL,
               'member_alamat'  => NULL,
               'member_hp'      => NULL,
               'value'          => NULL,
            ]
       ];

       if ($id === NULL) {
            $data['flow_data'] = [
                'member_id'       => NULL,
                'member_name'     => $this->input->post('member_name'),
                'member_email'    => $this->input->post('member_email'),
                'member_alamat'   => $this->input->post('member_alamat'),
                'member_hp'       => $this->input->post('member_hp'),
                'value'           => $this->form_validation->error_array(),
            ];   
        } else {
            $this->db->select('MEMBER_ID,MEMBER_NAME,MEMBER_EMAIL,MEMBER_ALAMAT,MEMBER_HP');
            $this->db->from('TBL_MEMBER');
            $this->db->where('MEMBER_ID', $id);
            $query = $this->db->get();

            $data['flow_data'] = [
                'member_id'       => $query->row('MEMBER_ID'),
                'member_name'     => $query->row('MEMBER_NAME'),
                'member_email'    => $query->row('MEMBER_EMAIL'),
                'member_alamat'   => $query->row('MEMBER_ALAMAT'),
                'member_hp'       => $query->row('MEMBER_HP'),
                'value'           => $this->form_validation->error_array(),
            ];   

        }
        
        $this->data['data'] = $data;
        $this->set_breadcrump('Registrasi Member' , 'Input data');
        $this->page = "member/form";
        $this->layout();
    }

    public function save () {
        $this->form_validation
        ->set_rules('member_name', "Nama", 'trim|required')
        ->set_rules('member_alamat', "Alamat", 'trim|required')
        ->set_rules('member_email', "Email", 'trim|required|valid_email')
        ->set_rules('member_hp', "Phone", 'trim|required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }  

        try {
            $data = array(
                'MEMBER_NAME'   => $this->input->post('member_name'),
                'MEMBER_EMAIL'  => $this->input->post('member_email'),
                'MEMBER_ALAMAT' => $this->input->post('member_alamat'),
                'MEMBER_HP'     => $this->input->post('member_hp'),
                'ID_USER'       => $this->session->userdata('iduser'),
                'DTM_CRT'       => date('Y-m-d H:i:s'),
            );

            $this->db->insert('TBL_MEMBER', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('member/lists');

    }

    public function update ($id) {
        $this->form_validation
        ->set_rules('member_name', "Nama", 'trim|required')
        ->set_rules('member_alamat', "Alamat", 'trim|required')
        ->set_rules('member_email', "Email", 'trim|required|valid_email')
        ->set_rules('member_hp', "Phone", 'trim|required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }  

        try {
            $data = array(
                'MEMBER_NAME'   => $this->input->post('member_name'),
                'MEMBER_EMAIL'  => $this->input->post('member_email'),
                'MEMBER_ALAMAT' => $this->input->post('member_alamat'),
                'MEMBER_HP'     => $this->input->post('member_hp'),
                'ID_USER'       => $this->session->userdata('iduser'),
                'DTM_CRT'       => date('Y-m-d H:i:s'),
            );

            $this->db->where('MEMBER_ID', $id);
            $this->db->update('TBL_MEMBER', $data);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        redirect('member/lists');

    }

    public function search(){

        $query='SELECT MEMBER_ID,MEMBER_NAME,MEMBER_EMAIL,MEMBER_ALAMAT,MEMBER_HP FROM TBL_MEMBER WHERE';
        $search  = ($this->input->post("table_search"))? $this->input->post("table_search") : NULL;
        $data['member'] = $this->get_paging($query." MEMBER_NAME like '%".$search."%'",URL_MEMBER_LIST);

        $this->data['data'] = $data;
        $this->set_breadcrump('List Member' , 'Data Member');
        $this->page = "member/index";
        $this->layout();
    }
    
}
