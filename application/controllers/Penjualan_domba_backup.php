<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_domba extends MY_Controller {

    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    /**
     * Form Pemberian Pakan
     * @param integer
     */
    public function form($id = NULL){
        $data = [
                'jenis_jual' => NULL,
                'flow_data'        => [
                    'registrasi_no' => NULL, 
                    'outside_date'  => NULL,
                    'weight_out'    => NULL,
                    'weight_cus'    => NULL,
                    'jenis_jual'    => NULL,
                    'member_id'     => NULL,
                    'member_nama'   => NULL,
                    'value'         => NULL,
                 ] 
        ];
        if ($id === NULL) {
            $this->db->select('DESCR, MASTER_CODE');
            $this->db->from('GENERAL_SETTING');
            $this->db->where('GENERAL_SETTING_CODE', 'TYPE_SALE');
            $query = $this->db->get();

            $records = [];
            foreach ($query->result() as $row)
            {
                $records[] = [
                    'descr' => $row->DESCR,
                    'master_code' => $row->MASTER_CODE
                ];
                    
            }
            $data['jenis_jual'] = $records;

            $data['flow_data'] = [
                'registrasi_no' => $this->input->post('registrasi_no'), 
                'outside_date'  => $this->input->post('outside_date'),
                'weight_out'    => $this->input->post('weight_out'),
                'weight_cus'    => $this->input->post('weight_cus'),
                'jenis_jual'    => $this->input->post('jenis_jual'),
                'member_id'     => $this->input->post('member_id'),
                'member_name'   => NULL,
                'value'         => $this->form_validation->error_array(),
            ];   

            $this->data['data'] = $data;
        }
        $this->set_breadcrump('Penjualan Domba' , 'Input data');
        $this->page = "penjualan_domba/form";
        $this->layout();
    }

    public function getListRegisterno(){
        $this->db->select('a.REGISTER_NO,a.REGISTER_DATE,a.ROOM_NUMBER,e.WEIGHT AS WEIGHT_IN,b.DESCR AS TIPE_TERNAK,c.DESCR AS JENIS_TERNAK,d.DESCR AS TIPE_KANDANG,a.ID_FARM');
        $this->db->from('TBL_FARM a');
        $this->db->join('GENERAL_SETTING b','a.TYPE_LIVESTOCK = b.MASTER_CODE','INNER');
        $this->db->join('GENERAL_SETTING c','a.TYPE_FARM = c.MASTER_CODE','INNER');
        $this->db->join('GENERAL_SETTING d','a.TYPE_CAGE = d.MASTER_CODE','INNER');
        $this->db->join('TBL_DAILY_WEIGHT e','a.REGISTER_NO = e.REGISTER_NO','INNER');
        $this->db->where("a.REGISTER_NO like '%".$this->input->post('searchTerm')."%'  ");
        $this->db->where("a.STATUS = 80");
        $query = $this->db->get();

        $records = [];
        foreach ($query->result() as $row)
        {
            $records[] = [
                'REGISTER_NO' => $row->REGISTER_NO,
                'ROOM_NUMBER' => $row->ROOM_NUMBER,
                'WEIGHT_IN'   => $row->WEIGHT_IN,
                'TIPE_TERNAK' => $row->TIPE_TERNAK,
                'JENIS_TERNAK'=> $row->JENIS_TERNAK,
                'TIPE_KANDANG'=> $row->TIPE_KANDANG,
                'ID_FARM'     => $row->ID_FARM,
                'REGISTER_DATE'  => date("d-m-Y", strtotime($row->REGISTER_DATE))
            ];
                
        }

        echo json_encode($records);
    }

    public function save() {
        $this->form_validation
        ->set_rules('registrasi_no', "No Registrasi", 'trim|required')
        ->set_rules('outside_date', "Tanggal Keluar", 'trim|required')
        ->set_rules('weight_out', "Berat Keluar Kandang", 'trim|required')
        ->set_rules('weight_cus', "Berat Badan Di Customer", 'trim|required')
        ->set_rules('jenis_jual', "Jenis Jual", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        try {
            $data = array(
                'REGISTER_NO'       => $this->input->post('registrasi_no'),
                'TYPE_SALE'         => $this->input->post('jenis_jual'),
                'OUTSIDE_DATE'      => date("Y-m-d", strtotime($this->input->post('outside_date'))),
                'WEIGHT_CAGE'       => $this->input->post('weight_out'),
                'WEIGHT_CUSTOMER'   => $this->input->post('weight_cus'),
                'CREATE_DATE'       => date('Y-m-d H:i:s'),
                'USR_CRT'           => $this->session->userdata('iduser'),
            );
            $this->db->insert('TBL_SALE_FARM', $data);

            $dataFarm = array(
                'STATUS'   => STATUS_SOLD,
            );

            $this->db->where('REGISTER_NO', $this->input->post('registrasi_no'));
            $this->db->update('TBL_FARM', $dataFarm);

            $logs = array(
                'USR_CRT' => $this->session->userdata('iduser'),
                'DTM_CRT' => date('Y-m-d H:i:s'),
                'DESCR'   => json_encode($data), 
            );
            $this->db->insert('TBL_LOG', $logs);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        redirect('penjualan_domba/form');
    }

    public function lists(){
        $data=[
            'penjualan' => NULL,
        ];
        
        $stardate = date("Y-m-d", strtotime($this->input->post('outside_date1')));
        $enddate = date("Y-m-d", strtotime($this->input->post('outside_date2')));

        if($stardate==='1970-01-01' && $enddate==='1970-01-01'){
            $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE between '."'".$stardate."'".' and '."'".$enddate."'";
        } else if ($stardate==='1970-01-01') {
            $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE = '."'".$stardate."'";
        } else if ($enddate==='1970-01-01') {
            $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE = '."'".$enddate."'";
        } else {
            $query = SALE_FARM_LIST.' WHERE b.STATUS='.STATUS_SOLD.' and a.OUTSIDE_DATE between '."'".$stardate."'".' and '."'".$enddate."'";
        }

        $data['penjualan'] = $this->get_paging($query,URL_SALE_FARM);
        $this->data['data'] = $data;
        $this->set_breadcrump('List Penjualan' , 'List data');
        $this->page = "penjualan_domba/list";
        $this->layout();
    }

    public function getSelectMember() {
        $searchTerm = $this->input->post('searchTerm');

        $response   = $this->getMember($searchTerm);
        echo json_encode($response);
    }
}

