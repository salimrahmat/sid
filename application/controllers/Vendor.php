<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MY_Controller {
    /**
     * Auth constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->verifyLogin();
    }

    public function lists() {
        $data=[
            'vendors' => NULL, 
        ];

        $query=VENDOR_LIST;
        $data['vendors'] = $this->get_paging($query,URL_REGISTRASI);
        $this->data['data'] = $data;
        $this->set_breadcrump('Vendor' , 'List Data');
        $this->page = "vendor/index";
        $this->layout();
    }

    public function form($id = NULL) {
        $data=[
            'flow_data'        => [
                'vendor_id'         => NULL, 
                'vendor_name'       => NULL,
                'vendor_address'    => NULL,
                'vendor_phone'      => NULL,
                'vendor_pic'        => NULL,
                'status'            => NULL,
                'value'             => NULL,
                ]
        ];

        if ($id === NULL) {
            $data['flow_data'] = [
                'vendor_id'      => NULL,
                'vendor_name'    => $this->input->post('vendor_name'),
                'vendor_address' => $this->input->post('vendor_address'),
                'vendor_phone'   => $this->input->post('vendor_phone'),
                'vendor_pic'     => $this->input->post('vendor_pic'),
                'status'         => $this->input->post('status'),
                'value'          => $this->form_validation->error_array(),
            ];   
            
        } else {
            $this->db->select('ID_VENDOR,VENDOR_NAME,VENDOR_ADDRESS,VENDOR_PHONE,VENDOR_PIC,IS_ACTIVE');
            $this->db->from('TBL_VENDOR');
            $this->db->where('ID_VENDOR', $id);
            $query = $this->db->get();

            $data['flow_data'] = [
                'vendor_id'      => $query->row('ID_VENDOR'),
                'vendor_name'    => $query->row('VENDOR_NAME'),
                'vendor_address' => $query->row('VENDOR_ADDRESS'),
                'vendor_phone'   => $query->row('VENDOR_PHONE'),
                'vendor_pic'     => $query->row('VENDOR_PIC'),
                'status'         => $query->row('IS_ACTIVE'),
                'value'          => $this->form_validation->error_array(),
            ];  
        }
        $this->data['data'] = $data;
        $this->set_breadcrump('Vendor' , 'Input data');
        $this->page = "vendor/form";
        $this->layout();

    }

    public function save(){
        $this->form_validation
        ->set_rules('vendor_name', "Nama Vendor", 'trim|required')
        ->set_rules('vendor_address', "Alamat Vendor", 'trim|required')
        ->set_rules('vendor_phone', "Phone Vendor", 'trim|required')
        ->set_rules('vendor_pic', "Pic", ['trim','required'])
        ->set_rules('status', "Status", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        try {
            $data = array(
                'VENDOR_NAME'    => $this->input->post('vendor_name'),
                'VENDOR_ADDRESS' => $this->input->post('vendor_address'),
                'VENDOR_PHONE'   => $this->input->post('vendor_phone'),
                'VENDOR_PIC'     => $this->input->post('vendor_pic'),
                'IS_ACTIVE'      => $this->input->post('status'),
            );

            $this->db->insert('TBL_VENDOR', $data);
        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('vendor/lists');
    }

    public function update($id){
        $this->form_validation
        ->set_rules('vendor_name', "Nama Vendor", 'trim|required')
        ->set_rules('vendor_address', "Alamat Vendor", 'trim|required')
        ->set_rules('vendor_phone', "Phone Vendor", 'trim|required')
        ->set_rules('vendor_pic', "Pic", ['trim','required'])
        ->set_rules('status', "Status", 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->form();
            return;
        }    

        try {
            $data = array(
                'VENDOR_NAME'    => $this->input->post('vendor_name'),
                'VENDOR_ADDRESS' => $this->input->post('vendor_address'),
                'VENDOR_PHONE'   => $this->input->post('vendor_phone'),
                'VENDOR_PIC'     => $this->input->post('vendor_pic'),
                'IS_ACTIVE'      => $this->input->post('status'),
            );

            $this->db->where('ID_VENDOR', $id);
            $this->db->update('TBL_VENDOR', $data);

        } catch (Exception $e) {
            // this will not catch DB related `enter code here`errors. But it will include them, because this is more general. 
            log_message('error ',$e->getMessage());
            $this->form();
        }
        
        redirect('vendor/lists');
    }

}